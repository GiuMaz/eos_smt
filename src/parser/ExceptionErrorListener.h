#ifndef EXCEPTION_ERROR_LISTENER_HH
#define EXCEPTION_ERROR_LISTENER_HH

#include <BaseErrorListener.h>
#include "error_handling.h"
#include <string>
#include <sstream>

class ExceptionErrorListener : public antlr4::BaseErrorListener {

public:

    virtual void syntaxError(
            antlr4::Recognizer *recognizer, antlr4::Token * offendingSymbol,
            size_t line, size_t charPositionInLine, const std::string &msg,
            std::exception_ptr e) override {

        e = nullptr;
        std::ostringstream oss;
        oss << "rule: " << recognizer->getRuleNames()[0] <<  ", symbol " <<
            offendingSymbol->getText() << " at line: " <<
            line << ":" << charPositionInLine << " " << msg;
        error_msg = oss.str();
        throw parsing_exception(error_msg);
    }

private:
    std::string error_msg;
};

#endif // EXCEPTION_ERROR_LISTENER_HH
