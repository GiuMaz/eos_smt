/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_CORE_ASSIGNMENT_HH
#define EOS_CORE_ASSIGNMENT_HH

#include <vector>
#include <iostream>
#include "variable.h"
#include "value.h"

/**
 * save the value assigned to variable during the cdsat research
 */
class Assignment {
public:
    Assignment() = default;

    size_t size() const;

    void notify_new_variable(const Variable &var);

    bool has_value(const Variable &var) const;

    uint32_t get_timestamp(const Variable &var) const;

    Value get_value(const Variable &var) const;

    void set_value(Variable var, Value val);

    void reset_value(const Variable &var);

    bool all_assigned() const;

private:

    uint32_t global_timestamp = 0;
    std::vector<Value>  var_to_value;
    std::vector<uint32_t> timestamps;
};

#endif // EOS_CORE_ASSIGNMENT_HH
