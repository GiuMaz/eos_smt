/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef EOS_CORE_TERM_HH
#define EOS_CORE_TERM_HH

#include "compact_vector.h"
#include "sort.h"
#include "rational.h"

#include <string>
#include <functional>
#include <unordered_map>
#include <vector>

/**
 * Terms
 *
 * Terms are implemented as unsigned integers, the firts 31 bits contains an
 * index used to identify the term and the last one contains the polarity
 * of the term, it's used ONLY by term of boolean sort. For terms of non-boolean
 * sort the polarity bit are always set to zero.
 *
 * the index component is used alongside a TermTable that store all the
 * information related to a term such as sort and body (see below). All the
 * term and they're information are hash-consed to avoid repetition.
 *
 * term index start from 1 (0 is reserved). The first term is the boolean
 * constant term and is always defined. The two polarity of this term are the
 * true constant and the false constant, and their value are 2 and 3.
 *
 * Terms can be of different kind:
 * atomic terms:
 *   TERM_RESERVED:        reserved term for special purpose
 *   TERM_UNUSED:          unused inside a table, used for garbage collection
 *   TERM_BOOL_CONSTANT:   a boolean constant
 *   TERM_REAL_CONSTANT:   real constant
 *   TERM_UNINTERPRETED:   uninterpreted term (atomic variable or function)
 *
 * all the other are composed terms:
 *   TERM_FUN_APPLICATION: function application (unint term + arguments)
 *
 * arithmetic operator in the form ATOM ~ 0 where ~ is one of this relation:
 *   TERM_ARITH_EQ:       ==
 *   TERM_ARITH_GE:       >=
 *
 * boolean operator:
 *   TERM_OR:  or of an array of boolean
 *   TERM_XOR: xor of an array of boolean
 *
 * equality and disequality:
 *   TERM_EQ:       equality between a pair of terms
 *   TERM_DISTINCT: disequality between an array of terms
 *
 * if then else:
 *   TERM_ITE: if-then-else a b c, it take a boolean guard a and return
 *            b if the guard is true, c otherwise
 *
 * All the term have a sort, for all the information about sort read "sort.h"
 *
 * Terms can also have a body, a body is a set of extra information related
 * to the kind of stored term:
 *
 *  TERM_RESERVED, TERM_BOOL_CONSTANT, TERM_UNINTERPRETED
 *      don't have a body
 *
 *  TERM_UNUSED
 *      have an integer use for garbage collection (see below)
 *
 *  TERM_REAL_CONSTANT:
 *      store a rational constant, in LRA rational and real make no difference
 *
 *  TERM_FUN_APPLICATION, TERM_OR, TERM_XOR, TERM_DISTINCT
 *      store the array with all they're arguments
 *
 *  TERM_EQ, TERM_ARITH_EQ, TERM_ARITH_GE
 *      store the pair of term in the specified relation
 *
 *  TERM_ITE
 *      store a boolean guard and two brench of the same sort
 *
 * Some usefull special term are declared globally:
 *   NULL_TERM:  have all 1, is always greater than the other terms
 *   true_term:  is the boolean true constant
 *   false_term: is the boolean false constant
 */

class Term {
    static const uint32_t ONE_MASK = 0x00000001;
    // index :31, polarity :1
    uint32_t value = UINT32_MAX; // all 1 is the value of NULL_TERM
public:
    Term() = default; 
    Term(const Term &) = default;
    Term(Term &&) = default;
    explicit Term(uint32_t v) noexcept : value(v<<1) {}
    Term(uint32_t v, bool s)  noexcept : value( (v<<1) | (s ? 1:0)) {}

    ~Term() = default;

    Term &operator=(const Term &) = default;
    Term &operator=(Term &&) = default;

    size_t   hash()     const { return static_cast<size_t>(value);   }
    uint32_t index()    const { return value >> 1;   }
    bool     polarity() const { return static_cast<bool>(~value & ONE_MASK); }

    bool operator==(const Term &rhs) const { return value == rhs.value; }
    bool operator!=(const Term &rhs) const { return value != rhs.value; }
    bool operator< (const Term &rhs) const { return value <  rhs.value; }
    bool operator<=(const Term &rhs) const { return value <= rhs.value; }
    bool operator> (const Term &rhs) const { return value >  rhs.value; }
    bool operator>=(const Term &rhs) const { return value >= rhs.value; }

    // basic term manipulation
    bool is_negative() const { return static_cast<bool>(value  & ONE_MASK); }
    bool is_positive() const { return static_cast<bool>(~value & ONE_MASK); }

    Term opposite() const { Term t; t.value = value ^  ONE_MASK; return t; }
    Term   unsign() const { Term t; t.value = value & ~ONE_MASK; return t; }
};

// hash function
namespace std {
    template <> struct hash<Term> {
        size_t operator()(const Term &x) const { return x.hash(); }
    };
}

// printing
std::ostream & operator<< (std::ostream & os, Term t);



/**
 * linear polyinomial for real arithmetic
 */

// mononmial of the kind c*x for a rational coefficent c and an indeterminant x
struct Monomial {
    Rational coeff;
    Term indet;
};

// A normalized vector of monomial
using Polynomial = compact_vector<Monomial>;
using PolynomialPtr = Polynomial *;

void add_monomial(std::vector<Monomial> &v, const Monomial &mono);
void add_monomial(std::vector<Monomial> &v, const Polynomial &pol);
void sub_monomial(std::vector<Monomial> &v, const Monomial &mono);
void sub_monomial(std::vector<Monomial> &v, const Polynomial &pol);


std::ostream & operator<< (std::ostream & os, const Monomial &m );

// hashing
struct polynomial_hasher {
    std::size_t operator()(const PolynomialPtr &key) const;
};

struct polynomial_equality {
    bool operator() (PolynomialPtr const& p1, PolynomialPtr const& p2) const;
};

using PolynomialHash = std::unordered_map<PolynomialPtr, uint32_t,
      polynomial_hasher, polynomial_equality>;

// the kind of term
enum TermKind {

    // special
    TERM_RESERVED,
    TERM_UNUSED,

    // constant
    TERM_BOOL_CONSTANT,
    TERM_REAL_CONSTANT,

    // variables
    TERM_UNINTERPRETED,
    TERM_FUN_APPLICATION,

    // linear arithmetic polynomial
    TERM_ARITH_POLY,

    // LRA, of the kind t1 ~ 0 for some relation ~ and polynomial t
    TERM_ARITH_EQ,
    TERM_ARITH_GE,

    // composite
    TERM_OR,
    TERM_XOR,
    TERM_EQ,
    TERM_DISTINCT,
    TERM_ITE,

    // count
    TOTAL_TERM_KIND
};

// possible content of a term
union TermBody {
    int32_t integer;
    compact_vector<Term> *term_list_ptr;
    compact_string *name;
    Rational rational;
    Polynomial *polynomial;

    TermBody() : integer(0) {}
    explicit TermBody(compact_vector<Term> *p) : term_list_ptr(p) {}
    explicit TermBody(compact_string *n) : name(n) {}
    explicit TermBody(int64_t i) : integer(i) {}
    explicit TermBody(Rational r) : rational(r) {}
    explicit TermBody(Polynomial *p) : polynomial(p) {}
};

// operator for the hash table that store the various kind of sorts

// term array of the kind (op t[1] ... t[n])
struct TermArrayKey {
    TermKind    kind;
    size_t      size;
    const Term *ptr;
};

struct term_array_hasher {
    std::size_t operator()(const TermArrayKey &key) const;
};

struct term_array_equality {
    bool operator() (TermArrayKey const& t1, TermArrayKey const& t2) const;
};

using TermArrayHash =  std::unordered_map<TermArrayKey, uint32_t,
      term_array_hasher, term_array_equality>;

// table of named term
using TermNamedHash =  std::unordered_map<const char *, Term, named_hasher,
      named_equality>;

// special term (id start at 1)
static const Term NULL_TERM; // all 1s
static const Term true_term(1,false); // index 1 polarity 0
static const Term false_term(1,true); // index 1 polairty 1
static const Term zero_term(2); // real constant == 0

/**
 * Term Table
 */
class TermTable {

public:
    explicit TermTable(SortTable &st);

    SortTable &get_sort_table();

    Term get_named_term(const std::string &name);
    Term get_named_term(const char *name);
    Term get_named_term(const compact_string *name);

    Sort sort_of( Term t );

    //term generation

    // Uninterpreted term, they can have a name
    Term add_unint_term(compact_string *name, Sort s);
    Term add_unint_term(Sort s);

    // boolean operation
    Term add_not(Term t);
    Term add_or(std::vector<Term> &args);
    Term add_and(std::vector<Term> &args);
    Term add_iff(Term x, Term y);
    Term add_implication(std::vector<Term> &args);
    Term add_binary_xor(Term x, Term y);
    Term add_xor(std::vector<Term> &args);

    // funciton application
    Term add_fun_app(Term fun, std::vector<Term> &args);

    // equality and disequality
    Term add_binary_equality(Term x, Term y);
    Term add_arith_equality(Term x, Term y);
    Term add_equality(std::vector<Term> &args);
    Term add_distinct(std::vector<Term> &args);
    Term add_binary_distinct(Term x, Term y);

    // if then else
    Term add_ite(std::vector<Term> &args);
    Term add_boolean_ite(std::vector<Term> &args);

    // real constant
    Term add_real_constant(Rational q);
    Term add_division(const std::vector<Term> &args);

    // polynomial for linear real arithmetic
    Term add_polynomial(Polynomial *q);
    Term add_multiplication(const std::vector<Term> &args);
    Term add_sum(const std::vector<Term> &args);
    Term add_sub(const std::vector<Term> &args);
    Term add_negated_arith_term( Term t );

    // arithmetic comparison
    Term add_binary_ge(Term x, Term y);
    Term add_binary_gt(Term x, Term y);
    Term add_binary_le(Term x, Term y);
    Term add_binary_lt(Term x, Term y);

    // checks
    void check_add_unint_term(compact_string *n, Sort s);
    void check_not(const std::vector<Term> &args);
    void check_or(const std::vector<Term> &args);
    void check_and(const std::vector<Term> &args);
    void check_implication(const std::vector<Term> &args);
    void check_xor(const std::vector<Term> &args);
    void check_fun_app(const std::string &s, const std::vector<Term> &args);
    void check_equality(const std::vector<Term> &args);
    void check_distinct(const std::vector<Term> &args);
    void check_ite(const std::vector<Term> &args);
    void check_negated_arith_term( Term t );
    void check_division(const std::vector<Term> &args);
    void check_multiplication(const std::vector<Term> &args);
    void check_sum(const std::vector<Term> &args);
    void check_sub(const std::vector<Term> &args);
    void check_binary_ge(Term x, Term y);
    void check_binary_gt(Term x, Term y);
    void check_binary_le(Term x, Term y);
    void check_binary_lt(Term x, Term y);

    // handle local context
    void push_local_context();
    void pop_local_context();
    void add_local_name_to_term(compact_string *n, Term);

    // kind of
    TermKind kind_of( Term t ) const;

    const compact_vector<Term> &get_composite_args( Term t ) const;
    uint32_t get_integer_arg( Term t ) const;
    const Rational &get_rational( Term t ) const;
    compact_string *get_name( Term t ) const;
    const Polynomial &get_polynomial( Term t) const;

    std::string pprint(Term t) const;

private:
    Term get_binary_term( TermKind k, Sort s, Term x, Term y);

    // return the positive term associated to a composite term, create a new
    // term if necessary
    Term get_composite_term( TermKind k, Sort s,  const std::vector<Term> &args );

    void check(bool fact, const std::string &msg);
    bool check_size_at_least(size_t size, size_t limit);
    bool check_size_exactly(size_t size, size_t limit);
    bool check_sort(Term t, Sort s);
    bool check_same_sort(Term s1, Term s2);
    bool check_all_sort(const std::vector<Term> &args, Sort s);
    bool check_all_equal_sort(const std::vector<Term> &args);

    SortTable &sort_table;

    uint32_t add_term(TermKind k, Sort s, TermBody b = TermBody());

    uint32_t total_term;
    std::vector<TermKind> kind;
    std::vector<TermBody> body;
    std::vector<Sort>     sort;

    TermArrayHash  composite_map;
    PolynomialHash polynomial_map;
    TermNamedHash  named_map; 
    std::unordered_map<Rational, uint32_t> constant_map;
    std::unordered_map<Term, uint32_t> arith_ge_map;
    std::unordered_map<Term, uint32_t> arith_eq_map;

    std::vector<bool> marked; // for garbage collection

    // handle local names
    std::vector<std::vector<std::pair<const char *,Term> > > shadowed_names;
    std::vector<std::vector<compact_string *> > local_name;

    int free_index;

    friend std::ostream &operator<< (std::ostream & os, const TermTable &s );
};

#endif // EOS_CORE_TERM_HH
