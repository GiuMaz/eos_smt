/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "assignment.h"
#include "printing.h"

size_t Assignment::size() const { return var_to_value.size(); }

void Assignment::notify_new_variable(const Variable &var) {
    if ( var_to_value.size() <= var.index() ) {
        if ( MESSAGE_LEVEL( VERBOUSE_MESSAGE ) ) {
            debug << "ASSIGNMENT: expand to variable " << var << std::endl;
        }
        var_to_value.resize(var.index()+1, NULL_VALUE);
        timestamps.resize(var.index()+1, 0);
    }
}

bool Assignment::has_value(const Variable &var) const {
    if ( var.index() >= var_to_value.size() )
        return false;
    else
        return var_to_value[var.index()] != NULL_VALUE;
}

uint32_t Assignment::get_timestamp(const Variable &var) const {
    if ( var.index() >= timestamps.size() )
        return 0;
    else
        return timestamps[var.index()];
}

Value Assignment::get_value(const Variable &var) const {
    if ( var.index() >= var_to_value.size() )
        return NULL_VALUE;
    else
        return var_to_value[var.index()];
}

void Assignment::set_value(Variable var, Value val) {
    // check that the term exist
    assert( var.index() < var_to_value.size() );

    var_to_value[var.index()] = val;
    timestamps[var.index()] = ++global_timestamp;
}

void Assignment::reset_value(const Variable &var) {
    // check that the term exist
    assert( var.index() < var_to_value.size() );
    var_to_value[var.index()] = NULL_VALUE;
    timestamps[var.index()] = ++global_timestamp;
}

bool Assignment::all_assigned() const {
    // skip the firt one, it's the NULL_VAR
    for ( auto it = var_to_value.begin()+1; it != var_to_value.end(); ++it )
        if ( *it == NULL_VALUE )
            return false;

    return true;
}
