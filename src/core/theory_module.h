/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_CORE_THEORY_MODULE_HH
#define EOS_CORE_THEORY_MODULE_HH

#include "sort.h"
#include "term.h"
#include "variable.h"
#include "trail.h"
#include <vector>
#include <set>
#include <cassert>
#include <unordered_set>

class SMTSolver;

/**
 * this is a virtual class that define all the component of a theory solver
 */
class TheoryModule {
public:

    TheoryModule( SMTSolver &solver );

    virtual ~TheoryModule() = default;

    virtual void new_term_notify( Term ) { }

    // propagate some event, return true if there is a conflict
    virtual void propagate() {
        // don't do any propagation
    }

    /*
     * return a set (stored as a vector) of variable previously assigned in the
     * trail that justify the current assignment.
     */
    virtual void get_justification(Variable, std::vector<Variable> &) {
        assert(false);
    }

    // get the variables with an infeasible assignment
    virtual void get_conflict(std::vector<Variable>&) {}

    // assign a theory value to a Term
    virtual void make_decision(Variable) {}

    // return a extra information on the building solution
    virtual void build_solution() {
        /* no extra info by default */
    }

    virtual void push() {}

    virtual void pop() { }

    virtual void new_lemma(const std::vector<Term> &) {}

    const ModuleId &get_id() { return id; }

protected:
    SMTSolver &main_solver;
    SortTable &sort_table;
    TermTable &term_table;
    VariableTable &variable_table;
    Trail &trail;

private:
    ModuleId id;
};

#endif // EOS_CORE_THEORY_SOLVER_HH
