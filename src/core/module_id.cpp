/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include "module_id.h"

const std::string ModuleId::NO_MODULE_NAME = "NO_MODULE";
std::vector<std::string> ModuleId::name_table;

uint32_t ModuleId::index() const {
    return id;
}

const std::string &ModuleId::name() const {
    if ( id == NO_MODULE )
        return NO_MODULE_NAME;
    else {
        assert( name_table.size() > id );
        return name_table[id];
    }
}

void ModuleId::set_name(const ModuleId &id, const std::string &name) {
    if ( name_table.size() <= id.index() )
        name_table.resize(id.index()+1,"");
    name_table[id.index()] = name;
}
