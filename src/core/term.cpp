/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <sstream>

#include "compact_vector.h"
#include "error_handling.h"
#include "term.h"

#include "printing.h"

//  TERM HASH
// -----------

size_t term_array_hasher::operator()(const TermArrayKey &key) const {
    static const size_t hash_combiner = 0x9e3779b9;
    size_t seed = (key.kind << 16) + (key.size % (1 << 16));
    // based of boost hash combiner
    for( size_t i = 0; i < key.size; ++i )
        seed ^= key.ptr[i].hash() + hash_combiner + (seed << 6) + (seed >> 2);
    return seed;
}

bool term_array_equality::operator()(
        TermArrayKey const& t1, TermArrayKey const& t2) const {
    if ( t1.kind != t2.kind )
        return false;
    if ( t1.size != t2.size )
        return false;
    for( size_t i = 0; i < t1.size; ++i )
        if ( t1.ptr[i] != t2.ptr[i] )
            return false;
    return true;
}



//  TERM TABLE
// ------------

/*
 * index 0 must be reserved, and index 1 must be set to TERM_BOOL_CONSTANT
 */
TermTable::TermTable(SortTable &st):
    sort_table(st),
    total_term(0),
    kind(),
    body(),
    sort(),
    composite_map(),
    named_map(),
    marked(),
    shadowed_names(),
    local_name(),
    free_index(-1) {
        // reserved term zero
        add_term(TERM_RESERVED, NULL_SORT);

        // insert true and false
        add_term(TERM_BOOL_CONSTANT, bool_sort);

        // arithmetic zero
        add_real_constant(Rational(0));

        // check assumption
        assert( kind.size() == 3 );
        assert( body.size() == 3 );
        assert( sort.size() == 3 );
        assert( total_term  == 3 );

        assert( true_term.opposite() == false_term );
        assert( true_term.index() == 1 );

        assert( zero_term.index() == 2 );
    }

SortTable &TermTable::get_sort_table() {
    return sort_table;
}

Sort TermTable::sort_of( Term t ) {
    assert( t.index() >= 0 && t.index() < sort.size() );
    return sort[t.index()];
}

Term TermTable::get_binary_term( TermKind k, Sort s, Term x, Term y) {
    return get_composite_term(k,s,{x,y});
}

Term TermTable::get_composite_term( TermKind k, Sort s,  const std::vector<Term> &args ) {
    TermArrayKey key { k, args.size(), args.data() };
    if ( composite_map.find( key ) != composite_map.end() ) {
        return Term(composite_map[key]);
    }

    uint32_t pos = add_term(k, s,
            TermBody(compact_vector<Term>::allocate(args)));

    const auto &ref = body[pos].term_list_ptr;
    key = {k, ref->size(), ref->data()};
    composite_map[key] = pos;
    return Term(pos);
}

void TermTable::push_local_context() {
    // open a new level in the local term stack
    local_name.push_back({});
    shadowed_names.push_back({});
}

void TermTable::pop_local_context() {
    // remove all local variable
    while ( ! local_name.back().empty() ) {
        auto it = named_map.find(local_name.back().back()->data());
        named_map.erase(it);
        compact_string::deallocate(local_name.back().back());
        local_name.back().pop_back();
    }

    // reset old name if some name shadowed an already existing term
    while ( ! shadowed_names.back().empty() ) {
        const auto & p = shadowed_names.back().back();
        named_map[p.first] = p.second;
        shadowed_names.back().pop_back();
    }

    // remove level in local term stack
    local_name.pop_back();
    shadowed_names.pop_back();
}

void TermTable::add_local_name_to_term(compact_string *n, Term t) {
    auto it = named_map.find(n->data());
    if ( it != named_map.end() ) {
        // this name already exist! must be shadowed in this local context
        shadowed_names.back().push_back(*it);
    }
    local_name.back().push_back(n);
    named_map[n->data()] = t;
}

Term TermTable::add_unint_term(compact_string *n, Sort s) {
    uint32_t pos = add_term(TERM_UNINTERPRETED, s, TermBody(n));
    named_map[body[pos].name->data()] = Term(pos);

    return Term(pos);
}

Term TermTable::add_unint_term(Sort s) {
    uint32_t pos = add_term(TERM_UNINTERPRETED, s, TermBody(
                static_cast<compact_string *>(nullptr)));
    return Term(pos);
}

Term TermTable::get_named_term(const char *n) {
    auto it = named_map.find(n);
    if ( it == named_map.end() )
        return NULL_TERM;
    else
        return it->second;
}

Term TermTable::get_named_term(const std::string &name) {
    return get_named_term(name.c_str());
}

Term TermTable::get_named_term(const compact_string *name) {
    return get_named_term(name->data());
}

TermKind TermTable::kind_of( Term t ) const {
    return kind[t.index()];
}

// helper method
uint32_t TermTable::add_term(TermKind k, Sort s, TermBody b) {

    int pos = free_index;

    if ( pos == -1 ) {
        // add a new term
        pos = total_term++;
        kind.push_back(k);
        sort.push_back(s);
        body.push_back(b);
    }
    else {
        // reuse deleted term
        free_index = body[pos].integer;
        kind[pos] = k;
        sort[pos] = s;
        body[pos] = b;
    }

    return pos;
}


//  TERM CONSTRUCTION
// -------------------

/*
 * simply return the opposite literal
 */
Term TermTable::add_not(Term t) {
    return t.opposite();
}

/*
 * create the or of a list of Term,  args is modified.
 * sort the variable to have a standardized form.
 * simplification:
 *   (or true ... )      ~> true
 *   (or false ... )     ~> (or ... )
 *   (or x x ... )       ~> (or x ... )
 *   (or x (not x) ... ) ~> true
 *   if the arguments have size zero ~> false
 *   if the arguments have size one  ~> return the only argument
 */
Term TermTable::add_or(std::vector<Term> &args) {
    // sorting the terms by Term
    std::sort(args.begin(), args.end());

    // check if true
    if (args[0] == true_term)
        return true_term;

    size_t i = 1, j = 0;

    // remove false literal
    if ( args[0] == false_term ) {
        // is everything false?
        if ( args.back() == false_term )
            return false_term;

        // skip all the false
        while ( args[i] == false_term ) { ++i; }

        // move the first meaningfull litteral in the firt position
        args[0] = args[i++];
    }

    // remove duplicate and check for opposite terms
    for ( ; i < args.size(); ++i ) {
        Term t1 = args[i], t2 = args[j];
        if ( t1 != t2 ) {
            if ( t1 == t2.opposite())
                return true_term;
            args[++j] = t1;
        }
    }

    args.resize(j+1);

    if ( args.size() == 0 )
        return false_term;
    if (args.size() == 1 )
        return args[0];
    else
        return get_composite_term(TERM_OR, bool_sort, args);
}

/*
 * (and t[0] ... [n]) ~> (not (or (not t[0]) ... (not t[n])))
 */
Term TermTable::add_and(std::vector<Term> &args) {
    for ( size_t i = 0; i < args.size(); ++i)
        args[i] = args[i].opposite();
    return add_or(args).opposite();
}

/*
 * (=> t[0] t[1] ... t[n]) ~> (or (not t[0]) (not t[1]) ... t[n])
 * => is right associative. flip all the term except for the last one. 
 */
Term TermTable::add_implication(std::vector<Term> &args) {
    for ( size_t i = 0; i < args.size()-1; ++i)
        args[i] = args[i].opposite();
    return add_or( args );
}

/*
 * if-and-only-if of a couple of term. Store the couple in order.
 * simplification:
 *   (eq x x)       ~> true
 *   (eq x true)    ~> x
 *   (eq x false)   ~> (not x)
 *   (eq x (not x)) ~> false
 */
Term TermTable::add_iff(Term x, Term y) {
    if ( x == y ) return true_term;
    if ( x == true_term) return y;
    if ( y == true_term) return x;
    if ( x == false_term) return y.opposite();
    if ( y == false_term) return x.opposite();
    if ( x == y.opposite()) return false_term;

    // standardize
    if ( x > y ) std::swap(x,y);

    return get_binary_term(TERM_EQ, bool_sort, x, y);
} 

/*
 * binary xor, the opposite of iff
 */
Term TermTable::add_binary_xor( Term x, Term y) {
    return add_iff(x,y).opposite();
}

/* 
 * xor of an array of terms. The array is stored in order.
 * simplification:
 *   (xor ... (not x) ... ) ~> (not (xor ... x ... ))
 *   (xor true ...)  ~>  (not (xor ...))
 *   (xor false ...) ~> (xor ...)
 *   (xor x x ...) ~> (xor ...)
 *
 *   if size == 0 ~> false
 *   if size == 1 ~> return the only element
 */
Term TermTable::add_xor(std::vector<Term> &args) {

    // used to store the various negation
    bool all_negated = false;

    // remove true and false and move negation outside the xor
    size_t j = 0;
    size_t i = 0;
    for (; i < args.size(); ++i ) {
        if ( args[i] == true_term )
            all_negated = !all_negated;
        else if ( args[i] == false_term )
            continue;
        else {
            if ( args[i].is_negative() ) {
                all_negated = !all_negated;
                args[i] = args[i].unsign();
            }
            args[j++] = args[i];
        }
    }
    args.resize(j);

    // standardize
    std::sort( args.begin(), args.end() );

    // remove duplicate, x ^ x == false_term
    j = 0;
    for (i = 0; i < args.size()-1; ++i) {
        // try to simplify with the next term, if possible skip both of them
        if ( args[i] == args[i+1] )
            ++i;
        // otherwise simply copy
        else
            args[j++] = args[i];
    }

    // if the last element is not simplyfied but it predecessor, add it
    if ( i == args.size() - 1 )
        args[j++] = args[i];

    // resize to j+1, the last argument are always included
    args.resize(j);

    if (args.empty())
        return all_negated ? true_term : false_term ;
    if ( args.size() == 1 )
        return all_negated ? args[0].opposite() : args[0];
    if ( args.size() == 2 ) {
        Term t = add_binary_xor(args[0], args[1]);
        return all_negated ? t.opposite() : t;
    }
    else {
        Term t = get_composite_term(TERM_XOR, bool_sort, args);
        return all_negated ? t.opposite() : t;
    }
}

/*
 * if(cond) then x else y for all boolean term
 *
 * the condition is normalized to be positive, after that simplify.
 *
 * possible simplification:
 *  duplicate elimination
 *  (ite c x x)       ~> x
 *  (ite c x (not x)) ~> (eq c x)
 *
 *  trivial condition elimination
 *  (ite true x y)  ~> x
 *
 *  factoring duplicate condition in branch
 *  (ite c c y)       ~> (or  c y)
 *  (ite c (not c) y) ~> (and (not c) y)
 *  (ite c x c)       ~> (and c x)
 *  (ite c x (not c)) ~> (or  x (not c))
 *  
 *  ite redundancy elimination
 *  (ite c (ite c x y) z) ~> (ite c x z)
 *  (ite c x (ite c y z)) ~> (ite c x z)
 *
 *  trivial branch elimination
 *  (ite c true y)  ~> (or  c y)
 *  (ite c false y) ~> (and (not c) y)
 *  (ite c x true)  ~> (or  (not c) x)
 *  (ite c x false) ~> (and c x)
 */
Term TermTable::add_boolean_ite(std::vector<Term> &args) {
    Term &cond = args[0], &x = args[1], &y = args[2];

    // standardize
    if ( cond.is_negative() ) {
        // (ite (not c) x y) ~> (ite c y x)
        cond = cond.opposite();
        std::swap(x,y);
    }

    /*
     * (ite c (ite c x y) z) ~> (ite c x z)
     * (ite c x (ite c y z))       ~> (ite c x z)
     */
    if ( kind_of(x) == TERM_ITE ) {
        const auto &child = get_composite_args(x);
        if ( child[0] == cond )
            x = child[1];
    }
    if ( kind_of(y) == TERM_ITE ) {
        const auto &child = get_composite_args(y);
        if ( child[0] == cond )
            x = child[2];
    }

    /*
     *  (ite c x x)       ~> x
     *  (ite c x (not x)) ~> (eq c x)
     */
    if ( x == y ) return x;
    if ( x == y.opposite() ) return add_iff(cond,x);

    /*
     *  (ite true x y) ~> x
     */
    if ( cond == true_term)  return x;

    /*
     *  (ite c c y)       ~> (or  c y)
     *  (ite c (not c) y) ~> (and (not c) y)
     *  (ite c x c)       ~> (and c x)
     *  (ite c x (not c)) ~> (or  x (not c))
     */
    if ( cond == x ) {
        args[0] = cond; args[1] = y; args.resize(2);
        return add_or(args);
    }
    if ( cond == x.opposite() ) {
        args[0] = cond.opposite(); args[1] = y; args.resize(2);
        return add_and(args);
    }
    if ( cond == y ) {
        args[0] = cond; args[1] = x; args.resize(2);
        return add_and(args);
    }
    if ( cond == y.opposite() ) {
        args[0] = cond.opposite(); args[1] = x; args.resize(2);
        return add_or(args);
    }

    /*
     *  (ite c true y)    ~> (or  c y)
     *  (ite c false y)   ~> (and (not c) y)
     *  (ite c x true)    ~> (or  (not c) x)
     *  (ite c x false)   ~> (and c x)
     */
    if ( x == true_term ) {
        args[0] = cond; args[1] = y; args.resize(2);
        return add_or(args);
    }
    if ( x == false_term ) {
        args[0] = cond.opposite(); args[1] = y; args.resize(2);
        return add_and(args);
    }
    if ( y == true_term ) {
        args[0] = cond.opposite(); args[1] = x; args.resize(2);
        return add_or(args);
    }
    if ( y == false_term ) {
        args[0] = cond; args[1] = x; args.resize(2);
        Term t = add_and(args);
        return t;
    }

    return get_composite_term(TERM_ITE, bool_sort, args);
}

/*
 * given a relation between arithmetic atom x == y this method build a
 * normalized Term of the kind x - y = 0.
 *
 * simplification:
 *  - if x-y is empty return true
 *  - if x-y is a real constant != 0 return false
 *  - if x-y is of the kind a*t, store (a/|a|) * t == 0
 *  - if x-y is of size >= 2, simplify all the nominator and denominator by
 *    their gcd. Normalize the sign (the first coefficent is always positive).
 */
Term TermTable::add_arith_equality(Term x, Term y) {

    // build x - y
    std::vector<Monomial> new_pol;

    // add x
    if ( kind[x.index()] == TERM_REAL_CONSTANT ) {
        // turn real constant in monomial with NULL_TERM
        add_monomial(new_pol,{body[x.index()].rational,NULL_TERM}); 
    }
    else if ( kind[x.index()] == TERM_ARITH_POLY ) {
        // flatten polynomial
        add_monomial(new_pol,*body[x.index()].polynomial); 
    }
    else {
        assert(sort_of(x) == real_sort);
        add_monomial(new_pol,{Rational(1),x}); 
    }

    // subtract y
    if ( kind[y.index()] == TERM_REAL_CONSTANT ) {
        // turn real constant in monomial with NULL_TERM
        sub_monomial(new_pol,{body[y.index()].rational,NULL_TERM});
    }
    else if ( kind[y.index()] == TERM_ARITH_POLY ) {
        // flatten polynomial
        sub_monomial(new_pol,*body[y.index()].polynomial); 
    }
    else {
        assert( sort_of(y) == real_sort);
        sub_monomial(new_pol,{Rational(1),y});
    }

    if ( new_pol.size() == 0 )
        return true_term; // 0 == 0

    // rational constant
    // it should be false, because new_pol[0].coeff != 0
    if ( new_pol.size() == 1 && new_pol[0].indet == NULL_TERM  )
        return false_term;

    // normalize gcd of numerator and divide everything by this value
    // e.g.: (2/9)*x + (8/3)*y == 0 ~> (1/9)*x + (4/3)*y == 0
    int norm_num;
    if ( new_pol.size() == 1 )
        // take the absolute value
        norm_num = std::abs(new_pol[0].coeff.get_num());
    else
        norm_num = gcd(new_pol[0].coeff.get_num(), new_pol[1].coeff.get_num());

    // for size() > 2, gcd(a,b,c) == gcd(gcd(a,b),c) ecc...
    for ( size_t i = 2; i < new_pol.size(); ++i)
        norm_num = gcd( norm_num, new_pol[i].coeff.get_num() );

    // find lcm of denominator and put this value at all denominator
    int norm_den;
    if ( new_pol.size() == 1 )
        // take the absolute value
        norm_den = std::abs(new_pol[0].coeff.get_denom());
    else
        norm_den = lcm(new_pol[0].coeff.get_denom(), new_pol[1].coeff.get_denom());

    for ( size_t i = 2; i < new_pol.size(); ++i)
        norm_den = lcm(norm_den, new_pol[i].coeff.get_denom());

    // normalize sign: the firtst monomial is positive
    if ( new_pol[0].coeff < 0 )
        norm_den = -norm_den;

    // normalize all coefficent:
    //  - divide the original numerator by the gcd of all the numerato
    //  - multiply the numerator by the division of the lcm of the denumerator
    //    and the actual denumerator
    //  - set the denumerator to 1
    for ( auto &m : new_pol ) { 
        m.coeff = Rational(
                (norm_den/m.coeff.get_denom())*
                (m.coeff.get_num()/norm_num), 1);
    }

    // build the normalized polynomial
    Polynomial *p = Polynomial::allocate(new_pol);
    Term poly_term = add_polynomial(p);

    auto it = arith_eq_map.find( poly_term );
    if( it != arith_eq_map.end() ) {
        return Term(it->second);
    }

    uint32_t pos = add_term(TERM_ARITH_EQ, bool_sort, TermBody(poly_term.index()));
    arith_eq_map[poly_term] = pos;

    return Term(pos);
}

/*
 * equality between two terms.
 *  - use an iff if they are boolean
 *  - use an arithmetic equality if the component are real
 *  - create a generic equality term otherwise.
 *
 * the pair are stored in order
 *
 * simplification:
 *   if they are the same term ~> true
 */
Term TermTable::add_binary_equality(Term x, Term y) {
    // if boolean ~> iff
    if ( sort_of(x) == bool_sort )
        return add_iff(x,y);

    // if arithmetic ~> x - y = 0
    if ( sort_of(x) == real_sort )
        return add_arith_equality(x,y);
    
    // generic simplification
    if ( x == y ) return true_term;

    // standardize
    if ( x > y ) std::swap(x,y);

    return get_binary_term(TERM_EQ, bool_sort, x, y);
}

/*
 * add a real constant, in LRA we only use rational value to identify
 * real value because it make no difference
 */
Term TermTable::add_real_constant(Rational q) {
    // search for existing constant term
    auto it = constant_map.find(q);
    if ( it != constant_map.end() ) {
        return Term(it->second);
    }

    // build a new constant term
    uint32_t pos = add_term(TERM_REAL_CONSTANT, real_sort, TermBody(q));
    constant_map[q] = pos;
    return Term(pos);
}

/*
 * equality between a list of term, with the :chainable property
 */
Term TermTable::add_equality( std::vector<Term> &args) {
    if ( args.size() == 2 )
        return add_binary_equality(args[0], args[1]);

    // (eq t0 t1 ... tn) ~> (and (eq t0 t1) (eq t1 t2) ... (eq tn-1 tn))
    for ( size_t i = 0; i < args.size()-1; ++i)
        args[i] = add_binary_equality(args[i],args[i+1]);

    // remove last argumnet, is not part of the equality
    args.pop_back();

    return add_and( args );
}

/* 
 * binary distinct
 */
Term TermTable::add_binary_distinct(Term x, Term y) {

    if ( sort_of(x) == bool_sort )
        return add_binary_xor(x,y);

    if ( sort_of(x) == real_sort )
        return add_arith_equality(x,y).opposite();

    // generic simplification
    if ( x == y ) return false_term;
    // standardize
    if ( x > y ) std::swap(x,y);

    return get_binary_term(TERM_EQ, bool_sort, x, y).opposite();
}

/*
 * distinct of an array of terms. order the args.
 * semplification:
 *   if size is 1 ~> true
 *   if size is 2 ~> binary distinct
 *   if two equal term are present ~> false
 */
Term TermTable::add_distinct( std::vector<Term> &args) {
    // special cases for small size
    if ( args.size() == 1 )
        return true_term;
    if ( args.size() == 2 )
        return add_binary_distinct(args[0], args[1]);

    // standardize
    std::sort( args.begin(), args.end() );

    for ( size_t i = 0; i < args.size()-1; ++i)
        if ( args[i] == args[i+1] )
            return false_term;

    return get_composite_term( TERM_DISTINCT, bool_sort, args );
}

/*
 * if(cond) then x else y for generic term
 * all boolean ite can be simplified furter and must be handled by a
 * specific function
 *
 * the condition is normalized to be positive, after that simplify.
 *
 * general simplification:
 *
 *  duplicate elimination
 *  (ite c x x) ~>  x
 *
 *  trivial condition elimination
 *  (ite true x y)  ~>  x
 *  (ite false x y) ~>  y
 *
 *  ite redundancy elimination
 *  (ite c (ite c x y) z)       ~> (ite c x z)
 *  (ite c x (ite c y z))       ~> (ite c x z)
 */
Term TermTable::add_ite(std::vector<Term> &args) {
    Term &cond = args[0], &x = args[1], &y = args[2];

    // special boolean ite
    if ( sort_of(x) == bool_sort )
        return add_boolean_ite(args);

    // standardize
    if ( cond.is_negative() ) {
        // ite !c x y  ~>  ite c y x
        cond = cond.opposite();
        std::swap(x,y);
    }

    // general simplifications

    if ( kind_of(x) == TERM_ITE ) {
        const auto &child = get_composite_args(x);
        if ( child[0] == cond )
            x = child[1];
    }
    if ( kind_of(y) == TERM_ITE ) {
        const auto &child = get_composite_args(y);
        if ( child[0] == cond )
            x = child[2];
    }

    if ( x == y ) return x;
    if ( cond == true_term) return x;

    return get_composite_term(TERM_ITE, sort_of(x), args);
}

/*
 * function application: contains all the argument terms followed by the
 * term that identify the function, and have the sort of the codomain
 */
Term TermTable::add_fun_app(Term fun, std::vector<Term> &args) {
    Sort s = sort_of(fun);
    Sort codomain = sort_table.get_function_codomain(s);

    args.push_back(fun);

    return get_composite_term(TERM_FUN_APPLICATION, codomain, args);
}

/*
 * build a polynomial term, this take the ownership of the Polynomial pointer
 */
Term TermTable::add_polynomial(Polynomial *q) {

    for ( const auto &m : *q ) {
        assert(m.coeff != 0);
        assert(m.indet == NULL_TERM || m.indet.is_positive());
    }

    auto it = polynomial_map.find(q);
    if (  it != polynomial_map.end() ) {
        // release unnecessary memory
        if ( q != it->first )
            Polynomial::deallocate(q);
        return Term(it->second);
    }

    for ( const auto &m : *q ) {
        assert( m.coeff != Rational(0) );
    }

    uint32_t pos = add_term(TERM_ARITH_POLY, real_sort, TermBody(q));
    polynomial_map[q] = pos;

    return Term(pos);
}

/*
 * rational number of the form (d n) where d is an integer (positive or
 * negative) and n is a positive numeral
 */
Term TermTable::add_division(const std::vector<Term> &args) {
    int num = body[args[0].index()].rational.get_num();
    int den = body[args[1].index()].rational.get_num();
    return add_real_constant(Rational(num,den));
}

void TermTable::check_add_unint_term(compact_string *n, Sort s) {
    auto it = named_map.find(n->data());
    if ( it != named_map.end() )
        throw parsing_exception("multiple declaration of term");

    check(sort_table.is_valid(s),"Uninterpreted terms must be of of a valid type");
}

/*
 * multiplication of the form (c x) or (x c) where c is a rational constant
 * and x is a real variable or a polynomial
 */
Term TermTable::add_multiplication(const std::vector<Term> &args) {
    Rational coeff;
    Term other;

    assert( args.size() == 2 );

    if ( kind[args[0].index()] == TERM_REAL_CONSTANT ) {
        coeff = body[args[0].index()].rational;
        other = args[1];
    }
    else {
        coeff = body[args[1].index()].rational;
        other = args[0];
    }

    Polynomial *p;

    // zero multiplication eliminate everything
    if ( coeff == 0 )
        return add_real_constant(Rational(0));

    if ( kind[other.index()] == TERM_REAL_CONSTANT ) {
        Rational other_coeff = body[other.index()].rational;
        return add_real_constant(coeff * other_coeff);
    }
    if ( kind[other.index()] == TERM_ARITH_POLY ) {
        // move the multiplication to the coefficent
        std::vector<Monomial> tmp;
        for ( const auto &m : *body[other.index()].polynomial )
            tmp.push_back( {m.coeff * coeff, m.indet} );

        p = Polynomial::allocate(tmp);
    }
    else {
        // generic term
        p = Polynomial::allocate({ {coeff,other} });
    }

    return add_polynomial(p);
}

Term TermTable::add_negated_arith_term( Term t ) {
    if ( kind[t.index()] == TERM_REAL_CONSTANT ) {
        // return the opposite real constant
        Rational q = -get_rational(t);
        return add_real_constant(q);
    }
    else if ( kind[t.index()] == TERM_ARITH_POLY ) {
        // reverse all the coefficent
        std::vector<Monomial> tmp;
        for ( const auto &m : *body[t.index()].polynomial ) {
            tmp.push_back( { m.coeff * Rational(-1), m.indet } );
        }

        auto p = Polynomial::allocate(tmp);
        return add_polynomial(p);
    }
    else {
        // generic real term
        auto p = Polynomial::allocate({ {Rational(-1), t} });
        return add_polynomial(p);
    }
}

Term TermTable::add_sum(const std::vector<Term> &args) {
    std::vector<Monomial> new_pol;

    for ( Term t: args ) {
        switch (kind[t.index()]) {
            case TERM_REAL_CONSTANT:
                add_monomial(new_pol, { body[t.index()].rational, NULL_TERM });
                break;

            case TERM_ARITH_POLY:
                add_monomial(new_pol, *body[t.index()].polynomial);
                break;

            default:
                assert( sort_of(t) == real_sort );
                add_monomial(new_pol, { Rational(1), t });
                break;
        }
    }

    Polynomial *p = Polynomial::allocate(new_pol);
    return add_polynomial(p);
}

Term TermTable::add_sub(const std::vector<Term> &args) {
    std::vector<Monomial> new_pol;

    // add the first as a sum
    auto it = args.begin();

    switch (kind[it->index()]) {
        case TERM_REAL_CONSTANT:
            add_monomial(new_pol, { body[it->index()].rational, NULL_TERM });
            break;

        case TERM_ARITH_POLY:
            add_monomial(new_pol, *body[it->index()].polynomial);
            break;

        default:
            assert( sort_of(*it) == real_sort );
            add_monomial(new_pol, { Rational(1), *it });
            break;
    }

    // subtract all the others
    ++it;
    for ( ;it != args.end(); ++it ) {
        switch (kind[it->index()]) {
            case TERM_REAL_CONSTANT:
                sub_monomial(new_pol, { body[it->index()].rational, NULL_TERM });
                break;

            case TERM_ARITH_POLY:
                sub_monomial(new_pol, *body[it->index()].polynomial);
                break;

            default:
                assert( sort_of(*it) == real_sort );
                sub_monomial(new_pol, { Rational(1), *it });
                break;
        }
    }

    Polynomial *p = Polynomial::allocate(new_pol);
    return add_polynomial(p);
}

/*
 * greater equal between two terms, the term can be:
 *  - arithmetic polynomial
 *  - rational/real constant
 *  - another term of real sort
 *
 * this build a normalized term of the kind x - y >= 0:
 *  - if the polynomial is only a real constant, return true if the constant
 *    is >= to 0, false otherwise
 *  - if the polynomial is of a single term of the kind a*x, simply build
 *    (a/|a|) * x >= 0
 *  - otherwise build a full polynomial, normalize the coefficent by a division
 *    of the gcd of the various coefficents
 */
Term TermTable::add_binary_ge(Term x, Term y) {

    std::vector<Monomial> new_pol;

    // add x
    if ( kind[x.index()] == TERM_REAL_CONSTANT ) {
        add_monomial(new_pol,{body[x.index()].rational,NULL_TERM}); 
    }
    else if ( kind[x.index()] == TERM_ARITH_POLY ) {
        add_monomial(new_pol,*body[x.index()].polynomial); 
    }
    else {
        assert( sort_of(x) == real_sort);
        add_monomial(new_pol,{Rational(1),x}); 
    }

    // subtract y
    if ( kind[y.index()] == TERM_REAL_CONSTANT ) {
        sub_monomial(new_pol,{body[y.index()].rational,NULL_TERM});
    }
    else if ( kind[y.index()] == TERM_ARITH_POLY ) {
        sub_monomial(new_pol,*body[y.index()].polynomial); 
    }
    else {
        assert( sort_of(y) == real_sort);
        sub_monomial(new_pol,{Rational(1),y});
    }

    if ( new_pol.size() == 0 )
        return true_term; // 0 >= 0

    // rational constant
    if ( new_pol.size() == 1 && new_pol[0].indet == NULL_TERM  ) {
        // directly make the check
        if ( new_pol[0].coeff >= 0 )
            return true_term;
        else
            return false_term;
    }

    // normalize num
    int norm_num;
    if ( new_pol.size() == 1 )
        // take the absolute value
        norm_num = std::abs(new_pol[0].coeff.get_num());
    else
        norm_num = gcd(new_pol[0].coeff.get_num(), new_pol[1].coeff.get_num());

    // for size() > 2, gcd(a,b,c) == gcd(gcd(a,b),c) ecc...
    for ( size_t i = 2; i < new_pol.size(); ++i)
        norm_num = gcd( norm_num, new_pol[i].coeff.get_num() );

    // normalize den
    int norm_den;
    if ( new_pol.size() == 1 )
        // take the absolute value
        norm_den = std::abs(new_pol[0].coeff.get_denom());
    else
        norm_den = lcm(new_pol[0].coeff.get_denom(), new_pol[1].coeff.get_denom());

    // for size() > 2, lcm(a,b,c) == lcm(lcm(a,b),c) ecc...
    for ( size_t i = 2; i < new_pol.size(); ++i)
        norm_den = lcm( norm_den, new_pol[i].coeff.get_denom() );

    // no sign normalization in >=

    // normalize all coefficent:
    //  - divide the original numerator by the gcd of all the numerators
    //  - multiply the numerator by the division of the lcm of the denumerators
    //    and the actual denumerator
    //  - set the denumerator to 1
    for ( auto &m : new_pol ) { 
        m.coeff = Rational(
                (norm_den/m.coeff.get_denom())*
                (m.coeff.get_num()/norm_num), 1);
    }

    // build the normalized polynomial
    Polynomial *p = Polynomial::allocate(new_pol);
    Term poly_term = add_polynomial(p);

    auto it = arith_ge_map.find( poly_term );
    if( it != arith_ge_map.end() ) {
        return Term(it->second);
    }

    uint32_t pos = add_term(TERM_ARITH_GE, bool_sort, TermBody(poly_term.index()));
    arith_ge_map[poly_term] = pos;

    return Term(pos);
}

/*
 * (> x y)  ~>  (not (>= y x))
 */
Term TermTable::add_binary_gt(Term x, Term y) {
    return add_binary_ge(y,x).opposite();
}

/*
 * (<= x y)  ~>  (>= y x)
 */
Term TermTable::add_binary_le(Term x, Term y) {
    return add_binary_ge(y,x);
}

/*
 * (< x y)  ~>  (not (>= x y))
 */
Term TermTable::add_binary_lt(Term x, Term y) {
    return add_binary_ge(x,y).opposite();
}



//  COMPLEX TERM ARGUMENTS
// ------------------------

/*
 * return the content of a composite term
 */
const compact_vector<Term> &TermTable::get_composite_args( Term t ) const {
    // TODO: assert that is a kind with a composite term
    return *body[t.index()].term_list_ptr;
}

uint32_t TermTable::get_integer_arg( Term t ) const {
    return body[t.index()].integer;
}

const Polynomial &TermTable::get_polynomial(Term t) const {
    TermKind k = kind[t.index()];

    assert( k == TERM_ARITH_POLY || k == TERM_ARITH_EQ || k == TERM_ARITH_GE );

    if ( k == TERM_ARITH_POLY )
        return *body[t.index()].polynomial;
    else
        return *body[body[t.index()].integer].polynomial;
}

/*
 * get the rational value of a rational term
 */
const Rational &TermTable::get_rational( Term t ) const {
    assert( kind[t.index()] == TERM_REAL_CONSTANT );
    return body[t.index()].rational;
}

/*
 * get name of named term
 */
compact_string *TermTable::get_name( Term t ) const {
    return body[t.index()].name;
}



//  TERM CHECK
// ------------

void TermTable::check(bool fact, const std::string &msg) {
    if ( ! fact )
        throw parsing_exception(msg);
}

bool TermTable::check_sort(Term t, Sort s) {
    return sort_of(t) == s;
}

bool TermTable::check_same_sort(Term t1, Term t2) {
    return sort_of(t1) == sort_of(t2);
}

bool TermTable::check_all_sort(const std::vector<Term> &args, Sort s) {
    for ( const auto & t : args )
        if ( sort_of(t) != s )
            return false;
    return true;
}

bool TermTable::check_all_equal_sort(const std::vector<Term> &args) {
    for ( const auto & t : args )
        if ( sort_of(args[0]) != sort_of(t) )
            return false;
    return true;
}

void TermTable::check_not(const std::vector<Term> &args) {
    check(args.size() == 1, "'not' of multiple arguments");
    check(check_sort(args[0], bool_sort), "'not' of nonboolean operator");
}

void TermTable::check_or(const std::vector<Term> &args) {
    check(args.size() >= 1, "'or' of zero arguments");
    check(check_all_sort(args, bool_sort),
            "'or' of non boolean argument");
    ;
}

void TermTable::check_and(const std::vector<Term> &args) {
    check(args.size() >= 1, "'and' of zero arguments");
    check(check_all_sort(args, bool_sort),
            "'and' of non boolean argument");
}

void TermTable::check_implication(const std::vector<Term> &args) {
    check(args.size() >= 2, "'=>' of less than 2 arguments");
    check(check_all_sort(args, bool_sort),
            "'=>' of non boolean argument");
}

void TermTable::check_xor(const std::vector<Term> &args) {
    check(args.size() >= 1, "'xor of zero arguments");
    check(check_all_sort(args, bool_sort),
            "'xor of non boolean argument");
}

void TermTable::check_ite(const std::vector<Term> &args) {
    check(args.size() == 3, "'ite' requires 3 arguments");
    check(check_sort(args[0], bool_sort),"'ite' with non-boolean guard");
    check(check_same_sort(args[1], args[2]),
            "different sorts for the two branch of an 'ite'");
}

/*
 * division are only between an integer (positivie or negativive) and a
 * positive numeral
 */
void TermTable::check_division(const std::vector<Term> &args) {
    check(args.size() == 2, "division of only two term");

    check(check_sort(args[0], real_sort), "division of two real value");
    check(kind[args[0].index()] == TERM_REAL_CONSTANT,
            "only division of real value");
    check(body[args[0].index()].rational.get_denom() == 1,
            "first member of division is not an integer");

    check(check_sort(args[1], real_sort), "division of two real value");
    check(kind[args[1].index()] == TERM_REAL_CONSTANT,
            "only division of real value");
    check(body[args[1].index()].rational.get_denom() == 1,
            "second member of division is not an integer");
    check(body[args[1].index()].rational > 0,
            "second member of division must be positive");
}

void TermTable::check_equality(const std::vector<Term> &args) {
    check(args.size() > 1, "= of less than 2 terms");
    check(check_all_equal_sort(args), "= of different sorts");
}

void TermTable::check_distinct(const std::vector<Term> &args) {
    check(check_all_equal_sort(args), "distinct of different sorts");
}

void TermTable::check_fun_app(const std::string &name,
        const std::vector<Term> &args) {
    Term t = get_named_term(name);
    if ( t == NULL_TERM )
        throw parsing_exception("undefined symbol " + name);
    if ( kind[t.index()] != TERM_UNINTERPRETED )
        throw parsing_exception(name + " is not a function symbol");

    Sort s = sort_of(t);

    if ( sort_table.get_function_arity(s) != args.size() )
        throw parsing_exception("mismatching number of arguments");

    for ( size_t i = 0; i < args.size(); ++i )
        if ( sort_table.get_function_domain(s)[i] != sort_of(args[i]) )
            throw parsing_exception("incompatible type");
}

void TermTable::check_negated_arith_term( Term t ) {
    check( check_sort(t,real_sort),"- require an arithmetic argument" );
}

void TermTable::check_multiplication(const std::vector<Term> &args) {
    check(args.size() == 2, "* of only two term");

    check(check_sort(args[0], real_sort), "* of non-real value");
    check(check_sort(args[1], real_sort), "* of non-real value");

    check( kind[args[0].index()] == TERM_REAL_CONSTANT ||
            kind[args[1].index()] == TERM_REAL_CONSTANT,
            "invalid arguments for *");
}

void TermTable::check_sum(const std::vector<Term> &args) {
    for ( Term t : args ) {
        check(check_sort(t, real_sort),
                "arithmetic operation of non-arith sort");
    }
}

void TermTable::check_sub(const std::vector<Term> &args) {
    for ( Term t : args ) {
        check(check_sort(t, real_sort),
                "arithmetic operation of non-arith sort");
    }
}

void TermTable::check_binary_ge(Term x, Term y) {
    check(check_sort(x, real_sort),
            "invalid sort of term for an arithmetic comparison");
    check(check_sort(y, real_sort),
            "invalid sort of term for an arithmetic comparison");
}

void TermTable::check_binary_gt(Term x, Term y) {
    check_binary_ge(x,y);
}

void TermTable::check_binary_le(Term x, Term y) {
    check_binary_ge(x,y);
}

void TermTable::check_binary_lt(Term x, Term y) {
    check_binary_ge(x,y);
}



//  POLYNOMIAL
// ------------

void add_monomial(std::vector<Monomial> &v, const Polynomial &pol) {
    std::vector<Monomial> new_pol;

    size_t i1, i2;
    for ( i1 = 0,i2 = 0; i1 < v.size() && i2 < pol.size();) {
        if ( v[i1].indet == pol[i2].indet ) {
            Rational new_coeff = v[i1].coeff + pol[i2].coeff;
            if ( new_coeff != 0 )
                new_pol.push_back( { new_coeff , v[i1].indet } );
            ++i1;
            ++i2;
        }
        else if (v[i1].indet.index() < pol[i2].indet.index())
            new_pol.push_back( v[i1++] );
        else
            new_pol.push_back( pol[i2++] );
    }

    assert( i1 == v.size() || i2 == pol.size() );
    for (; i1 < v.size(); ++i1)
        new_pol.push_back(v[i1]);
    for (; i2 < pol.size(); ++i2)
        new_pol.push_back(pol[i2]);

    std::swap(new_pol,v);
}

void sub_monomial(std::vector<Monomial> &v, const Polynomial &pol) {
    std::vector<Monomial> new_pol;

    size_t i1, i2;
    for ( i1 = 0,i2 = 0; i1 < v.size() && i2 < pol.size();) {
        if ( v[i1].indet == pol[i2].indet ) {
            Rational new_coeff = v[i1].coeff - pol[i2].coeff;
            if ( new_coeff != 0 )
                new_pol.push_back( { new_coeff , v[i1].indet } );
            ++i1;
            ++i2;
        }
        else if (v[i1].indet.index() < pol[i2].indet.index()) {
            new_pol.push_back( v[i1++] );
        }
        else {
            new_pol.push_back( { -pol[i2].coeff, pol[i2].indet } );
            ++i2;
        }
    }

    assert( i1 == v.size() || i2 == pol.size() );
    for (; i1 < v.size(); ++i1)
        new_pol.push_back(v[i1]);
    for (; i2 < pol.size(); ++i2)
        new_pol.push_back( {-pol[i2].coeff, pol[i2].indet } );

    std::swap(new_pol,v);
}

void add_monomial(std::vector<Monomial> &p, const Monomial &mono) {
    // no monomial with a zero coefficent
    if ( mono.coeff == 0 )
        return;

    // insert in an ordered way
    for ( auto it = p.begin(); it != p.end(); ++it ) {
        if ( it->indet == mono.indet ) {
            Rational new_coeff = it->coeff + mono.coeff;
            if ( new_coeff != 0 )
                it->coeff = new_coeff;
            else
                p.erase(it);
            return;
        }
        else if (it->indet.index() > mono.indet.index()) {
            p.insert(it,mono);
            return;
        }
    }
    // otherwise, append at the end
    p.push_back(mono);
}

void sub_monomial(std::vector<Monomial> &p, const Monomial &mono) {
    if ( mono.coeff == 0 )
        return;

    for ( auto it = p.begin(); it != p.end(); ++it ) {
        if ( it->indet == mono.indet ) {
            Rational new_coeff = it->coeff - mono.coeff;
            if ( new_coeff != 0 )
                it->coeff = new_coeff;
            else
                p.erase(it);
            return;
        }
        else if (it->indet.index() > mono.indet.index()) {
            p.insert(it,{-mono.coeff,mono.indet});
            return;
        }
    }
    // otherwise, append at the end
    p.push_back({-mono.coeff,mono.indet});
}

std::size_t polynomial_hasher::operator()(const PolynomialPtr &key) const {
    static const size_t hash_combiner = 0x9e3779b9;
    size_t seed = key->size();

    // based of boost hash combiner
    for( size_t i = 0; i < key->size(); ++i ) {
        const Monomial &m = key->at(i);
        seed ^= m.coeff.get_hash() + m.indet.hash() +
            hash_combiner + (seed << 6) + (seed >> 2);
    }

    return seed;
}

bool polynomial_equality::operator() (PolynomialPtr const& p1,
        PolynomialPtr const& p2) const {

    if (p1->size() != p2->size())
        return false;

    for( size_t i = 0; i < p2->size(); ++i )
        if ( p1->at(i).coeff != p2->at(i).coeff ||
                p1->at(i).indet != p2->at(i).indet )
            return false;

    return true;
}

//  PRETTY PRINTING
// -----------------

std::ostream & operator<< (std::ostream & os, Term t ) {
    if ( t == NULL_TERM )
        os << "T_NULL";
    else
        os << ( t.is_negative() ? "-":"" ) << "T_" << t.index();
    return os;
}

std::ostream & operator<< (std::ostream & os, const Monomial &m ) {
    if ( m.indet == NULL_TERM )
        os << m.coeff;
    else if ( m.coeff == 1 )
        os << m.indet;
    else
        os << "(* " << m.coeff << " " << m.indet << ")";
    return os;
}

std::ostream & operator<< (std::ostream & os, const TermTable &table ) {
    os << "=== TERM TABLE ===" << std::endl;

    for ( uint32_t i = 0; i < table.kind.size(); ++i ) {
        std::string sort_name = table.sort_table.pprint(table.sort[i]);
        Term t(i);
        os << table.pprint(t) << "   [" << t << "]: ";
        switch ( table.kind[i] ) {
            case TERM_RESERVED:
                os << "RESERVED | sort: " << table.sort[i];
                break;

            case TERM_UNUSED:
                os << "UNUSED | sort: " << sort_name << " | integer: " <<
                    table.body[i].integer;
                break;

            case TERM_BOOL_CONSTANT:
                os << "BOOL CONST | sort: " << sort_name;
                break;

            case TERM_REAL_CONSTANT:
                os << "REAL CONST | sort: " << sort_name << " | real: " <<
                    table.body[i].rational;
                break;

            case TERM_UNINTERPRETED:
                os << "UNINTERPTETED | sort: " << sort_name;
                if ( table.body[i].name )
                    os << " | name: " << table.body[i].name->data();
                break;

            case TERM_FUN_APPLICATION:
                os << "FUN APP | sort: " << sort_name << " | composite: [ ";
                for ( const auto & arg : *(table.body[i].term_list_ptr) )
                    os << arg << " ";
                os << "]";
                break;

            case TERM_ARITH_EQ:
                os << "ARITH EQ | sort: " << sort_name << " | polynomial: " <<
                    Term(table.body[i].integer);
                break;

            case TERM_ARITH_GE:
                // TODO
                os << "ARITH GE | sort: " << sort_name << " | polynomial: " <<
                    Term(table.body[i].integer);
                break;

            case TERM_OR:
                os << "OR | sort: " << sort_name << " | composite: [ ";
                for ( const auto & arg : *(table.body[i].term_list_ptr) )
                    os << arg << " ";
                os << "]";
                break;

            case TERM_XOR:
                os << "XOR | sort: " << sort_name << " | composite: [ ";
                for ( const auto & arg : *(table.body[i].term_list_ptr) )
                    os << arg << " ";
                os << "]";
                break;

            case TERM_EQ:
                os << "EQ | sort: " << sort_name << " | composite: [ ";
                for ( const auto & arg : *(table.body[i].term_list_ptr) )
                    os << arg << " ";
                os << "]";
                break;

            case TERM_DISTINCT:
                os << "DISTINCT | sort: " << sort_name << " | composite: [ ";
                for ( const auto & arg : *(table.body[i].term_list_ptr) )
                    os << arg << " ";
                os << "]";
                break;

            case TERM_ITE:
                os << "ITE | sort: " << sort_name << " | composite: [ ";
                for ( const auto & arg : *(table.body[i].term_list_ptr) )
                    os << arg << " ";
                os << "]";
                break;

            case TERM_ARITH_POLY:
                os << "POLYNOMIAL | sort: " << sort_name << " | values: [ ";
                for ( const auto & arg : *(table.body[i].polynomial) )
                    os << arg.coeff << "*" << arg.indet << " ";
                os << "]";
                break;

            default:
                os << "NO TERM";
        }
        os << std::endl;
    }
    os << "==================" << std::endl;
    return os;
}

std::string print_with_negation(Term t, const std::string &s) {
    if ( t.is_negative() )
        return "(not " + s + ")";
    else
        return s;
}

// pretty printing of a term, with all the extra info of a term table
std::string TermTable::pprint(Term t) const {
    if ( t == NULL_TERM )
        return "NULL";

    std::ostringstream oss;
    oss << t;
    std::string t_str = oss.str();

    // print single term
    switch ( kind_of(t) ) {
        case TERM_RESERVED:
        case TERM_UNUSED:
            return t_str;

        case TERM_BOOL_CONSTANT:
            return t.is_negative() ? "false" : "true";

        case TERM_REAL_CONSTANT: {
            std::ostringstream oss;
            oss << body[t.index()].rational;
            return oss.str();
        }

        case TERM_UNINTERPRETED: {
            auto name = get_name(t);
            if ( name != nullptr )
                return print_with_negation(t,std::string(name->data(), name->size()));
            else
                return t_str;
        }

        default:
            // composite term
            break;
    }

    // print a composite term
    std::string ret = "(";
    switch ( kind_of(t) ) {

        case TERM_FUN_APPLICATION: {
            const auto &comp = get_composite_args(t);
            Term fun = comp[comp.size()-1];

            auto name = get_name(fun);
            if ( name != nullptr )
                ret += std::string(name->data(), name->size());
            else
                ret += t_str;

            for ( size_t i = 0; i < comp.size()-1; ++i )
                ret += " " + pprint(comp[i]);
            break;
        }

        case TERM_ARITH_EQ:
            ret += "= ";
            ret += pprint( Term(body[t.index()].integer) );
            ret += " 0";
            break;

        case TERM_ARITH_GE:
            ret += ">= ";
            ret += pprint(Term(body[t.index()].integer));
            ret += " 0";
            break;

        case TERM_OR: {
            const auto &comp = get_composite_args(t);
            ret += "or";
            for ( size_t i = 0; i < comp.size(); ++i )
                ret += " " + pprint(comp[i]);
            break;
        }

        case TERM_XOR: {
            const auto &comp = get_composite_args(t);
            ret += "xor";
            for ( size_t i = 0; i < comp.size(); ++i )
                ret += " " + pprint(comp[i]);
            break;
        }

        case TERM_EQ: {
            const auto &comp = get_composite_args(t);
            ret += "=";
            for ( size_t i = 0; i < comp.size(); ++i )
                ret += " " + pprint(comp[i]);
            break;
        }

        case TERM_DISTINCT: {
            const auto &comp = get_composite_args(t);
            ret += "disinct";
            for ( size_t i = 0; i < comp.size(); ++i )
                ret += " " + pprint(comp[i]);
            break;
        }

        case TERM_ITE: {
            const auto &comp = get_composite_args(t);
            ret += "ite";
            for ( size_t i = 0; i < comp.size(); ++i )
                ret += " " + pprint(comp[i]);
            break;
        }

        case TERM_ARITH_POLY: {
            const auto &poly = *body[t.index()].polynomial;
            ret += "+";
            for ( const auto &m: poly ) {
                if ( m.indet == NULL_TERM ) {
                    std::ostringstream oss;
                    oss << m.coeff; // rational number
                    ret += " " + oss.str();
                }
                else if ( m.coeff != 1 ) {
                    std::ostringstream oss;
                    oss << m.coeff; // rational number
                    ret += " (* " + oss.str() + " " + pprint(m.indet) + ")";
                }
                else
                    ret += " " + pprint(m.indet);
            }
            break;
        }

        default:
            // no special printing
            return t_str;
    }

    ret+=")";
    return print_with_negation(t,ret);
}

