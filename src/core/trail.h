/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_CORE_TRAIL_HH
#define EOS_CORE_TRAIL_HH

#include "assignment.h"
#include "variable.h"
#include "variable.h"
#include "term.h"
#include "sort.h"
#include "module_id.h"
#include <cassert>

enum trail_kind_t {
    TRAIL_DECIDED,
    TRAIL_PROPAGATED,
    TRAIL_UNASSIGNED
};

class Trail {
public:
    Trail(const VariableTable &vt, const SortTable &st) :
        variable_table(vt), sort_table(st) {};
    ~Trail() = default;

    // get notified when a new variable is added
    void notify_new_variable( Variable var );

    // return a reference to the current assignment
    const Assignment &get_assignment() const;

    // trail consistency
    bool is_consistent() const;
    void set_inconsistent(bool i);

    // trail query
    size_t size() const;
    size_t get_current_level() const;
    bool   is_at_level_zero() const;
    bool   completly_assigned() const;

    // variable query
    Variable   at(size_t pos) const;
    Variable   back() const;
    uint32_t     get_timestamp(Variable var) const;
    size_t       get_level( Variable var ) const;
    size_t       get_position_in_trail( Variable var ) const;
    trail_kind_t get_kind( Variable var ) const;
    const ModuleId  &get_module_id( Variable var ) const;

    // value query
    bool    has_value(Variable var) const;
    bool    has_cached_value(Variable var) const;
    Value get_value(Variable var) const;
    Value get_cached_value(Variable var) const;
    bool    get_boolean_value(Variable var) const;

    // add new assignment (of any sort)
    void add_decision(Variable var, const Value &val,const ModuleId &module_id);
    // add a new propagation (of boolean sort)
    void add_propagation(Variable var, bool value, const ModuleId &module_id, size_t level);

    // pop
    void pop_propagation();
    void pop_decision();
    void pop_last_level(std::vector<Variable> &undo);

private:

    const size_t   NO_LEVEL    = ~static_cast<size_t>(0);
    const size_t   NO_POSITION = ~static_cast<size_t>(0);

    void add_value(Variable var, const Value &val,
            const ModuleId &module_id, trail_kind_t kind, size_t level);
    void open_decision_level();
    void undo_decision_level();
    void undo_value(Variable var);

    std::vector<Variable> elements;  // element decided/propagated in the trail
    std::vector<size_t> level_size;    // mark the size of each levels
    size_t current_level = 0;

    Assignment assignment;
    const VariableTable &variable_table;
    const SortTable &sort_table;

    // info on all variables
    std::vector<size_t>       position_in_trail = {NO_POSITION};
    std::vector<trail_kind_t> kinds = {TRAIL_UNASSIGNED};
    std::vector<size_t>       levels = {NO_LEVEL};
    std::vector<ModuleId>  module_ids = {ModuleId()};


    bool inconsistent = false;

    // buffers
    std::vector<Variable> buffer_to_repropagate;

    friend std::ostream &operator<< (std::ostream & os, const Trail &t);
};

#endif // EOS_CORE_TRAIL_HH
