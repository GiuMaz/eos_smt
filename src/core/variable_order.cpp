/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "variable_order.h"

VariableOrder::VariableOrder(VariableTable &vt, double df) :
    variable_table(vt),
    variable_decay_factor(df)
{}

Variable VariableOrder::get_var_for_decision() {
    // if there are no more variables return NULL_VAR
    if ( empty() )
        return NULL_VAR;

    if ( size() == 1 ) {
        // simple pop, no update required
        Variable max = order.front();
        assert(map_position[max.index()] == 0);

        map_position[max.index()] = NO_POS;

        order.pop_back();

        return max;
    }

    // pop max value and update the heap
    Variable max = order.front();
    assert(map_position[max.index()] == 0);

    // move the last value at the beginning, replacing the max
    order.front() = order.back();
    map_position[max.index()] = NO_POS;
    map_position[order.front().index()] = 0;
    order.pop_back();

    // heapify the moved variable
    heapify(0);

    // return the max
    return max;
}

void VariableOrder::increase_activity( Variable var ) {
    // increase activity
    activity[var.index()] += variable_activity_update;

    // if inside the heap, reorder the variable
    if ( map_position[var.index()] != NO_POS )
        increase_key(map_position[var.index()]);
}

void VariableOrder::insert(Variable var) {

    // resize if the variable is a new variable
    assert( map_position.size() == activity.size() );
    if ( map_position.size() <= var.index() ) {
        map_position.resize(var.index()+1, NO_POS);
        activity.resize(var.index()+1, variable_activity_update);
    }

    // already inserted?
    else if ( map_position[var.index()] != NO_POS )
        return;

    // append at the end of the ordering
    map_position[var.index()] = order.size();
    order.push_back(var);

    // renormalize the last element
    increase_key(map_position[var.index()]);
}

void VariableOrder::variable_activity_decay() {
    // if a big value is reached, a normalization is required
    if ( variable_activity_update > 1e100 ) {
        for ( auto & a : activity )
            a /= 1e100;
        variable_activity_update/=1e100;
    }

    // don't decay all activity, simply increase the updating unit
    variable_activity_update *= variable_decay_factor;
}

VariableOrder::size_type VariableOrder::size() {
    return order.size();
}

bool VariableOrder::empty() {
    return order.empty();
}

/*
 * tree navigation.
 * It should be noted that this implementation of a binary heap is based on
 * the CLRS, but the heap is 0 based and not 1 based here.
 */
VariableOrder::size_type VariableOrder::father(VariableOrder::size_type i) {
    return (i-1) >> 1;
}

VariableOrder::size_type VariableOrder::left(VariableOrder::size_type i)   {
    return (i<<1) + 1;
}

VariableOrder::size_type VariableOrder::right(VariableOrder::size_type i)  {
    return (i+1) << 1;
}

void VariableOrder::heapify(VariableOrder::size_type i) {

    // find if one of the child is greater than the fater
    while ( true ) {
        auto largest = i;
        if (left(i) < size() &&
                activity[order[largest].index()] < activity[order[left(i)].index()])
            largest = left(i);
        if (right(i) < size() &&
                activity[order[largest].index()] < activity[order[right(i)].index()])
            largest = right(i);

        // swap father and child if a child is greater
        if ( largest != i ) {
            // swap the father with the largest child (the other child is correct)
            std::swap(order[i],order[largest]);

            // swap the position mapping
            std::swap(map_position[order[i].index()],
                    map_position[order[largest].index()]);

            // now largest store the lower value, heapify it
            //heapify(largest);
            i = largest;
        }
        else return;
    }
}

void VariableOrder::increase_key( VariableOrder::size_type pos ) {
    // move up an increased value
    while (pos > 0 &&
            activity[order[father(pos)].index()] < activity[order[pos].index()]) {

        // swap father and son
        std::swap(order[pos],order[father(pos)]);
        // swap mapping
        std::swap(map_position[order[pos].index()],
                map_position[order[father(pos)].index()]);

        // move to the father
        pos = father(pos);
    }
}

std::ostream &operator<<(std::ostream &os, VariableOrder &vo) {
    os << "=== ORDERING ===" << std::endl;

    for (size_t i = 0;  i < vo.order.size(); ++i) {
        const Variable &v = vo.order[i];
        os << "var " <<  vo.variable_table.pprint(v) <<
            " activity " << vo.activity[v.index()] << std::endl;
    }

    os << "=== DETAILS ====" << std::endl;
    for (size_t i = 0;  i < vo.map_position.size(); ++i) {
        Variable v(i);
        os << "VAR: " << vo.variable_table.pprint(v) << " POS: " << 
            (vo.map_position[v.index()] == vo.NO_POS ? "-1" : std::to_string(vo.map_position[v.index()])) <<
            " ACTIVITY: " << vo.activity[v.index()] << std::endl;
    }

    os << "================" << std::endl;
    return os;
}
