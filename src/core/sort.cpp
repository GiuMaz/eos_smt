/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sort.h"
#include "error_handling.h"
#include <sstream>

//  SORTS HASH TABLE
// ------------------

size_t function_signature_hasher::operator()(const function_signature_t *key) const {
    static const size_t hash_combiner = 0x9e3779b9;
    size_t seed = key->size();
    // based of boost hash combiner
    for( const auto &k : *key )
        seed ^= k.hash() + hash_combiner + (seed << 6) + (seed >> 2);
    return seed;
}

bool function_signature_equality::operator() (
        function_signature_t const* t1, function_signature_t const* t2) const {
    if ( t1->size() != t2->size() )
        return false;
    for ( size_t i = 0; i < t1->size(); ++i )
        if ( t1->at(i) != t2->at(i) )
            return false;
    return true;
}

size_t named_hasher::operator()(const char *key) const {
    size_t hash = 5381;
    while ( *key != '\0' ) {
        hash = ((hash << 5) + hash) + *key;
        ++key; // NOLINT
    }
    return hash;
}

bool named_equality::operator() (
        const char *t1, const char *t2) const {
    if ( t1 == t2 )
        return true;

    size_t i = 0;
    do {
        if ( t1[i] != t2[i] )   // NOLINT
            return false;
        ++i;
    } while ( t1[i] != '\0' );  // NOLINT

    return t1[i] == t2[i];      // NOLINT
}



//  SORT TABLE
// ------------

// it MUST reserve the 0th position and put Bool in the 1st and Real in the 2nd
// position with the appropriate kind and body
SortTable::SortTable():
    kind({NO_SORT, BOOL_SORT, REAL_SORT}),
    body({{},{},{}}) {

        // insert "Bool"
        body[1].named_sort = compact_string::allocate("Bool");
        named_map[body[1].named_sort->data()] = bool_sort;

        // insert "Real"
        body[2].named_sort = compact_string::allocate("Real");
        named_map[body[2].named_sort->data()] = real_sort;

        // check assumptions
        assert( kind.size() == 3 );
        assert( body.size() == 3 );
        assert( named_map.size() == 2 );
        assert( function_map.size() == 0 );
        assert( bool_sort == get_named_sort("Bool") );
        assert( real_sort == get_named_sort("Real") );
    }

Sort SortTable::declare_named_sort(compact_string *name) {
    auto it = named_map.find(name->data());
    if ( it != named_map.end() ) // already inserted
        throw invalid_command_exception("multiple declaration of sort");

    Sort s(kind.size());

    kind.emplace_back(UNINTERPRETED_SORT);
    body.emplace_back(SortBody(name));

    named_map[name->data()] = s;
    return s;
}

Sort SortTable::get_named_sort(const char *name) const {
    auto it = named_map.find(name);
    if ( it != named_map.end() )
        return it->second;
    return NULL_SORT;
}

Sort SortTable::get_named_sort(const compact_string *name) const {
    return get_named_sort(name->data());
}

Sort SortTable::get_named_sort(const std::string &name) const {
    return get_named_sort(name.c_str());
}

Sort SortTable::add_function_signature(function_signature_t *s) {
    auto it = function_map.find(s);
    if ( it != function_map.end() ) {
        return it->second; // already inserted
    }

    Sort pos(kind.size());
    kind.emplace_back(FUNCTION_SORT);
    body.emplace_back(SortBody(s));
    function_map[body[pos.index()].function_signature] = pos;

    return pos;
}

Sort SortTable::get_function_codomain( Sort fun ) const {
    assert( kind[fun.index()] == FUNCTION_SORT );
    const auto &ref = body[fun.index()].function_signature;
    return ref->at( ref->size()-1 ); // last element
}

uint32_t SortTable::get_function_arity( Sort fun ) const {
    assert( kind[fun.index()] == FUNCTION_SORT );
    return body[fun.index()].function_signature->size()-1;
}

Sort *SortTable::get_function_domain( Sort fun ) const {
    assert( kind[fun.index()] == FUNCTION_SORT );
    return body[fun.index()].function_signature->data();
}

bool SortTable::is_function( Sort s ) const {
    return kind[s.index()] == FUNCTION_SORT;
}

bool SortTable::is_uninterpreted( Sort s ) const {
    return kind[s.index()] == UNINTERPRETED_SORT;
}

bool SortTable::is_boolean( Sort s ) const {
    return kind[s.index()] == BOOL_SORT;
}

bool SortTable::is_real( Sort s ) const {
    return kind[s.index()] == REAL_SORT;
}

bool SortTable::is_valid( Sort s ) const {
    return s.index() < kind.size() && kind[s.index()] != NO_SORT;
}

SortKind SortTable::kind_of(Sort s) const {
    assert( s.index() < kind.size() );
    return kind[s.index()];
}

std::ostream & operator<< (std::ostream & os, Sort s ) {
    if ( s == NULL_SORT )
        os << "S_NULL";
    else
        os << "S_" << s.index();
    return os;
}

const compact_string * SortTable::get_name(Sort s) const {
    assert( kind_of(s) == UNINTERPRETED_SORT );
    return body[s.index()].named_sort;
}

// print of sort table
std::ostream & operator<< (std::ostream & os, const SortTable &table ) {
    os << "=== SORT TABLE ===" << std::endl;

    // print all registrered sort
    for ( uint32_t i = 0; i < table.kind.size(); ++i ) {

        // build sort
        Sort sort(i);

        os << table.pprint(sort) << "   [" << sort << "]: ";
        switch ( table.kind[i] ) {
            case NO_SORT:
                os << "NO SORT";
                break;
            case BOOL_SORT:
                os << "BOOLEAN";
                break;
            case REAL_SORT:
                os << "REAL";
                break;
            case UNINTERPRETED_SORT:
                os << "UNINTERPTED | name: " <<
                    (table.body[i].named_sort->data());
                break;
            case FUNCTION_SORT:
                os << "FUNCTION | signature: [ ";
                for ( size_t i = 0; i < table.get_function_arity(sort); ++i )
                    os << table.get_function_domain(sort)[i] << " ";
                os << "] -> " << table.get_function_codomain(sort);
                break;
            default:
                os << "NO KIND";
        }

        os << std::endl;
    }

    os << "==================" << std::endl;
    return os;
}

std::string SortTable::pprint(Sort s) const {
    if ( s == NULL_SORT ) return "NULL";

    // save a simple representation of the sort if no extra info are available
    std::ostringstream oss;
    oss << s;
    std::string s_str = oss.str();

    switch ( kind_of(s) ) {
        case BOOL_SORT:
            return "bool";
        case REAL_SORT:
            return "real";
        case UNINTERPRETED_SORT:
            return std::string(get_name(s)->data());
        case FUNCTION_SORT: {
            std::string ret = "( ";
            for ( size_t i = 0; i < get_function_arity(s); ++i )
                ret += pprint(get_function_domain(s)[i]) + " ";
            ret += ") -> " + pprint(get_function_codomain(s));
            return ret;
        }
        case NO_SORT:
        default:
            return s_str;
    }
}
