/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_CORE_PRINTING_HH
#define EOS_CORE_PRINTING_HH

#include <iostream>
#include <vector>
#include <set>
#include <unordered_set>

// debug and standard stream

extern std::ostream standard;
extern std::ostream debug;

// TODO: maybe set stream to file
void set_default_standard_stream();
void set_default_debug_stream();

// global debug level
enum message_level_t {
    NO_MESSAGE = 0,
    STANDARD_MESSAGE = 1,
    VERBOUSE_MESSAGE = 2
}; 

extern message_level_t global_message_level;

// set the global debug level of the program
void set_message_level( message_level_t lv );

// print vectors
template < class T >
inline std::ostream& operator<<(std::ostream& os, const std::vector<T>& v) 
{
    os << "[";
    for ( const auto &i : v)
    {
        os << " " << i;
    }
    os << " ]";
    return os;
}

template < class T >
inline std::ostream& operator<<(std::ostream& os, const std::unordered_set<T>& v) 
{
    os << "[";
    for ( const auto &i : v)
    {
        os << " " << i;
    }
    os << " ]";
    return os;
}
template < class T, class U >
inline std::ostream& operator<<(std::ostream& os, const std::unordered_set<T,U>& v) 
{
    os << "[";
    for ( const auto &i : v)
    {
        os << " " << i;
    }
    os << " ]";
    return os;
}
template < class T, class U, class V >
inline std::ostream& operator<<(std::ostream& os, const std::unordered_set<T,U,V>& v) 
{
    os << "[";
    for ( const auto &i : v)
    {
        os << " " << i;
    }
    os << " ]";
    return os;
}

// print multiset
template < class T >
inline std::ostream& operator<<(std::ostream& os, const std::unordered_multiset<T>& v) 
{
    os << "[";
    for ( const auto &i : v)
    {
        os << " " << i;
    }
    os << " ]";
    return os;
}
template < class T, class U >
inline std::ostream& operator<<(std::ostream& os, const std::unordered_multiset<T,U>& v) 
{
    os << "[";
    for ( const auto &i : v)
    {
        os << " " << i;
    }
    os << " ]";
    return os;
}
template < class T, class U, class V >
inline std::ostream& operator<<(std::ostream& os, const std::unordered_multiset<T,U,V>& v) 
{
    os << "[";
    for ( const auto &i : v)
    {
        os << " " << i;
    }
    os << " ]";
    return os;
}
#ifndef NDEBUG

#define MESSAGE_LEVEL(X) \
    global_message_level >= X

#else

#define MESSAGE_LEVEL(X) \
    false

#endif // NDEBUG

#endif // EOS_CORE_PRINTING_HH
