/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_CORE_VALUE_HH
#define EOS_CORE_VALUE_HH

#include <cassert>
#include <iostream>
#include "rational.h"
#include "sort.h"

/**
 * propositional and first order values
 */

enum ValueKind {
    NO_VALUE,
    BOOLEAN_VALUE,
    RATIONAL_VALUE,
    UNINTERPRETED_VALUE
};

class UninterpretedValue {
    Sort sort;
    uint32_t id;
public:
    UninterpretedValue(const UninterpretedValue &) = default;
    UninterpretedValue(Sort s, uint32_t i): sort(s), id(i) {}
    UninterpretedValue & operator=(const UninterpretedValue &) = default;

    // comparison operator
    bool operator==(const UninterpretedValue &rhs) const {
        return sort.index() == rhs.sort.index() && id == rhs.id;
    }
    bool operator!=(const UninterpretedValue &rhs) const {
        return sort.index() == rhs.sort.index() && id == rhs.id;
    }

    uint32_t get_id() const { return id; }
    Sort get_sort() const { return sort; }
};

const static UninterpretedValue NULL_UNINT(NULL_SORT,0);

std::ostream &operator<< (std::ostream &os, UninterpretedValue val);

union ValueBody {
    bool boolean;
    Rational rational;
    UninterpretedValue unint;

    ValueBody() noexcept: boolean(false) {}
    ValueBody(bool b) noexcept: boolean(b) {}
    ValueBody(Rational q) noexcept: rational(q) {}
    ValueBody(Sort sort,uint32_t id): unint(UninterpretedValue(sort,id)) {}
    ValueBody(const UninterpretedValue &v): unint(v) {}
};

class Value  {
public:
    Value() noexcept : kind(NO_VALUE), body(false) {}
    explicit Value(bool b) noexcept: kind(BOOLEAN_VALUE), body(b) {}
    explicit Value(Rational q) noexcept: kind(RATIONAL_VALUE), body(q) {}
    Value(Sort s, uint32_t id) noexcept: kind(UNINTERPRETED_VALUE), body(s,id) {}
    Value(const UninterpretedValue &v) noexcept: kind(UNINTERPRETED_VALUE), body(v) {}

    Value(const Value &) = default;
    Value(Value &&) = default;

    Value & operator= (const Value &) = default;

    bool operator== ( const Value &rhs ) const;
    bool operator!= ( const Value &rhs ) const;

    size_t hash() const;

    bool is_zero() const;
    bool is_true() const;
    bool is_false() const;

    bool is_boolean() const;
    bool is_rational() const;
    bool is_uninterpreted() const;

    bool   get_boolean_value() const;
    Rational get_rational_value() const;
    UninterpretedValue get_uninterpreted() const;

    std::string pprint(const SortTable &st) const;

    ValueKind get_kind() const;

private:

    ValueKind kind;
    ValueBody body;
};

std::ostream &operator<< (std::ostream &os, Value val);

// usefull global constant
static const Value NULL_VALUE;
static const Value true_value(true);
static const Value false_value(false);

#endif // EOS_CORE_VALUE_HH
