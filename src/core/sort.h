/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_CORE_SORT_H
#define EOS_CORE_SORT_H

#include <cassert>

#include "compact_vector.h"

#include <string>
#include <unordered_map>
#include <vector>
#include <iostream>

/**
 * Sorts
 *
 * A sort is implemented as an integer index, a SortTable contains all the
 * information related to the index.
 * By default only 3 sorts are defined: NULL_SORT, Bool and Real. It is possible
 * to declare new sort, but every sort requires a different name.
 * It is also possible to build composite sort for functions.
 *
 * Sort can be of different kind:
 *     NO_SORT:            for impossible sort
 *     BOOL_SORT:          the standard "Bool"
 *     REAL_SORT:          the standard "Real"
 *     UNINTERPRETED_SORT: for simple type, associated to a name
 *     FUNCTION_SORT:      for function signature, they have an arity n,
 *                         n Sort for the domain and a Sort for the codomain
 *
 * Some Sort have a body associated to them when they requires more information:
 *     BOOL_SORT:          have the name "Bool"
 *     REAL_SORT:          have the name "Real"
 *     UNINTERPRETED_SORT: have a user defined name
 *     FUNCTION_SORT:      have a vector with the domain sort followed by the
 *                         codomain sort [ d[0] .... d[n] c ]. it's size is n+1
 *                         where n is the arity of the domain
 *
 *  This values are hash-consed to avoid multiple declaration.
 *
 * finally some usefull global variable are defined:
 *     NULL_SORT:  used to rappresent term that is impossible to sort
 *     bool_sort:  a shorthand for the boolean sort
 *     real_sort:  a shorthand for the real sort
 */

// the Sort type
class Sort {
    uint32_t value = 0;
public:

    Sort() = default;
    Sort(const Sort &) = default;
    Sort(Sort &&) = default;
    explicit Sort(uint32_t v) noexcept : value(v) {}

    ~Sort() = default;

    Sort &operator=(const Sort &) = default;
    Sort &operator=(Sort &&) = default;

    uint32_t index() const { return value; };
    size_t   hash()  const { return static_cast<size_t>(value); };

    bool operator==(const Sort &rhs) const { return value == rhs.value; }
    bool operator!=(const Sort &rhs) const { return value != rhs.value; }

};

std::ostream & operator<< (std::ostream & os, Sort s );

static_assert(sizeof(Sort) == 4,"uint32_t must have 4 byte size");

enum SortKind {
    // atomic
    NO_SORT,
    BOOL_SORT,
    REAL_SORT,
    UNINTERPRETED_SORT,

    // composite
    FUNCTION_SORT,

    // count
    TOTAL_SORT_KIND
};

// vector of the form [ d[0] .... d[n] c ] for a function
// of signature  d[0] x ... x d[n] -> c
using function_signature_t =  compact_vector<Sort>;

// possible content of a sort
union SortBody {
    int integer; // right now is an unused placeolder
    function_signature_t *function_signature;
    compact_string  *named_sort;

    SortBody() : integer(0) {}
    explicit SortBody(int i) : integer(i) {}
    explicit SortBody(compact_string *p) : named_sort(p) {}
    explicit SortBody(function_signature_t *p) : function_signature(p) {}
};

// operator for the hash table that store the function sorts
struct function_signature_hasher {
    std::size_t operator()(const function_signature_t *key) const;
};

struct function_signature_equality {
    bool operator() (const function_signature_t *t1, const function_signature_t *t2) const;
};

using FunctionSortTable = std::unordered_map<function_signature_t *,
      Sort, function_signature_hasher, function_signature_equality> ; 

// operator for the hash table that store named sort
struct named_hasher {
    std::size_t operator()(const char *key) const;
};

struct named_equality {
    bool operator() (const char *t1, const char *t2) const;
};

using SortNamedTable =  std::unordered_map<const char *, Sort, named_hasher, named_equality>;

/**
 * Sort Table
 * This class contains all the information related to sorts.
 *
 * - kind: contains the kind of sorts (boolean, function ecc)
 * - body: contains the name for named sorts or the composition of sorts for
 *         function sorts
 * - named_map:    maps string to Sort
 * - function_map: maps function (arity,domain,codomain) to Sort
 *
 * all the SortsTables are initialized with the NULL_SORT, bool_sort
 * and real_sort
 */
class SortTable {

public:
    SortTable();

    // named sort: UNINTERPRETED_SORT, BOOL_SORT, REAL_SORT
    Sort declare_named_sort(compact_string *name);

    Sort get_named_sort(const compact_string *name) const;
    Sort get_named_sort(const char *name) const;
    Sort get_named_sort(const std::string &name) const;

    const compact_string *get_name(Sort s) const;

    // function sort: FUNCTION_SORT
    Sort   add_function_signature(function_signature_t *s);
    Sort   get_function_codomain( Sort fun ) const;
    uint32_t get_function_arity( Sort fun ) const;
    Sort  *get_function_domain( Sort fun ) const;

    // kind check
    bool is_boolean( Sort s ) const;
    bool is_real( Sort s ) const;
    bool is_uninterpreted( Sort s ) const;
    bool is_function( Sort s ) const;
    bool is_valid( Sort s ) const;

    SortKind kind_of(Sort s) const;

    std::string pprint(Sort s) const;

private:

    // property
    std::vector<SortKind> kind;
    std::vector<SortBody> body;

    // maps
    SortNamedTable    named_map;
    FunctionSortTable function_map;

    friend std::ostream &operator<< (std::ostream & os, const SortTable &s );
};

// usefull global variable
static const Sort NULL_SORT;
static const Sort bool_sort(1);
static const Sort real_sort(2);


#endif // EOS_CORE_SORT_H
