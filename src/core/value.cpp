/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "value.h"
#include <sstream>

size_t Value::hash() const {
    switch ( kind ) {
        case BOOLEAN_VALUE:
            return body.boolean;
        case RATIONAL_VALUE:
            return body.rational.get_hash();
        case UNINTERPRETED_VALUE:
            return (static_cast<size_t>(body.unint.get_sort().index())<<32) +
                body.unint.get_id();
        case NO_VALUE:
            return 0;
    }
}

bool Value::operator== ( const Value &rhs ) const {
    if ( kind != rhs.kind )
        return false;
    switch ( kind ) {
        case BOOLEAN_VALUE:
            return body.boolean == rhs.body.boolean;
        case RATIONAL_VALUE:
            return body.rational == rhs.body.rational;
        case UNINTERPRETED_VALUE:
            return body.unint == rhs.body.unint;
        case NO_VALUE:
            return body.boolean == rhs.body.boolean;
    }
}

bool Value::operator!= ( const Value &rhs ) const {
    return ! operator==(rhs);
}

bool Value::is_zero() const {
    switch ( kind ) {
        case RATIONAL_VALUE:
            return body.rational == 0;
        default:
            return false;
    }
}

bool Value::is_true() const {
  return kind == BOOLEAN_VALUE && body.boolean;
}

bool Value::is_false() const {
  return kind == BOOLEAN_VALUE && !body.boolean;
}

bool Value::is_boolean() const {
  return kind == BOOLEAN_VALUE;
}

bool Value::is_rational() const {
    return kind == RATIONAL_VALUE;
}

bool Value::is_uninterpreted() const {
    return kind == UNINTERPRETED_VALUE;
}

bool Value::get_boolean_value() const {
    assert( kind == BOOLEAN_VALUE );
    return body.boolean;
}

Rational Value::get_rational_value() const {
    assert( kind == RATIONAL_VALUE );
    return body.rational;
}

UninterpretedValue Value::get_uninterpreted() const {
    assert( kind == UNINTERPRETED_VALUE );
    return body.unint;
}

ValueKind Value::get_kind() const {
    return kind;
}

std::ostream &operator<< (std::ostream &os, UninterpretedValue val) {
    os << val.get_id() << "#" << val.get_sort();
    return os;
}

std::ostream &operator<< (std::ostream &os, Value val) {
    switch ( val.get_kind() ) {

        case BOOLEAN_VALUE:
            os << (val.get_boolean_value() ? "true" : "false");
            break;

        case RATIONAL_VALUE:
            os << val.get_rational_value();
            break;

        case UNINTERPRETED_VALUE:
            os << val.get_uninterpreted();
            break;

        case NO_VALUE:
        default:
            os << "NO_VALUE";
            break;
    }
    return os;
}

std::string Value::pprint(const SortTable &st) const {

    if ( *this == NULL_VALUE )
        return "NULL";

    switch ( get_kind() ) {

        case BOOLEAN_VALUE:
            return get_boolean_value() ? "true" : "false";

        case RATIONAL_VALUE: {
            std::ostringstream oss;
            oss << get_rational_value();
            return oss.str();
        }

        case UNINTERPRETED_VALUE: {
            std::string ret;
            auto un_var = get_uninterpreted();
            ret += std::to_string(un_var.get_id());
            ret += "#" + st.pprint(un_var.get_sort());
            return ret;
        }

        case NO_VALUE:
        default:
            return "NO_VALUE";
    }
}

