/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SMT_CORE_VARIABLE_ORDER_HH
#define SMT_CORE_VARIABLE_ORDER_HH

#include <stdint.h>
#include "variable.h"

/**
 * implementation of the VSIDS heuristic for variable selection
 */
class VariableOrder {
public:
    using size_type = uint32_t;

    explicit VariableOrder(VariableTable &vt, double df = (1.0/0.95) );

    /*
     * get the variable with the higher activity, if there are no more variables
     * this method return NULL_VAR
     */
    Variable get_var_for_decision();

    /*
     * increase the activity of the given variable of a unit increment
     */
    void increase_activity( Variable var );

    /*
     * insert a variable inside the ordering, if the variable is already
     * inserted this has no effect.
     */
    void insert(Variable var);

    /*
     * decay of all activitys
     */
    void variable_activity_decay();

    /*
     * return the size of the binary heap, thus the number of variable to
     * process. This usually is not the same as the variable of the problem
     */
    size_type size();

    /*
     * return true iff there are no more variable to process inside the
     * binary heap
     */
    bool empty();

private:

    /*
     * return the position of the father of a given position of the binary heap
     */
    size_type father(size_type i);

    /*
     * return the position of the left child of a given position of the binary heap
     */
    size_type left(size_type i);

    /*
     * return the position of the right child of a given position of the binary heap
     */
    size_type right(size_type i);

    /*
     * given a position i in the binary heap this method assure that the two
     * child (if they exist) have lesser activity than i. If this is not true
     * the position i is moved and a recursive call to heapify is used the
     * currect the rest of the heap.
     */
    void heapify(size_type i);

    /*
     * if the activity of the position pos increase this method restore
     * the proper order in the heapify exchanging father and son if required.
     */
    void increase_key( size_type pos );

    /*
     * reference to the variable table, only used for printing
     */
    VariableTable &variable_table;

    /*
     * this vector store the activity of all the variable in the problem.
     */
    std::vector<double>    activity;

    /*
     * this vector map the index of a variable to the position inside the
     * ordered binary heap. if a variable is already processed so it is not
     * inside the heap it store a NO_POS value.
     */
    std::vector<size_type> map_position;
    const size_type NO_POS = UINT32_MAX;

    /*
     * a binary heap with the variable ordered by descending activity.
     * It contains only variable not already processed, but it is possible
     * that some of the variables are actualy assigned in the trail
     */
    std::vector<Variable> order;

    /*
     * constant factor that dictate how much the activity should decrease
     * when a decay activity is called
     */
    const double variable_decay_factor;

    /*
     * unit of increment in activity applied when an increase activity action
     * is called
     */
    double variable_activity_update = 1.0;

    /*
     * pretty printing utility
     */
    friend std::ostream &operator<<(std::ostream &os, VariableOrder &v);
};

std::ostream &operator<<(std::ostream &os, VariableOrder &v);

#endif // SMT_CORE_VARIABLE_HEAP_HH
