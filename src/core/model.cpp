/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "model.h"
#include "term.h"
#include "sort.h"
#include "variable.h"
#include "trail.h"
#include "trail.h"

#include <unordered_map>

std::ostream &operator<<(std::ostream &os, const Model &m) {
    for ( const auto &t : m.all_term ) {
        // print only variable with a name
        if ( m.term_table.get_name(t) ){
            os << "(= " <<  (m.term_table.get_name(t))->data()
                << " " << m.term_to_val.at(t).pprint(m.sort_table) << 
                ")" << std::endl;
        }
    }
    return os;
}

void Model::add_from_trail(const Trail &trail) {
    // add all uninterpreted term in the trail
    for ( size_t i = 0; i < trail.size(); ++i ) {
        Variable v = trail.at(i);
        Term t = variable_table.get_term(v);
        if ( term_table.kind_of(t) == TERM_UNINTERPRETED ) {
            add_assignment(t, trail.get_value(v));
        }
    }
}

void Model::add_assignment( Term t, Value val) {
    // assignment are uninterpreted term with a value
    assert( term_table.kind_of(t) == TERM_UNINTERPRETED );

    all_term.push_back(t);

    compact_string *name = term_table.get_name(t);
    if ( name )
        name_to_var[std::string(name->begin(), name->end())] = t;

    term_to_val[t] = val;
}
