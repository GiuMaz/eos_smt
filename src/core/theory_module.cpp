/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "theory_module.h"
#include "SMTSolver.h"

TheoryModule::TheoryModule( SMTSolver & solver ) :
    main_solver(solver),
    sort_table(solver.get_sort_table()),
    term_table(solver.get_term_table()),
    variable_table(solver.get_variable_table()),
    trail(solver.get_trail()),
    id( solver.add_theory_module(this) )
{ }

