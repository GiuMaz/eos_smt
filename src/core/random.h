/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_CORE_RANDOM_HH
#define EOS_CORE_RANDOM_HH

/*
 * TODO rewrite properly
    // generate a random number in 0..2^31, it modify the seed
    unsigned int random();
    uint32_t seed_1, seed_2, seed_3, seed_4;
unsigned int BooleanModule::random() { 
    // the KISS random number generator

    seed_1 = 69069*seed_1+12345; 

    seed_2 ^= (seed_2<<13);
    seed_2 ^= (seed_2>>17);
    seed_2 ^= (seed_2<<5);

    uint64_t t = 698769069ULL*seed_3+seed_4;

    seed_4 = (t>>32);

    return seed_1+seed_2+(seed_3=seed_4); 
}
*/

#endif // EOS_CORE_RANDOM_HH
