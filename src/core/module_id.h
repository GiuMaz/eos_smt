/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_CORE_MODULE_ID_HH
#define EOS_CORE_MODULE_ID_HH

#include <vector>
#include <string>

class ModuleId {
    // no_module is the defualt value
    const static uint32_t NO_MODULE = ~0;

    // pretty printing
    static const std::string NO_MODULE_NAME;
    static std::vector<std::string> name_table;

    // the identification inside the solver
    uint32_t id;

public:

    ModuleId(): id(NO_MODULE) {}
    explicit ModuleId(uint32_t i): id(i) {}
    ModuleId(const ModuleId &) = default;

    ModuleId &operator=(const ModuleId &rhs) = default;

    bool operator==(const ModuleId &rhs) const { return id == rhs.id; }
    bool operator!=(const ModuleId &rhs) const { return id != rhs.id; }

    // set a global name associated to the module (for debugging)
    static void set_name(const ModuleId &id, const std::string &name);

    // getter
    uint32_t index() const;
    const std::string &name() const;
};

const static ModuleId NULL_MODULE;

// specialization of hash
namespace std {
    template <> struct hash<ModuleId>
    {
        size_t operator()(const ModuleId & x) const
        {
            return x.index();
        }
    };
}

#endif // EOS_CORE_MODULE_ID_HH
