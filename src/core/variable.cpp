/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "variable.h"
#include "SMTSolver.h"
#include "printing.h"
#include <sstream>

VariableTable::VariableTable(SortTable &st, TermTable &tt, SMTSolver &s ):
    sort_table(st),
    term_table(tt),
    main_solver(s),
    var_to_term({NULL_TERM}),
    term_to_var({{NULL_TERM, NULL_VAR}}) {}

bool VariableTable::has_variable( Term t ) {
    assert ( t.is_positive() );
    return term_to_var.find(t) != term_to_var.end();
}

/*
 * get the associated variable and create a new variable if no previous
 * associated variable exist.
 */
Variable VariableTable::get_variable( Term t ) {
    assert ( t.is_positive() );

    if (term_to_var.find(t) != term_to_var.end() )
        return term_to_var[t];

    Variable var(var_to_term.size());
    var_to_term.push_back(t);
    term_to_var[t] = var;

    if ( MESSAGE_LEVEL(VERBOUSE_MESSAGE) ) {
        debug << "VARIABLE: create variable " << var << " for term "
            << term_table.pprint(t) << std::endl;
    }

    notify_solver(var);
    return var;
}

/*
 * get associated variable, return NULL_VAR if no associated variable exist
 */
Variable VariableTable::get_variable_if_exist( Term t ) {
    assert ( t.is_positive() );

    auto it = term_to_var.find(t);
    if ( it != term_to_var.end() )
        return it->second;
    else
        return NULL_VAR;
}

Term VariableTable::get_term( Variable var ) const {
    assert( var.index() < var_to_term.size() );
    return var_to_term[var.index()];
}

SortKind VariableTable::sort_kind(Variable var) {
    return sort_table.kind_of(term_table.sort_of(get_term(var)));
}

bool VariableTable::is_boolean(Variable var) {
    return sort_table.is_boolean(term_table.sort_of(get_term(var)));
}

bool VariableTable::is_real(Variable var) {
    return sort_table.is_real(term_table.sort_of(get_term(var)));
}

bool VariableTable::is_valid(Variable var) {
    if( var == NULL_VAR)
        return false;
    if ( var.index() >= var_to_term.size() )
        return false;
    if ( var_to_term[var.index()] == NULL_TERM )
        return false;
    return true;
}

void VariableTable::notify_solver(Variable var) {
    main_solver.register_new_var(var);
}

Term VariableTable::substitue_subvar(Term origin,
        Variable var, Term sub) {
    assert( term_table.kind_of(origin) == TERM_EQ );
    Term var_term = get_term(var);
    const auto &args = term_table.get_composite_args(origin);

    Term left  = args[0];
    Term right = args[1];

    if ( left  == var_term )
        left  = sub;
    if ( right == var_term )
        right = sub;

    Term result = term_table.add_binary_equality(left, right);
    return result;
}

std::ostream & operator<< (std::ostream & os, Variable v) {
    if ( v == NULL_VAR )
        os << "V_NULL";
    else
        os << "V_" << v.index();
    return os;
}

std::ostream & operator<< (std::ostream & os, VariableTable table) {
    os << "=== VARIABLE TABLE ===" << std::endl;

    for ( uint32_t i = 0; i < table.var_to_term.size(); ++i ) {
        Variable v(i);
        Term t = table.var_to_term[v.index()];
        os << "var "  << v << " -> term " << t << " | " ;
        os << "term " << t << " -> var "  << table.term_to_var[t] << std::endl;
    }

    os << "======================" << std::endl;
    return os;
}

std::string VariableTable::pprint(Variable v) const {
    return term_table.pprint( get_term(v)  );
}
