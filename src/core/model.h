/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_CORE_MODEL_HH
#define EOS_CORE_MODEL_HH

#include "term.h"
#include "sort.h"
#include "variable.h"
#include "trail.h"

#include <iostream>
#include <unordered_map>
#include <string>

/*
 * resulting model of a SAT assignment
 */
class Model {

public:
    Model(Trail &t, TermTable &tt, SortTable &st, VariableTable &vt) :
        trail(t), term_table(tt), sort_table(st), variable_table(vt)
    {}

    void add_from_trail(const Trail &);

protected:

    void add_assignment( Term t, Value val);

    std::unordered_map<Term,Value> term_to_val;
    std::unordered_map<std::string, Term> name_to_var;
    std::vector<Term> all_term;

    Trail &trail;
    TermTable &term_table;
    SortTable &sort_table;
    VariableTable &variable_table;

    friend std::ostream &operator<<(std::ostream &os, const Model &m);
};

#endif // EOS_CORE_MODEL_HH
