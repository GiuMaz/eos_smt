/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_CORE_VARIABLE_HH
#define EOS_CORE_VARIABLE_HH

#include "term.h"
#include "sort.h"
#include <functional>
#include <unordered_map>
#include <vector>

class SMTSolver;

class Variable {
    uint32_t value = 0 ;
public:
    Variable() = default;
    Variable(const Variable &) = default;
    Variable(Variable &&) = default;
    explicit Variable(uint32_t i) noexcept: value(i) {}

    ~Variable() = default;


    Variable &operator=(const Variable &) = default;
    Variable &operator=(Variable &&) = default;

    uint32_t index() const { return value; }
    size_t   hash()  const { return static_cast<size_t>(value); };

    bool operator==(const Variable &rhs) const { return value == rhs.value; }
    bool operator!=(const Variable &rhs) const { return value != rhs.value; }
};

std::ostream & operator<< (std::ostream & os, Variable v);

const static Variable NULL_VAR;

// only positive term has a variable associated

class VariableTable {
public:

    VariableTable(SortTable &st, TermTable &tt, SMTSolver &s );
    ~VariableTable() = default;

    bool has_variable( Term t );

    /*
     * get the associated variable and create a new variable if no previous
     * associated variable exist.
     */
    Variable get_variable( Term t );

    /*
     * get associated variable, return NULL_VAR if no associated variable exist
     */
    Variable get_variable_if_exist( Term t );

    Term get_term( Variable var ) const;

    SortKind sort_kind(Variable var);

    bool is_boolean(Variable var);
    bool is_real(Variable var);
    bool is_valid(Variable var);

    Term substitue_subvar(Term origin, Variable var, Term sub);

    std::string pprint(Variable v) const;

private:
    /*
     * notify the main solver when a new variable is added.
     */
    void notify_solver(Variable var);

    SortTable &sort_table;
    TermTable &term_table;
    SMTSolver &main_solver;

    std::vector<Term> var_to_term;
    std::unordered_map<Term,Variable> term_to_var;

    friend std::ostream & operator<< (std::ostream & os, VariableTable t);
};

// hash function
namespace std {
    template <> struct hash<Variable> {
        size_t operator()(const Variable &x) const { return x.hash(); }
    };
}

#endif // EOS_CORE_VARIABLE_HH
