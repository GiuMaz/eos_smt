/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "trail.h"
#include "printing.h"

void Trail::notify_new_variable( Variable var ) {
    assignment.notify_new_variable(var);

    assert( (kinds.size() == position_in_trail.size()) &&
            (kinds.size() == levels.size() ) &&
            (kinds.size() == module_ids.size()) );

    if (kinds.size() <= var.index()) {
        if ( MESSAGE_LEVEL(VERBOUSE_MESSAGE) ) {
            debug << "TRAIL: expand to variable " << var << std::endl;
        }

        kinds.resize(var.index()+1, TRAIL_UNASSIGNED);
        levels.resize(var.index()+1, NO_LEVEL);
        position_in_trail.resize(var.index()+1, NO_POSITION);
        module_ids.resize(var.index()+1, ModuleId());
    }
}

const Assignment &Trail::get_assignment() const {
    return assignment;
}

size_t Trail::size() const { return elements.size(); }

bool Trail::is_consistent() const { return !inconsistent; }
void Trail::set_inconsistent(bool i) { inconsistent = i; }

Variable Trail::at(size_t pos) const {
    assert( pos < elements.size() );
    return elements[pos];
}

Variable Trail::back() const {
    assert( elements.size() > 0 );
    return elements.back();
}

size_t Trail::get_current_level() const { return current_level; }

bool Trail::has_value(Variable var) const {
    assert( var.index() < levels.size() );
    return levels[var.index()] != NO_LEVEL; 
}

bool Trail::has_cached_value(Variable var) const {
    assert( var.index() < assignment.size() );
    return assignment.get_value(var) != NULL_VALUE;
}

bool Trail::is_at_level_zero() const { return current_level == 0; }

/*
 * get the value assigned in the trail to a variable
 */
Value Trail::get_value(Variable var) const {
    assert(has_value(var));
    return assignment.get_value(var);
}

/*
 * get the value assigned in the trail to a variable
 */
Value Trail::get_cached_value(Variable var) const {
    return assignment.get_value(var);
}

bool Trail::get_boolean_value(Variable var) const {
    Value val =  get_value(var);
    return val.get_boolean_value();
}

uint32_t Trail::get_timestamp(Variable var) const {
    assert(has_value(var));
    return assignment.get_timestamp(var);
}

size_t Trail::get_level( Variable var ) const {
    assert( var.index() < levels.size() );
    assert( levels[var.index()] != NO_LEVEL );
    return levels[var.index()];
}

size_t Trail::get_position_in_trail( Variable var ) const {
    assert( var.index() < position_in_trail.size() );
    assert( position_in_trail[var.index()] != NO_POSITION );
    return position_in_trail[var.index()];
}

trail_kind_t Trail::get_kind( Variable var ) const {
    assert( var.index() < kinds.size() );
    return kinds[var.index()];
}

const ModuleId &Trail::get_module_id( Variable var ) const {
    assert( var.index() < module_ids.size() );
    return module_ids[var.index()];
}

void Trail::add_decision(Variable var, const Value &val, const ModuleId &module_id) {
    assert( ! has_value(var) );

    // open new decision level
    open_decision_level();

    // build variable
    add_value(var, val, module_id, TRAIL_DECIDED, current_level);

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "TRAIL: assign DECISION" <<
            " var: " << variable_table.pprint(var) <<
            " val: " << val.pprint(sort_table) <<
            " by module: " << module_id.name() <<
            " at level: " << get_current_level() << std::endl;
    }
}

void Trail::add_propagation(Variable var, bool value, const ModuleId &module_id, size_t level) {
    assert( ! has_value(var) );

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "TRAIL: assign PROPAGATION" <<
            " var: " << variable_table.pprint(var) <<
            " val: " << (value ? "true" : "false") <<
            " by module: " << module_id.name() <<
            " at level: " << level << std::endl;
    }

    add_value(var, Value(value), module_id, TRAIL_PROPAGATED, level);
}

// pop_propagation
void Trail::pop_propagation() {
    // get variable
    Variable var = elements.back();
    assert( kinds[var.index()] == TRAIL_PROPAGATED );

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "TRAIL: undo propagation " << var << " -> " <<
            get_value(var) <<  " at level " << get_level(var) << std::endl;
    }

    // check the level of the last variable
    size_t lv = levels[var.index()];
    if ( lv == current_level ) {
        // simply delete all info
        undo_value( var );
    }
    else {
        // it should be at a lower level, add to the repropagate list
        assert( lv < current_level );
        buffer_to_repropagate.push_back(var);
    }

    // pop the element
    elements.pop_back();
}

// pop_decision
void Trail::pop_decision() {
    // get variable
    Variable var = elements.back();
    assert( kinds[var.index()] == TRAIL_DECIDED );

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "TRAIL: undo decision " << var << " -> " <<
            get_value(var) <<  " at level " << get_level(var) << std::endl;
    }

    // delete all info and remove from trail
    undo_value( var );
    elements.pop_back();

    // delete decision level
    undo_decision_level();

    // when the last decision is removed, the trail is not more inconsitent
    inconsistent = false;

    while ( ! buffer_to_repropagate.empty() ) {
        // get the variable to repropagate
        var = buffer_to_repropagate.back();
        buffer_to_repropagate.pop_back();

        // restore variable position
        position_in_trail[var.index()] = elements.size();
        elements.push_back(var);
    }
}

void Trail::pop_last_level(std::vector<Variable> &undo) {
    assert( level_size.size() > 0 );

    size_t target_pos = level_size.back();

    while( elements.size() > target_pos &&
            kinds[elements.back().index()] != TRAIL_DECIDED) {
        undo.push_back(back());
        pop_propagation();
    }
 
    if (elements.size() > target_pos) {
        undo.push_back(back());
        pop_decision();
    }
    else {
        // this was a fake push in the decision level, simply undo
        undo_decision_level();
        inconsistent = false;
    }
}
void Trail::add_value(Variable var, const Value &val, const ModuleId &module_id,
        trail_kind_t kind, size_t level) {

    uint32_t pos = var.index();

    // check that this is not a reassignment of an already existing value
    assert( position_in_trail[pos] == NO_POSITION );
    assert(             kinds[pos] == TRAIL_UNASSIGNED );
    assert(            levels[pos] == NO_LEVEL );
    assert(        module_ids[pos] == NULL_MODULE );
    assert( (kind == TRAIL_DECIDED    && level == current_level) ||
            (kind == TRAIL_PROPAGATED && level <= current_level) );

    // set info about the variable
    kinds[pos] = kind;
    levels[pos] = level;
    module_ids[pos] = module_id;

    // push in the element
    position_in_trail[pos] = elements.size();
    elements.push_back(var);

    // set the value in the assignment
    assignment.set_value( var, val );
}

void Trail::open_decision_level() {
    assert ( is_consistent() );
    ++current_level;
    level_size.push_back( elements.size() );
}

void Trail::undo_decision_level() {
    --current_level;
    level_size.pop_back();
}

void Trail::undo_value(Variable var) {
    uint32_t pos = var.index();
    kinds[pos] = TRAIL_UNASSIGNED;
    levels[pos] = NO_LEVEL;
    module_ids[pos] = NULL_MODULE;
    position_in_trail[pos] = NO_POSITION;
}

bool Trail::completly_assigned() const {
    return assignment.all_assigned();
}

std::ostream &operator<< (std::ostream & os, const Trail &t ) {
    os << "=== TRAIL ===" << std::endl;

    os << "assigned: " << std::endl;
    for ( const auto &e : t.elements ) {
        os << t.variable_table.pprint(e) <<
            " <- " << t.assignment.get_value(e).pprint(t.sort_table) <<
            " [ level: " << t.get_level(e) <<
            " kind: ";

        switch ( t.get_kind(e) ) {
            case TRAIL_DECIDED:
                os << "DECIDED";
                break;
            case TRAIL_PROPAGATED:
                os << "PROPAGATED";
                break;
            case TRAIL_UNASSIGNED:
                os << "LIT_UNASSIGNED";
                break;
            default:
                os << "NO KIND";
                break;
        }

        os << " by module: " << (t.get_module_id(e).name()) << " ]" << std::endl;
    }

    os << "=============" << std::endl;
    return os;
}
