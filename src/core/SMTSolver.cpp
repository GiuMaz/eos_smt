/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <array>
#include <assert.h>
#include <cassert>
#include <functional>
#include <iomanip>
#include <map>
#include <set>
#include <tuple>
#include <unordered_set>
#include <vector>

#include "module_id.h"
#include "printing.h"
#include "SMTSolver.h"
#include "term.h"
#include "theory_module.h"



//  SOLVER
// --------

SMTSolver::SMTSolver(TermTable &tt):
    sort_table(tt.get_sort_table()),
    term_table(tt),
    variable_table(tt.get_sort_table(), tt, *this),
    theory_modules(),
    trail(variable_table,tt.get_sort_table()),
    order(variable_table),
    model(trail, term_table, sort_table, variable_table)
{
    // define true
    Variable true_var = variable_table.get_variable(true_term);
    trail.add_propagation(true_var, true, NULL_MODULE, 0);
}

bool SMTSolver::check_sat() {

    if ( MESSAGE_LEVEL(STANDARD_MESSAGE) ) {
        debug << "BEGIN CHECK_SAT" << std::endl;
        debug << "trail at beginning:" << std::endl << trail;
    }
    if ( MESSAGE_LEVEL(VERBOUSE_MESSAGE) ) {
        debug << get_sort_table() << std::endl;
        debug << get_term_table() << std::endl;
    }

    // check if unsat from the beginning and exit
    if ( ! trail.is_consistent() ) {
        state = STATE_UNSAT;
        if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
            debug << "trivially UNSAT\n";
        }
        return false; // UNSAT
    }

    // initialize searching parameter
    uint32_t conflict_counter = 0;
    uint32_t restart_counter  = 0;
    uint32_t restart_threshold = new_restart_threshold();
    
    while (true) {
        // do propagation in all theory
        propagate();

        if ( ! trail.is_consistent() ) {

            if ( MESSAGE_LEVEL(STANDARD_MESSAGE) ) {
                debug << "SOLVER: conflict at level " <<
                    trail.get_current_level() << std::endl;
            }

            ++conflict_counter;

            // if current level is zero return unsat
            if ( trail.is_at_level_zero() ) {
                if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                    debug << "SOLVER: found a conflict at base level, UNSAT\n";
                }
                state = STATE_UNSAT;
                return false; // UNSAT
            }

            // analyze conflict (build a lemma from the theory) and backtrack
            analyze_conflict();

            // is impossible to backtrack?
            if ( state == STATE_UNSAT ) {
                if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                    debug << "SOLVER: impossible to recover from conflict, UNSAT\n";
                    debug << term_table << sort_table << variable_table << trail;
                }
                return false; // UNSAT
            }

            // decay variable activity
            order.variable_activity_decay();
        }

        else {
            // TODO: sometimes call the garbage collector

            // if restart limit reached -> restart
            if ( conflict_counter >= restart_threshold ) {
                ++restart_counter;

                if ( MESSAGE_LEVEL (STANDARD_MESSAGE) ) {
                    debug << "restart number " << restart_counter << std::endl;
                }

                // increase the threshold;
                restart_threshold += new_restart_threshold();

                backtrack_to(0);
            }

            // trye to choice a value from a theory
            bool made_new_decision = decide_value();

            if ( ! made_new_decision ) {
                if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                    debug << "SOLVER: everything assigned, SAT\n";
                    debug << term_table << sort_table <<
                        variable_table << trail << std::endl;
                }
                // everything is completely assigned!
                assert( trail.is_consistent() );
                state = STATE_SAT;
                return true; // SAT
            }
        }
    }
    return false;
}

/*
 * return the current model
 */
const Model &SMTSolver::get_model() {
    model.add_from_trail(trail);
    return model;
}

void SMTSolver::set_logic( smt_logic_t l ) {
    logic = l;
}

/*
 * analyze a conflict and resolve it, if possible
 */ 
void SMTSolver::analyze_conflict() {
    assert(module_in_conflict != NULL_MODULE);

    // get the reason of the conflict from the proper module
    std::vector<Variable> reason;
    theory_modules[module_in_conflict.index()]->get_conflict(reason);

    // the current infeasible assignment
    std::unordered_set<Variable> conflict;
    // variable at the current conflict level
    std::unordered_set<Variable> conflict_level_vars;
    // all variables of conflict and resolution, used to increase activity
    std::unordered_set<Variable> all_seen_vars;

    size_t conflict_level = 0;

    // find the level of the reasons
    for ( const auto &var : reason ) {
        assert(trail.has_value(var));
        size_t level = trail.get_level(var);

        if ( level > conflict_level ) {
            conflict_level = level;
            conflict_level_vars.clear();
        }

        if ( level == conflict_level ) {
            conflict_level_vars.insert(var);
        }

        conflict.insert(var);
        all_seen_vars.insert(var);
    }

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "SOLVER: reason of the conflict: [ ";

        for (const auto &r : reason )
            debug << variable_table.pprint(r) << " <- " <<
                trail.get_value(r).pprint(sort_table) << ", ";

        debug << "] at level " << conflict_level <<
            " from " << module_in_conflict.name() << std::endl;
    }

    // undo everything after the conflict
    backtrack_to(conflict_level);

    // conflict at level zero
    if ( conflict_level == 0 ) {
        trail.set_inconsistent(true);
        state = STATE_UNSAT;
        return;
    }

    bool require_undo_decide = false;
    Variable undo_decide_var = NULL_VAR;

    size_t trail_it = trail.size()-1;

    // search untill UIP
    while ( conflict_level_vars.size() > 1 ) {

        // get the last variable in the trail
        Variable last = trail.at(trail_it);
        assert( trail.get_kind(last) != TRAIL_DECIDED );

        if (MESSAGE_LEVEL(STANDARD_MESSAGE))
            debug << "SOLVER: resolve with " << variable_table.pprint(last);

        // variable is at conflict level?
        auto it = conflict_level_vars.find(last);
        if ( it == conflict_level_vars.end() ) {
            if (MESSAGE_LEVEL(STANDARD_MESSAGE))
                debug << " -> not at conflict level, skip" << std::endl;
            // not at conflict level, simply remove from the trail
            --trail_it;
            continue;
        }

        // remove from the conflict
        conflict_level_vars.erase(it);
        assert(conflict.find(last) != conflict.end());
        conflict.erase(conflict.find(last));

        // get the other module
        ModuleId id = trail.get_module_id(last);

        // get the explanation
        std::vector<Variable> just;
        theory_modules[id.index()]->get_justification(last,just);

        for (const auto &j : just) {
            // add to all seen for activity ordering
            all_seen_vars.insert(j);

            Term t = variable_table.get_term(j);

            // it is first order assignment at the current level?
            if ( term_table.sort_of(t) != bool_sort
                    && trail.get_level(j) == conflict_level ) {
                // impossible to resolve, and undo decide is required
                require_undo_decide = true;
                undo_decide_var = last;
            }
            else {
                conflict.insert(j);
                if ( trail.get_level(j) == conflict_level )
                    conflict_level_vars.insert(j);
            }
        }

        if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
            debug << " -> reason [ ";
            for (const auto &j : just)
                debug << variable_table.pprint(j) << " ";
            debug << "]" << std::endl;
            debug << "SOLVER: new conflict [ ";
            for (const auto &c : conflict)
                debug << variable_table.pprint(c) << " ";
            debug << "]" << std::endl;
        }

        // remove last from trail
        --trail_it;

        // impossible to continue on the propagation
        if ( require_undo_decide ) break;
    }

    // increased activity
    for( const auto &v : all_seen_vars ) {
        if (MESSAGE_LEVEL(STANDARD_MESSAGE))
            debug << "SOLVER: increase activity of " << variable_table.pprint(v) << std::endl;
        order.increase_activity(v);
    }

    // UndoDecide
    if ( require_undo_decide ) {

        assert( undo_decide_var != NULL_VAR );
        assert( term_table.sort_of(
                    variable_table.get_term(undo_decide_var)) == bool_sort );
        assert( trail.has_value(undo_decide_var) );

        if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
            debug << "SOLVER: UndoDecide on variable " <<
                variable_table.pprint(undo_decide_var) << std::endl;
        }

        // the new value is the opposite of the current assignment
        Value new_value( ! trail.get_boolean_value(undo_decide_var));

        // backtrack of one level
        backtrack_to(trail.get_current_level()-1);

        // register new variables
        process_registration_queue();

        // decide the opposite value
        push_level();

        trail.add_decision(undo_decide_var, new_value, NULL_MODULE);
        return;
    }

    // conflict should be a UIP
    assert( conflict_level_vars.size() == 1 );

    // get the uip term
    Term uip = variable_table.get_term(*conflict_level_vars.begin());

    // UndoClear
    if ( term_table.sort_of(uip) != bool_sort ) {

        if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
            debug << "SOLVER: UndoClear"<< std::endl;
        }

        // simply undo the last level
        backtrack_to(trail.get_current_level()-1);

        // register new variables
        process_registration_queue();

        return;
    }

    // Backjump and learn new lemma

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "SOLVER: Backjump"<< std::endl;
    }

    std::vector<Term> lemma;
    size_t  backtrack_level = 0;

    for ( const auto &v: conflict ) {
        assert(trail.has_value(v));
        assert(variable_table.is_boolean(v));

        Term t = variable_table.get_term(v);

        bool is_true = trail.get_boolean_value(v);
        if (is_true) t = t.opposite();

        lemma.push_back(t);
        size_t lv = trail.get_level(v);
        if ( lv != conflict_level && lv > backtrack_level )
            backtrack_level = lv;
    }

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "SOLVER: new lemma [ ";
        for ( const auto &t : lemma )
            debug << term_table.pprint(t) << " ";
        debug << "]" << std::endl;
    }

    assert(backtrack_level < conflict_level);

    backtrack_to(backtrack_level);

    // register new variables
    process_registration_queue();

    // reset the conflict module, add_lemma can find a new conflict
    module_in_conflict = NULL_MODULE;

    // notify module of new lemma
    for (auto &m : theory_modules)
        m->new_lemma(lemma);

    propagate();
}

/*
 * notify all the module that a new level is created
 */
void SMTSolver::push_level() {
    for ( auto &m : theory_modules)
        m->push();
}

/*
 * notify all the module that the last level is undone
 */
void SMTSolver::pop_level() {
    for ( auto &m : theory_modules)
        m->pop();
}

/*
 * backtrack to the speficied level of the trail.
 * all the assigment done after this level must be undone.
 */
void SMTSolver::backtrack_to(size_t level) {

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "SOLVER: undo from level " << trail.get_current_level() <<
            " to level " << level << std::endl;
    }

    std::vector<Variable> undo_var;

    while ( trail.get_current_level() > level ) {

        // pop level in theory modules
        pop_level();

        // undo the last level and save the removed variables
        undo_var.clear();
        trail.pop_last_level(undo_var);

        // reinsert removed variable in variable order
        for ( Variable v: undo_var )
            order.insert(v);

    }

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "SOLVER: trail after backtrack " << std::endl << trail;
        //debug << "SOLVER: order after backtrack" << std::endl << order;
    }
}

/*
 * get the new restart threshold.
 * The solver jump back to level zero when the number of conflict is greater
 * than this threshold. After a restart this threshold is changed.
 */
uint32_t SMTSolver::new_restart_threshold() {
    return restart_interval_multiplier*next_restart_interval(luby_next++);
}

/*
 * build the new restart interval using the Luby sequence
 */
uint32_t SMTSolver::next_restart_interval( uint32_t pos) {
    // use the luby sequence for evaluating the new interval
    // the code is based on minisat
    unsigned int size, seq;

    // find in what sequence we are, the sequence i have lenght (2^i)-1
    for ( size = 1, seq = 0; size < pos+1; ++seq, size = 2*size + 1 )
        ;

    // find the value inside the sequence
    while ( (size-1) != pos ) {
        size /= 2;
        --seq;
        pos %= size; // NOLINT(clang-analyzer-core.DivideZero)
    }

    return 1 << seq; // 2^seq
}

/*
 * make a decision on a value assignment.
 * All the trail must be propagated before this decision. We have a variable
 * ordering inside 'order' that use the VSIDS heuristic to keep the variables
 * ordered on their activity.
 * If a new variable is assigned this function return true, otherwise (if there
 * are no more variable to assign) the function return false.
 */
bool SMTSolver::decide_value() {
    assert(trail.is_consistent());

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "SOLVER: make a decision" << std::endl;
        //debug << "SOLVER: order before decision" << std::endl << order;
    }

    // find a valid assigment
    while (true) {

        // Get an unassigned variable from the queue

        // TODO: maybe add random decision

        // get the unasigned variable with the highest activity
        Variable var = NULL_VAR;
        while ( var == NULL_VAR && !order.empty() ) {
            var = order.get_var_for_decision();
            if ( trail.has_value(var) )
                var = NULL_VAR;
        }

        // no more variable to assign?
        if ( var == NULL_VAR )
           return false; // no assignement

        // get a decision from a theory module
        SortKind s_kind = variable_table.sort_kind(var); 
        assert(variable_decision_by_sort[s_kind] != NULL_MODULE );
        ModuleId id = variable_decision_by_sort[s_kind];

        if ( MESSAGE_LEVEL( VERBOUSE_MESSAGE) ) {
            debug << "SOLVER: decide variable " << variable_table.pprint(var) <<
                " in module: " << id.name() << std::endl;
        }

        // open a new level
        push_level();

        // make a decision and exit
        theory_modules[id.index()]->make_decision(var);
        return true; // assigned
    }
}

/*
 * given a list of unasigned terms make a decision on one of them.
 * The variable can be choosen in any way possible.
 */
void SMTSolver::decide_one_of(const std::vector<Term> & unassigned) {
    if (MESSAGE_LEVEL(VERBOUSE_MESSAGE)) {
        debug << " decide one of " << unassigned << std::endl;
    }

    for ( Term u : unassigned ) {
        Term pos_u = u.unsign();
        Variable var = variable_table.get_variable(pos_u);
        if ( ! trail.has_value(var) ) {
            push_level();
            trail.add_decision(var, u == pos_u ? true_value : false_value,
                    NULL_MODULE);
            return;
        }
    }
}

/*
 * Asserts some formulas.
 * Before the acutal assertion the term must be preprocessed to simplify the
 * interaction with the modules.
 */
void SMTSolver::asserts(const std::vector<Term> &assertions) {

    // make a copy of the assertions
    std::vector<Term> formulas(assertions);

    // preprocess the term and place the preprocessed in the assertions copy
    // it is possible that new term are generated!
    new_term_from_preproc.clear();
    for (size_t i = 0; i < formulas.size(); ++i) {
        Term f = preprocess_term(formulas[i]);

        if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
            if ( f != formulas[i] ) {
                debug << "SOLVER: term " << term_table.pprint(formulas[i]) <<
                    " preprocessed to " << term_table.pprint(f) << std::endl;
            }
        }

        formulas[i] = f;
        formulas.insert(formulas.end(),
                new_term_from_preproc.begin(), new_term_from_preproc.end());
        new_term_from_preproc.clear();
    }

    // remove nonboolean ite term
    if ( cofactoring_ite) {
        for (size_t i = 0; i < formulas.size(); ++i) {
            // process
            Term f = preprocess_ite_cofact(formulas[i]);

            if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                if ( formulas[i] != f )
                debug << "SOLVER: processed " << term_table.pprint(formulas[i])
                    << " to " << term_table.pprint(f) << std::endl;
            }

            formulas[i] = f;
        }
    }
    else {
        new_term_from_preproc.clear();
        for (size_t i = 0; i < formulas.size(); ++i) {
            // process
            Term f = preprocess_ite(formulas[i]);

            if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                if ( f != formulas[i] ) {
                    debug << "SOLVER: term " << term_table.pprint(formulas[i]) <<
                        " preprocessed to " << term_table.pprint(f) << std::endl;
                }
            }

            formulas[i] = f;
            formulas.insert(formulas.end(),
                    new_term_from_preproc.begin(), new_term_from_preproc.end());
            new_term_from_preproc.clear();
        }
    }

    if (MESSAGE_LEVEL(VERBOUSE_MESSAGE))
        debug << "Term after preprocessing" << std::endl << term_table;

    if (MESSAGE_LEVEL(VERBOUSE_MESSAGE))
        debug << "ASSERT FORMULAS" << std::endl;

    // assert all the terms
    for (size_t i = 0; i < formulas.size(); ++i) {
        assert_formula(formulas[i]);
        formulas.insert(formulas.end(),
                new_term_from_assert.begin(), new_term_from_assert.end());
        new_term_from_assert.clear();
    }
}

/*
 * Assert a formula term.
 * If there are no variable related to the term a new variable is generated
 * and all the theory module that own that kind of term or the sort of the
 * term are notified o this new variable. This new term is set to true on the
 * trial at level zero. If the variables is already assigned (for example for a
 * propagation) it should be set to true, otherwise the problem is unsolvable.
 * After the assignment propagate the effect of this assertion.
 */
void SMTSolver::assert_formula(Term f) {
    // if we are UNSAT, stop
    if ( ! trail.is_consistent() ) {
        return;
    }

    if (MESSAGE_LEVEL(STANDARD_MESSAGE))
        debug << "SOLVER: assert formula " << term_table.pprint(f) << std::endl;

    assert(trail.get_current_level() == 0);

    // build the variable and process, this can create new variables
    Term     f_pos = f.unsign();
    Variable f_var = variable_table.get_variable(f_pos);
    // process the newly created variable in all modules
    process_registration_queue();

    // add variable to asserted
    asserted_vars.push_back(f_var);

    // assign the asserted term
    if ( !trail.has_value(f_var) ) {
        // if the trail don't have the variable place it on the trail
        // as a base level propagation
        trail.add_propagation(f_var, f.is_positive(), NULL_MODULE,
                trail.get_current_level());
    }
    else if ( trail.get_boolean_value(f_var) != f.polarity() ) {
        // if is already inside the trail, check that the polarity is correct
        trail.set_inconsistent( true );
        return;
    }

    // propagate the assertion
    propagate();
}

/*
 * propagate the effect of all the assigment (but decision and propagation).
 * All the module should process the newly assigned variables.
 */
void SMTSolver::propagate() {

    if (MESSAGE_LEVEL(STANDARD_MESSAGE))
        debug << "SOLVER: propagate" << std::endl;
    if (MESSAGE_LEVEL(VERBOUSE_MESSAGE))
        debug << "SOLVER: trail before propagation" << std::endl << trail;

    bool someone_propagate;
    do {
        someone_propagate = false;

        // propagate in every modules
        for ( auto &m : theory_modules ) {

            if ( ! trail.is_consistent() )
                break;

            // used in looking for propagation
            size_t size_before_propagation = trail.size();

            m->propagate();

            // if the trail is longer someone made a propagation
            if ( size_before_propagation != trail.size())
                someone_propagate = true;
        }
    } while(someone_propagate && trail.is_consistent());

    if ( MESSAGE_LEVEL( STANDARD_MESSAGE))
        debug << "SOLVER: trail after propagation" << std::endl << trail;
}

/*
 * registration_queue store all the new variables created by the solver or
 * by one of the theory modules. All the interested modules must be notified of 
 * this new declaration
 */
void SMTSolver::process_registration_queue() {
    while( ! registration_queue.empty() ) {
        // pop from the queue
        Term t = registration_queue.front(); registration_queue.pop();
        
        if (MESSAGE_LEVEL(VERBOUSE_MESSAGE))
            debug << "SOLVER: notify of new term " << t << std::endl;

        // add owners of the term by kind and sort
        std::unordered_set<ModuleId> owners;

        auto & by_kind = term_owner_by_kind[term_table.kind_of(t)];
        owners.insert(by_kind.begin(), by_kind.end());

        auto & by_sort = term_owner_by_sort[
            sort_table.kind_of(term_table.sort_of(t))];
        owners.insert(by_sort.begin(), by_sort.end());

        // notified the interested modules
        for ( auto &m : owners)
            theory_modules[m.index()]->new_term_notify(t);
    }
}

/*
 * if the variable is new:
 * - add the term of the variable to registration_queue.
 * - extend the trial
 * - add the variable to the variable order
 */
void SMTSolver::register_new_var(Variable var) {
    // get the proper term
    Term t = variable_table.get_term(var);

    // already in the queue?
    if ( notify_cache.find(t) != notify_cache.end() )
        return;
    // add to the cache and queue
    notify_cache.insert(t);
    registration_queue.push(t);

    // notify the other component
    trail.notify_new_variable(var);

    // notify variable ordering
    order.insert(var);
}



//  MODULE-SOLVER INTERFACE
// -------------------------

/*
 * register a new module inside the solver. This process return a unique
 * identifier that the module must store to identify itself during all the
 * comunication with the solver.
 */
uint32_t SMTSolver::add_theory_module(TheoryModule *s) {
    theory_modules.push_back(s);
    return theory_modules.size()-1;
}

/*
 * used by a theory module to notify to the solver that a conflict happens.
 * The solver must remember the modules to perform a conflict analysis.
 */
void SMTSolver::report_conflict(ModuleId i) {
    module_in_conflict = i;
}

/*
 * used by theory module to notify the main solver of new lemma discovered
 * during the notification of a variable
 */
void SMTSolver::report_lemma(Term lemma) {
    new_term_from_assert.push_back(lemma);
}

/*
 * Notify to the solver that the module own a specific kind of term.
 * This module should be notified when a new variable associated to this
 * kind of term is declared by the solver or by another module.
 */
void SMTSolver::notify_term_ownership(ModuleId id, TermKind k) {
    term_owner_by_kind[k].push_back(id);
}

/*
 * Notify to the solver that the module own a specific sort.
 * This module should be notified when a new variable of this kind is
 * declared by the solver or by another module.
 */
void SMTSolver::notify_sort_ownership(ModuleId id, SortKind k) {
    term_owner_by_sort[k].push_back(id);
}

/*
 * increase the activity of a variable. It is called when a variable is
 * added to a clause or similar. The more a variable is used, the higher it's
 * activity should be.
 */
void SMTSolver::increase_activity(Variable var) {
    order.increase_activity(var);
}

/*
 * Notify to the solver that the module id make decision about a variable
 * of sort k. This module should be called when the decision procedure
 * want to make a new decision on a variable of this specific kind.
 */
void SMTSolver::notify_sort_decision(ModuleId id, SortKind k) {
    assert( variable_decision_by_sort[k] == NULL_MODULE );
    variable_decision_by_sort[k] = id;
}



//  PREPROCESSING
// ---------------

/*
 * remove nonboolean ite by introducing a new term
 *
 * c is a boolean condition, t and e are two term of the same (non-boolean) sort
 * f is an uninterpreted function or a theory specific relation (==, >=, ...)
 * x is a new variable of the same sort of t and e
 * (f (ite c t e)) ~> (and (f x) (ite c (= x t) (= x e)))
 */
Term SMTSolver::preprocess_ite(Term t) {
    TermKind k = term_table.kind_of(t);

    switch(k) {
        case TERM_ITE:
        {
            // nothing to do directly, search in sons
            const auto &comp = term_table.get_composite_args(t);

            // boolean ite are complex term
            if ( term_table.sort_of(comp[1]) == bool_sort ) {
                std::vector<Term> rebuild_sons(comp.size());
                bool changed = false;
                for ( size_t i = 0; i < comp.size(); ++i) {
                    rebuild_sons[i] = preprocess_ite(comp[i]);
                    if (rebuild_sons[i] != comp[i])
                        changed = true;
                }

                // rebuild term and return rebuilded term if necessary
                if (changed) {
                    Term rebuild = rebuild_composite(t, rebuild_sons);
                    if (t.is_negative())
                        rebuild = rebuild.opposite();
                    return rebuild;
                }

                // nothing change
                return t;
            }

            // replace nonboolean ite

            // build term
            Sort sort = term_table.sort_of(t);
            Term new_term  = term_table.add_unint_term(sort);

            // build equality for the two branch
            Term cond = comp[0];
            Term true_branch = comp[1]; 
            Term false_branch = comp[2]; 
            Term true_eq = term_table.add_binary_equality(new_term, true_branch);
            Term false_eq = term_table.add_binary_equality(new_term, false_branch);

            std::vector<Term> new_args = { cond, true_eq, false_eq };

            // Add equality
            term_table.check_ite(new_args);
            Term boolean_ite = term_table.add_ite(new_args);
            new_term_from_preproc.push_back(boolean_ite);

            // return the newly created substitute
            return new_term;
        }

        case TERM_ARITH_POLY:
        case TERM_ARITH_EQ:
        case TERM_ARITH_GE:
        {
            const auto &pol = term_table.get_polynomial(t);
            bool changed = false;
            std::vector<Term> new_args(pol.size());
            for (size_t i = 0; i < pol.size(); ++i) {
                if ( pol[i].indet == NULL_TERM ) continue;

                new_args[i] = preprocess_ite(pol[i].indet);
                if (new_args[i] != pol[i].indet)
                    changed = true;
            }
            if (changed) {
                Term rebuild = rebuild_polynomial_term(t,pol,new_args);
                if (t.is_negative())
                    rebuild = rebuild.opposite();
                return rebuild;
            }

            return t;
        }

        case TERM_FUN_APPLICATION:
        case TERM_OR:
        case TERM_XOR:
        case TERM_EQ:
        case TERM_DISTINCT:
        {
            // nothing to do directly, search in sons
            const auto &comp = term_table.get_composite_args(t);
            std::vector<Term> rebuild_sons(comp.size());
            bool changed = false;
            for ( size_t i = 0; i < comp.size(); ++i) {
                rebuild_sons[i] = preprocess_ite(comp[i]);
                if (rebuild_sons[i] != comp[i])
                    changed = true;
            }

            // rebuild term and return rebuilded term if necessary
            if (changed) {
                Term rebuild = rebuild_composite(t, rebuild_sons);
                if (t.is_negative())
                    rebuild = rebuild.opposite();
                return rebuild;
            }

            // nothing change
            return t;
        }


        case TERM_BOOL_CONSTANT:
        case TERM_REAL_CONSTANT:
        case TERM_UNINTERPRETED:
        default:
            return t;
    }
}

/*
 * remove ite term with cofactoring
 */
Term SMTSolver::preprocess_ite_cofact(Term t) {

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << term_table << std::endl;
        debug << "SOLVER: process ite " << term_table.pprint(t) << std::endl;
    }

    TermKind k = term_table.kind_of(t);

    switch(k) {

        case TERM_FUN_APPLICATION:
        case TERM_OR:
        case TERM_XOR:
        case TERM_ITE:
        {
            // nothing to do directly, search in sons
            const auto &comp = term_table.get_composite_args(t);
            std::vector<Term> rebuild_sons(comp.size());
            bool changed = false;
            for ( size_t i = 0; i < comp.size(); ++i) {
                rebuild_sons[i] = preprocess_ite_cofact(comp[i]);
                if (rebuild_sons[i] != comp[i])
                    changed = true;
            }

            // rebuild term and return rebuilded term if necessary
            if (changed) {
                Term rebuild = rebuild_composite(t, rebuild_sons);
                if (t.is_negative())
                    rebuild = rebuild.opposite();
                return rebuild;
            }

            // nothing change
            return t;
        }

        case TERM_EQ:
        case TERM_DISTINCT:
        {
            const auto &comp = term_table.get_composite_args(t);

            // do directly, search in sons
            if ( term_table.sort_of(comp[0]) == bool_sort ) {
                std::vector<Term> rebuild_sons(comp.size());
                bool changed = false;
                for ( size_t i = 0; i < comp.size(); ++i) {
                    rebuild_sons[i] = preprocess_ite_cofact(comp[i]);
                    if (rebuild_sons[i] != comp[i])
                        changed = true;
                }

                // rebuild term if necessary
                if (changed) {
                    Term rebuild = rebuild_composite(t, rebuild_sons);
                    if (t.is_negative())
                        rebuild = rebuild.opposite();
                    return rebuild;
                }

                // nothing change
                return t;
            }

            // nonboolean relation, look for ite
            bool has_ite = false;
            std::unordered_set<Term> cofact_terms;
            for ( size_t i = 0; i < comp.size(); ++i) {
                has_ite = require_cofactor(comp[i], cofact_terms) || has_ite;
            }

            if ( has_ite ) {
                std::vector<Term> new_args;
                for( Term c : cofact_terms ) {
                    if (MESSAGE_LEVEL(VERBOUSE_MESSAGE)) {
                        debug << "formula " << term_table.pprint(t) <<
                            " factor " << term_table.pprint(c) << std::endl;
                    }

                    // calculate the two cofactors
                    Term pos_cofact = cofactor_recur(t, c);
                    Term neg_cofact = cofactor_recur(t, c.opposite());

                    // build the cofactorized term
                    new_args = {c, pos_cofact, neg_cofact};
                    t = term_table.add_ite(new_args);
                    if (MESSAGE_LEVEL(VERBOUSE_MESSAGE)) {
                        debug << "pos cofactor " << term_table.pprint(pos_cofact)
                            << std::endl <<
                            "neg cofactor " << term_table.pprint(neg_cofact)
                            << std::endl <<
                            "new formula " << term_table.pprint(t) << std::endl;
                    }
                }
                return t;
            }

            // nothing to do
            return t;
        }

        case TERM_ARITH_EQ:
        case TERM_ARITH_GE:
        {
            const auto &pol = term_table.get_polynomial(t);

            bool has_ite = false;
            std::unordered_set<Term> cofact_terms;

            for ( size_t i = 0; i < pol.size(); ++i) {
                if (pol[i].indet == NULL_TERM) continue;
                bool require = require_cofactor(pol[i].indet,cofact_terms);
                if (require)
                    has_ite = true;
            }

            if ( has_ite ) {
                std::vector<Term> new_args;
                for( Term c : cofact_terms ) {
                    if (MESSAGE_LEVEL(VERBOUSE_MESSAGE)) {
                        debug << "formula " << term_table.pprint(t) <<
                            " factor " << term_table.pprint(c) << std::endl;
                    }

                    // calculate the two cofactors
                    Term pos_cofact = cofactor_recur(t, c);
                    Term neg_cofact = cofactor_recur(t, c.opposite());

                    // build the cofactorized term
                    new_args = {c, pos_cofact, neg_cofact};
                    t = term_table.add_ite(new_args);

                    if (MESSAGE_LEVEL(VERBOUSE_MESSAGE)) {
                        debug << "pos cofactor " << term_table.pprint(pos_cofact)
                            << std::endl <<
                            "neg cofactor " << term_table.pprint(neg_cofact)
                            << std::endl <<
                            "new formula " << term_table.pprint(t) << std::endl;
                    }
                }

                return t;
            }

            // nothing to do
            return t;
        }

        default:
            // nothing to do otherwise
            return t;
    }
}

bool SMTSolver::require_cofactor(Term t, std::unordered_set<Term> &cofact_terms) {
    TermKind k = term_table.kind_of(t);

    switch(k) {

        case TERM_ARITH_POLY:
        {
            const auto &pol = term_table.get_polynomial(t);
            bool has_ite = false;
            for ( size_t i = 0; i < pol.size(); ++i) {
                if ( pol[i].indet == NULL_TERM ) continue;

                bool require = require_cofactor(pol[i].indet,cofact_terms);
                if (require) has_ite = true;
            }
            return has_ite;
        }

        case TERM_ARITH_EQ:
        case TERM_ARITH_GE:
        {
            const auto &pol = term_table.get_polynomial(t);
            bool has_ite = false;

            // add the relation to the cofactor therm
            cofact_terms.insert(t);

            for ( size_t i = 0; i < pol.size(); ++i) {
                if ( pol[i].indet == NULL_TERM ) continue;

                bool require = require_cofactor(pol[i].indet,cofact_terms);
                if (require) has_ite = true;
            }

            return has_ite;
        }

        case TERM_ITE:
        {
            const auto &comp = term_table.get_composite_args(t);
            if ( term_table.sort_of(comp[1]) == bool_sort ) {
                // is a normal composite term
                bool has_ite = false;
                for ( size_t i = 0; i < comp.size(); ++i) {
                    bool require = require_cofactor(comp[i],cofact_terms);
                    if (require) has_ite = true;
                }
                return has_ite;
            }
            else {
                // we have foundt an ite to factorize! collect the cofactoring
                // terms and return true
                for ( size_t i = 0; i < comp.size(); ++i) {
                    require_cofactor(comp[i],cofact_terms);
                }
                return true;
            }
        }

        case TERM_OR:
        case TERM_XOR:
        case TERM_EQ:
        case TERM_DISTINCT:
        {
            const auto &comp = term_table.get_composite_args(t);
            bool has_ite = false;
            for ( size_t i = 0; i < comp.size(); ++i) {
                bool require = require_cofactor(comp[i],cofact_terms);
                if (require) has_ite = true;
            }
            cofact_terms.insert(t);
            return has_ite;
        }

        case TERM_FUN_APPLICATION:
        {
            const auto &comp = term_table.get_composite_args(t);
            bool has_ite = false;
            for ( size_t i = 0; i < comp.size(); ++i) {
                bool require = require_cofactor(comp[i],cofact_terms);
                if (require) has_ite = true;
            }
            if (sort_table.get_function_codomain(term_table.sort_of(t)) == bool_sort)
                cofact_terms.insert(t);
            return has_ite;
        }

        case TERM_UNINTERPRETED:
            if ( term_table.sort_of(t) == bool_sort )
                cofact_terms.insert(t.unsign());
            return false;

        default:
            return false;
    }
}

Term SMTSolver::cofactor_recur(Term formula, Term factor) {
    // terminal cases
    if ( formula == factor )
        return true_term;
    if ( formula == factor.opposite() )
        return false_term;

    TermKind k = term_table.kind_of(formula);

    switch(k) {
        case TERM_EQ:
        case TERM_DISTINCT:
        {
            const auto &comp = term_table.get_composite_args(formula);

            //if (term_table.sort_of(comp[0]) == bool_sort) {
            bool changed = false;
            std::vector<Term> new_args(comp.size());
            for (size_t i = 0; i < comp.size(); ++i) {
                new_args[i] = cofactor_recur(comp[i], factor);
                if (new_args[i] != comp[i])
                    changed = true;
            }

            if (changed) {
                Term rebuild = rebuild_composite(formula, new_args);
                if (formula.is_negative())
                    rebuild = rebuild.opposite();
                return rebuild;
            }

            return formula;
            //}
        }

        case TERM_ARITH_EQ:
        case TERM_ARITH_GE:
        case TERM_ARITH_POLY:
        {
            const auto &pol = term_table.get_polynomial(formula);
            bool changed = false;
            std::vector<Term> new_args(pol.size());
            for (size_t i = 0; i < pol.size(); ++i) {
                if (pol[i].indet == NULL_TERM) continue;
                new_args[i] = cofactor_recur(pol[i].indet, factor);
                if (new_args[i] != pol[i].indet)
                    changed = true;
            }
            if (changed) {
                Term rebuild = rebuild_polynomial_term(formula,pol,new_args);
                if (formula.is_negative())
                    rebuild = rebuild.opposite();
                return rebuild;
            }
            return formula;
        }

        case TERM_ITE:
        case TERM_FUN_APPLICATION:
        case TERM_OR:
        case TERM_XOR:
        {
            const auto &comp = term_table.get_composite_args(formula);
            bool changed = false;
            std::vector<Term> new_args(comp.size());
            for (size_t i = 0; i < comp.size(); ++i) {
                new_args[i] = cofactor_recur(comp[i], factor);
                if (new_args[i] != comp[i])
                    changed = true;
            }
            if (changed) {
                Term rebuild = rebuild_composite(formula, new_args);
                if (formula.is_negative())
                    rebuild = rebuild.opposite();
                return rebuild;
            }
            return formula;
        }

        case TERM_UNINTERPRETED:
        default:
            // nothing to do
            return formula;
    }
}

/*
 * normalization of term.
 * If the term is complex (i.e. have some child), all the children must be
 * processed before the processing of the term
 */
Term SMTSolver::preprocess_term(Term original) {
    
    // search if the value is already preprocessed
    Term t_prep = preprocessed_cache[original];
    if ( t_prep != NULL_TERM )
        return t_prep;

    if ( MESSAGE_LEVEL( VERBOUSE_MESSAGE ) ) {
        debug << term_table << std::endl;
        debug << "SOLVER: preprocess term " << term_table.pprint(original)
            << std::endl;
    }
    
    // initialize a preprocessing stack with original inside
    std::vector<Term> preprocessing_stack = {original};

    // preprocess terms and subterms
    while ( ! preprocessing_stack.empty() ) {

        // the term to process (don't pop)
        Term to_proc   = preprocessing_stack.back();
        Term processed = preprocessed_cache[to_proc];

        // if t is already preprocessed pop the term and continue
        if ( processed != NULL_TERM ) {
            preprocessing_stack.pop_back();
            continue;
        }

        // normalize negated term
        if ( to_proc.is_negative() ) {

            // get the positive
            Term pos      = to_proc.unsign();
            Term pos_prep = preprocessed_cache[pos];

            // if unsigned is NOT inside the preprocessing map
            if ( pos_prep == NULL_TERM ) {
                // preprocess the positive term before the negative one
                preprocessing_stack.push_back(pos);
                continue;
            }
            else {
                // map to the opposite
                preprocessed_cache[to_proc] = preprocessed_cache[pos].opposite();
                preprocessing_stack.pop_back();
                continue;
            }
        }

        assert( to_proc.is_positive() );
        
        // preprocess based on the kind of term...
        switch ( term_table.kind_of( to_proc ) ) {

            // no change required
            case TERM_UNINTERPRETED:
            case TERM_BOOL_CONSTANT:
            case TERM_REAL_CONSTANT:
                processed = to_proc;
                break;

            // case arith EQ and arith GE 
            case TERM_ARITH_EQ:
            case TERM_ARITH_GE:
                // TODO
                // nothing for now!
                processed = to_proc;
                break;

            case TERM_ARITH_POLY:
                // TODO
                // nothing for now!
                processed = to_proc;
                break;

            case TERM_ITE:
            case TERM_EQ:
            case TERM_OR:
            case TERM_XOR:
                processed = preprocess_boolean_op(to_proc,preprocessing_stack);
                break;

            case TERM_FUN_APPLICATION:
                processed = preprocess_fun_app(to_proc,preprocessing_stack);
                break;

            case TERM_DISTINCT:
                processed = preprocess_distinct(to_proc,preprocessing_stack);
                break;

            default:
                assert(false);
                return NULL_TERM;
                break;
        }

        // if possible, update the cache, otherwise don't pop the term and
        // process the new term in the stack
        if ( processed != NULL_TERM ) {
            preprocessed_cache[to_proc] = processed;
            preprocessing_stack.pop_back();
        }
    }

    t_prep = preprocessed_cache[original];
    assert( t_prep != NULL_TERM );

    // return preprocessed term
    return t_prep;
}

/*
 * all the component of a complex boolean operation must be preprocessed.
 * If some of the child are not already preprocessed the processing of the
 * operation are postponed after the children preprocessing.
 *
 * If the preprocess of the children change one of the child, the complex
 * term must be rebuild, otherwise nothing is changed
 */
Term SMTSolver::preprocess_boolean_op(Term to_proc,
        std::vector<Term> &preprocessing_stack) {

    // get the description as a composite term
    const auto &description = term_table.get_composite_args( to_proc );
    bool children_done = true;
    bool children_same = true;

    // if the equality is between function type return an error!
    if ( term_table.kind_of(to_proc) == TERM_EQ &&
            sort_table.is_function(term_table.sort_of(description[0])) ) {
        trail.set_inconsistent(true);
    }

    // create array for the n children = to composite size
    std::vector<Term> children;

    // for all element in the description
    for ( const Term &child : description ) {
        Term child_prep = preprocessed_cache[child];

        // if the children is NOT already preprocess (map)
        if ( child_prep == NULL_TERM ) {
            children_done = false;
            preprocessing_stack.push_back(child);
        }
        // else if the preprocessed is different from the  term
        else if ( child_prep != child )
            children_same = false;

        // if children done add it to the children vector
        if ( children_done )
            children.push_back(child_prep);
    }

    if (children_done) {
        if (children_same)
            // if all the children are the same keep the term
            return to_proc;
        else  {
            // otheriwise create a new preprocessed term
            return rebuild_composite(to_proc, children);
        }
    }

    // it's required to preprocess all the children before processing this
    // complex term
    return NULL_TERM;
}

/*
 * all the term of a function application must be normalized, so
 * for example f(x+y) become f(w) ^ w = x+y.
 * before the purification the subterms must be preprocessed.
 */
Term SMTSolver::preprocess_fun_app(Term to_proc,
        std::vector<Term> &preprocessing_stack) {
    // get the description as a composite term
    const auto &description = term_table.get_composite_args( to_proc );
    bool children_done = true;
    bool children_same = true;

    // create array for the n children = to composite size
    std::vector<Term> children;

    // for all element in the description
    for ( const Term &child : description ) {
        Term child_prep = preprocessed_cache[child];

        // if the children is NOT already preprocess (map)
        if ( child_prep == NULL_TERM ) {
            children_done = false;
            preprocessing_stack.push_back(child);
        }
        // else if the preprocessed is different from the  term
        else {
            child_prep = prepocess_purify(child_prep);
            if ( child_prep != child )
                children_same = false;
        }

        // if children done add it to the children vector
        if ( children_done )
            children.push_back(child_prep);
    }

    if (children_done) {
        if (children_same)
            // if all the children are the same keep the term
            return to_proc;
        else
            // otheriwise create a new preprocessed term
            return rebuild_composite(to_proc, children);
    }

    // the children must be preprocessed before
    return NULL_TERM;
}

/*
 * normalize the distinct term to not equal.
 * if the distinct have more than two child, build a conjunction of the
 * new equaliy.
 * for example:
 * (distinct x y) ~> (not (eq x y))
 * (distinct x y z) ~> (and (not (eq x y)) (not (eq x z)) (not (eq y z)) )
 * and so on...
 * all the children must be preprocessed before the distinct
 */
Term SMTSolver::preprocess_distinct(Term to_proc,
        std::vector<Term> &preprocessing_stack) {
    // get the description as a composite term
    const auto &description = term_table.get_composite_args( to_proc );
    bool children_done = true;

    // create array for the n children = to composite size
    std::vector<Term> children;

    // for all element in the description
    for ( const Term &child : description ) {
        Term child_prep = preprocessed_cache[child];

        // if the children is NOT already preprocess (map)
        if ( child_prep == NULL_TERM ) {
            children_done = false;
            preprocessing_stack.push_back(child);
        }

        // if children done add it to the children vector
        if ( children_done )
            children.push_back(child_prep);
    }

    if (children_done) {
        // build an and vector, if the distinct have only two term the and
        // is simplified out by the term_table construction
        std::vector<Term> distinct;
        for (size_t i = 0; i < children.size()-1; ++ i) {
            for (size_t j = i+1; j < children.size(); ++ j) {
                Term neq = term_table.add_binary_equality(
                        children[i], children[j]).opposite();
                distinct.push_back(neq);
            }
        }
        return term_table.add_and(distinct);
    }

    // the children must be preprocessed before
    return NULL_TERM;
}

Term SMTSolver::rebuild_polynomial_term(Term t, const Polynomial &pol,
        std::vector<Term> &args) {

    std::vector<Monomial> new_poly_vect;
    assert( pol.size() == args.size() );
    for (size_t i = 0; i < pol.size(); ++i) {

        // it change to a constant?
        if ( args[i] != NULL_TERM &&
                term_table.kind_of(args[i]) == TERM_REAL_CONSTANT) {
            Rational q = term_table.get_rational(args[i]);
            add_monomial(new_poly_vect,{q,NULL_TERM}); 
        }
        else {
            add_monomial(new_poly_vect,{pol[i].coeff,args[i]}); 
        }
    }

    auto new_pol = Polynomial::allocate(new_poly_vect);
    Term new_pol_term = term_table.add_polynomial(new_pol);

    switch( term_table.kind_of(t) ) {
        case TERM_ARITH_EQ:
            return term_table.add_arith_equality(new_pol_term,zero_term);
        case TERM_ARITH_GE:
            return term_table.add_binary_ge(new_pol_term,zero_term);
        case TERM_ARITH_POLY:
            return new_pol_term;
        default:
            // nothing to do
            return t;
    }
}

/*
 * if some of the children of a composite term are changed by the
 * prerocessing, a new term must be build with the preprocessed term 
 * instead of the original one.
 */
Term SMTSolver::rebuild_composite(Term t, std::vector<Term> &args) {
    Term tmp;
    switch( term_table.kind_of(t) ) {
        case TERM_ITE:
            term_table.check_ite(args);
            return term_table.add_ite(args);
        case TERM_EQ:
            return term_table.add_equality(args);
        case TERM_OR:
            return term_table.add_or(args);
        case TERM_XOR:
            return term_table.add_xor(args);
        case TERM_FUN_APPLICATION:
            // the function is on the back of the composite vector
            tmp = args.back(); args.pop_back();
            return term_table.add_fun_app(tmp, args);
        default:
            return NULL_TERM;
    }
}

/*
 * purify the term inside a function application.
 * variables and function application are already purified, all the other
 * need to be purified with an equality.
 */
Term SMTSolver::prepocess_purify(Term t) {
    auto kind = term_table.kind_of(t);

    // uninterpeted and function application are already pure
    if( kind == TERM_UNINTERPRETED || kind == TERM_FUN_APPLICATION )
        return t;

    auto it = purify_cache.find(t);
    if ( it != purify_cache.end() )
        return it->second;

    // build term
    Sort sort = term_table.sort_of(t);
    Term pur  = term_table.add_unint_term(sort);

    // save term
    purify_cache[t] = pur;

    // Add equality
    Term eq = term_table.add_binary_equality(pur, t);
    new_term_from_preproc.push_back(eq);

    return pur;
}

