/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_CORE_SMT_SOLVER_HH
#define EOS_CORE_SMT_SOLVER_HH

#include <array>
#include <algorithm>
#include <functional>
#include <map>
#include <queue>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "term.h"
#include "sort.h"
#include "trail.h"
#include "model.h"
#include "variable.h"
#include "variable_order.h"
#include "theory_module.h"

/**
 * logic
 */
enum smt_logic_t {
    LOGIC_UNKNOWN = 0,
    LOGIC_QF_UF,
    LOGIC_QF_LRA,
    LOGIC_QF_UFLRA
};

enum solver_state_t {
    STATE_UNSAT,
    STATE_SAT,
    STATE_UNKNOWN
};

static std::unordered_map<std::string, smt_logic_t> name_to_logic = {
    {"QF_UF",LOGIC_QF_UF}, {"QF_LRA",LOGIC_QF_LRA},
    {"QF_UFLRA",LOGIC_QF_UFLRA}, {"ALL",LOGIC_QF_UFLRA} };

class SMTSolver {
public:
    explicit SMTSolver(TermTable &tt);

    void   asserts(const std::vector<Term> &assertions);
    bool   check_sat();
    const Model &get_model();

    TermTable &get_term_table() { return term_table; }

    SortTable &get_sort_table() { return sort_table; }

    VariableTable &get_variable_table() { return variable_table; }

    Trail &get_trail() { return trail; }

    void set_logic(smt_logic_t l);

    uint32_t add_theory_module(TheoryModule *s);

    void register_new_var(Variable var);

    void report_conflict(ModuleId i);
    void report_lemma(Term lemma);
    void increase_activity(Variable var);

    void notify_term_ownership(ModuleId id, TermKind k);
    void notify_sort_ownership(ModuleId id, SortKind k);

    void notify_sort_decision(ModuleId id, SortKind k);
private:

    void backtrack_to(size_t l);

    void process_registration_queue();

    // propagate effect of assigned variables
    void propagate();

    void decide_one_of(const std::vector<Term> &terms);

    // open and close levels
    void push_level();
    void pop_level();

    bool decide_value();

    // analyze the last conflict and backtrack to the appropiate level
    void analyze_conflict();

    // if then else elimination (two available heuristics)
    Term preprocess_ite(Term t);
    Term preprocess_ite_cofact(Term t);

    bool require_cofactor(Term t, std::unordered_set<Term> &cofact_terms);
    Term cofactor_recur(Term formula, Term factor);

    // preprocessing terms
    Term preprocess_term(Term t);
    Term preprocess_boolean_op(Term , std::vector<Term> &);
    Term preprocess_fun_app(Term , std::vector<Term> &);
    Term preprocess_distinct(Term , std::vector<Term> &);

    Term prepocess_purify(Term t);
    Term rebuild_composite(Term t, std::vector<Term> &args);
    Term rebuild_polynomial_term(Term t, const Polynomial &pol,
            std::vector<Term> &args);

    void   assert_formula(Term t);

    // tables
    SortTable     &sort_table;
    TermTable     &term_table;
    VariableTable variable_table;

    // theory modules
    std::vector<TheoryModule *> theory_modules;

    // execution info
    Trail trail;
    std::vector<Variable> asserted_vars;

    // info
    int log_level = 0;
    smt_logic_t  logic;

    std::unordered_set<Term> notify_cache;

    // restartin policy (TODO: possibly move to some parameter class?)
    const unsigned int restart_interval_multiplier = 100;

    // values for luby sequence, used for restart policy
    uint32_t luby_next = 0;

    uint32_t next_restart_interval(uint32_t pos);
    uint32_t new_restart_threshold();

    // VSIDS
    VariableOrder order;

    // preprocess nonboolean ite term with cofactoring
    // otherwise a new term is created in place of the ite
    bool cofactoring_ite = false;

    // current module in conflict
    ModuleId module_in_conflict = NULL_MODULE;

    // preprocessing data
    std::unordered_map<Term,Term> preprocessed_cache;
    std::unordered_map<Term,Term> ite_cache;
    std::unordered_map<Term,bool> has_ite_cache;
    std::unordered_map<Term,Term> purify_cache;
    std::vector<Term> new_term_from_preproc;

    // assertion data
    std::vector<Term> new_term_from_assert;

    // registration of new variables
    std::queue<Term> registration_queue;
    std::array<std::vector<ModuleId>, TOTAL_TERM_KIND> term_owner_by_kind;
    std::array<std::vector<ModuleId>, TOTAL_SORT_KIND> term_owner_by_sort;
    std::array<ModuleId, TOTAL_SORT_KIND> variable_decision_by_sort;

    // solver state
    solver_state_t state = STATE_UNKNOWN;

    Model model;
};

#endif // EOS_CORE_SMT_SOLVER_HH
