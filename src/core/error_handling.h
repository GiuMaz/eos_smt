/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_ERROR_HANDLING_HH
#define EOS_ERROR_HANDLING_HH

#include <stdexcept>

class parsing_exception : public std::runtime_error {
public:
    parsing_exception(const std::string &what) : runtime_error(what) {}
};

class invalid_command_exception : public std::runtime_error {
public:
    invalid_command_exception(const std::string &what) : runtime_error(what) {}
};

#endif // EOS_ERROR_HANDLING_HH
