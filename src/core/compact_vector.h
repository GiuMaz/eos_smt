/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef EOS_COMPACT_VECTOR_HH
#define EOS_COMPACT_VECTOR_HH

#include <cassert>
#include <iostream>
#include <cstring>
#include <string>
#include <vector>

/**
 * Compact Vector
 * This is a template that create small, compact vector. It allocate in
 * contiguous memory both the header (that have the size and possible some
 * extra data strcuture, if speficied in compact_vector_with_header) and
 * the data.
 */

// compact array
template <class T>
class compact_vector {
protected:

    const size_t _size;

    explicit compact_vector(size_t s) : _size(s) {
    }

    explicit compact_vector(const std::vector<T> &vect) : _size(vect.size()) {
        std::copy(vect.begin(),vect.end(),this->begin());
    }

    compact_vector(const T *args, size_t s) : _size(s) {
        std::memcpy(begin(), args, s);
    }

    // allocate empty vector, it should have at least one component
    static compact_vector<T>* zero_allocate() {
        auto size = sizeof(compact_vector<T>) + sizeof(T)*1;
        void* memory =  malloc(size);
        assert( memory != nullptr);
        return new (memory) compact_vector<T>(0);
    }

public:
    using Iterator = T *;
    using ConstIterator = const T *;

    static compact_vector<T>* allocate(size_t s) {
        if ( s == 0 ) return zero_allocate();
        auto size = sizeof(compact_vector<T>) + sizeof(T)*s;
        void* memory =  malloc(size);
        assert( memory != nullptr);
        return new (memory) compact_vector<T>(s);
    }

    static compact_vector<T>* allocate(const std::vector<T> &args) {
        if ( args.size() == 0 ) return zero_allocate();
        auto size = sizeof(compact_vector<T>) + sizeof(T)*args.size();
        void* memory =  malloc(size);
        assert( memory != nullptr);
        return new (memory) compact_vector<T>(args);
    }

    static compact_vector<T>* allocate(const T *args, size_t s) {
        if ( s == 0 ) return zero_allocate();
        auto size = sizeof(compact_vector<T>) + sizeof(T)*s;
        void* memory =  malloc(size);
        assert( memory != nullptr);
        return new (memory) compact_vector<T>(args,s);
    }

    static void deallocate(compact_vector<T>* &c) {
        assert( c != nullptr);
        c->~compact_vector<T>();
        free((void*)c);
        c = nullptr;
    }

    uint64_t size() const { return _size; }

    const T* data() const {
        return reinterpret_cast<const T*>(this+1);
    }
    T* data() {
        return reinterpret_cast<T*>(this+1);
    }

    Iterator begin() { return data(); }
    Iterator end()   { return data() + _size; }
    ConstIterator begin() const { return data(); }
    ConstIterator end()   const { return data() + _size; }

    T& at(size_t i) { return data()[i]; }
    const T& at(size_t i) const { return data()[i]; }
    T& operator [] (size_t i) { return data()[i]; }
    const T& operator [] (size_t i) const { return data()[i]; }
};

template<class T>
inline std::ostream &operator<<( std::ostream &os,const compact_vector<T> &c) {
    os << "[ ";
    for ( const auto &l : c )
        os << l << " ";
    os << "]";
    return os;
}


template <class T, class H>
class compact_vector_with_header {

    using Iterator = T *;
    using ConstIterator = const T *;
private:

    const size_t _size;
    H header;

    compact_vector_with_header(H h, size_t s) : _size(s), header(h) {
    }

    compact_vector_with_header(const H &h, const std::vector<T> &vect):
        _size(vect.size()), header(h) {
        std::copy(vect.begin(),vect.end(),this->begin());
    }
    compact_vector_with_header(const H &h,const T *args, size_t s):
        _size(s), header(h) {
        std::memcpy(begin(), args, s);
    }

    // allocate empty vector, it should have at least one component
    static compact_vector_with_header<T,H>* zero_allocate(const H &h) {
        auto size = sizeof(compact_vector_with_header<T,H>) + sizeof(T)*1;
        void* memory =  malloc(size);
        assert( memory != nullptr);
        return new (memory) compact_vector_with_header<T,H>(h,0);
    }

public:
    static compact_vector_with_header<T,H>* allocate(
            const H  &h, size_t s) {
        if ( s == 0 ) zero_allocate(h);
        auto size = sizeof(compact_vector_with_header<T,H>) + sizeof(T)*s;
        void* memory =  malloc(size);
        assert( memory != nullptr);
        return new (memory) compact_vector_with_header<T,H>(h,s);
    }

    static compact_vector_with_header<T,H>* allocate(
            const H  &h, const std::vector<T> &args) {
        if ( args.size() == 0 ) zero_allocate(h);
        auto size = sizeof(compact_vector_with_header<T,H>) + sizeof(T)*args.size();
        void* memory =  malloc(size);
        assert( memory != nullptr);
        return new (memory) compact_vector_with_header<T,H>(h,args);
    }

    static compact_vector_with_header<T,H>* allocate(
            const H &h, const T *args, size_t s) {
        if ( s == 0 ) zero_allocate(h);
        auto size = sizeof(compact_vector_with_header<T,H>) + sizeof(T)*s;
        void* memory =  malloc(size);
        assert( memory != nullptr);
        return new (memory) compact_vector_with_header<T,H>(h,args,s);
    }

    static void deallocate(compact_vector_with_header<T,H>* &c) {
        assert( c != nullptr);
        c->~compact_vector_with_header<T,H>();
        free((void*)c);
        c = nullptr;
    }

    H &get_header() { return header; }
    const H &get_header() const { return header; }

    uint64_t size() const { return _size; }

    const T* data() const {
        return reinterpret_cast<const T*>(this+1);
    }

    T* data() {
        return reinterpret_cast<T*>(this+1);
    }

    Iterator begin() { return data(); }
    Iterator end()   { return data() + _size; }
    ConstIterator begin() const { return data(); }
    ConstIterator end()   const { return data() + _size; }

    T& at(size_t i) { return data()[i]; }
    const T& at(size_t i) const { return data()[i]; }
    T& operator [] (size_t i) { return data()[i]; }
    const T& operator [] (size_t i) const { return data()[i]; }
};

template<class T, class H>
inline std::ostream &operator<<( std::ostream &os,const compact_vector_with_header<T,H> &c) {
    os << "header: " << c.get_header() << " [ ";
    for ( const auto &l : c )
        os << l << " ";
    os << "]";
    return os;
}



class compact_string : public compact_vector<char> {
protected:
    compact_string(const char *ptr, size_t s) : compact_vector<char>(ptr,s) {}

public:
    static compact_string *allocate(const std::string &str) {
        // the array have an extra hidden space for a \0
        auto size = sizeof(compact_string) + sizeof(char)*(str.size()+1);
        void* memory =  malloc(size);
        assert( memory != nullptr);
        auto ptr =  new (memory) compact_string(str.data(),str.size());
        // have both a c++ string type features and a zero terminated char *
        ptr->at(str.size()) = '\0';
        return ptr;
    }

    // null terminated string in c style
    static compact_string *allocate(const char *str) {

        // find string size
        size_t str_size = 0;
        const char *ch = str;
        while ( *ch++ ) ++str_size;

        auto size = sizeof(compact_string) + sizeof(char)*(str_size+1);
        void* memory =  malloc(size);
        assert( memory != nullptr );

        auto ptr =  new (memory) compact_string(str,str_size);
        // have both a c++ string type features and a zero terminated char *
        ptr->at(str_size) = '\0';
        return ptr;
    }

    static void deallocate(compact_string* &c) {
        assert( c != nullptr);
        c->~compact_string();
        free((void*)c);
        c = nullptr;
    }

};



#endif // EOS_COMPACT_VECTOR_HH
