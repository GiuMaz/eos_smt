/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "printing.h"
#include <iostream>

message_level_t global_message_level = STANDARD_MESSAGE;

std::ostream standard( std::cout.rdbuf() );
std::ostream debug( std::cerr.rdbuf() );

void set_message_level( message_level_t lv ) {
    global_message_level = lv;
}

void set_default_standard_stream() {
    standard.rdbuf( std::cout.rdbuf() );
}

void set_default_debug_stream() {
    debug.rdbuf( std::cerr.rdbuf() );
}

