/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_CORE_RATIONAL_HH
#define EOS_CORE_RATIONAL_HH

#include <cassert>
#include <iostream>
#include "printing.h"
#include <string>



/*
 * greater common divisor of two number
 */
inline long long gcd(long long x, long long y) {
    x = std::abs(x);
    y = std::abs(y);
    while ( y != 0 ) {
        long long tmp = y;
        y = x % y;
        x = tmp;
    }
    assert( x >= 0 );
    return x;
}

/*
 * least common multiple
 */
inline long long lcm(long long x, long long y) {
    // lcm(a,b) == a*b / gcd(a,b)
    return std::abs(x*(y/gcd(x,y)));
}

/**
 * Rational Number Class
 * use two integer for the rappresentation of a number in the rational set Q.
 * All the operation are written with the goal of minimizing the overload
 * problem that can occour with the manipulation of the number.
 * The class work with simple integer or with other Rational, and can be
 * converted to floating point number
 */
class Rational {
    // numerator and denominator
    long long num;
    long long den;

    // all the rational are manteined in a normalized form:
    // - gcd(num,den) = 1
    // - den > 0
    void normalize();

public:

    // constructor and destructor
    Rational(): num(0), den(1) {}
    explicit Rational(long long n): num(n), den(1) {}
    Rational(long long n, long long d): num(n), den(d) { normalize(); }
    Rational(const Rational &r) = default;

    explicit Rational(const std::string &s): num(0), den(1) {

        auto it = s.begin();

        // before .
        while ( it != s.end() ) {
            assert( *it == '.' || (*it>='0' && *it<='9'));

            if ( *it == '.' ) break;

            num *= 10;
            num += (*it - '0');

            ++it;
        }

        // point?
        if ( it != s.end() ) {
            // skip point
            ++it;

            // after .
            while ( it != s.end() ) {
                assert(*it>='0' && *it<='9');

                den *= 10;
                num *= 10;
                num += (*it - '0');

                ++it;
            }
        }

        normalize();
        assert( num != 0 || den == 1 );
        assert( gcd(num,den) == 1 );
        assert( den > 0 );
    }

    ~Rational() = default;

    // getter
    long long get_num()   const { return num; }
    long long get_denom() const { return den; }

    size_t get_hash() const {
        return (static_cast<size_t>(get_num())<<32) +
            (static_cast<size_t>(get_denom()));
    }

    // in-place assignment
    Rational & assign(long long n) { num = n; den = 1; return *this; }
    Rational & assign(long long n, long long d) {
        // avoid invalid number
        if ( den == 0 ) throw std::domain_error("0 denominator");

        if ( d >= 0 ) {
            num = n;
            den = d;
        }
        else {
            num = -n;
            den = -d;
        }

        // normalize gcd
        long long g = gcd(num,den);
        num /= g;
        den /= g;

        assert( num != 0 || den == 1 );
        assert( gcd(num,den) == 1 );
        assert( den > 0 );

        return *this;
    }

    // assignment operator from in and Rational
    Rational & operator=(long long rhs) { num = rhs; den = 1; return *this; }
    Rational & operator=(const Rational &rhs)  = default;

    // single sign operator
    Rational operator-() const { return Rational(-num,den); }
    Rational operator+() const { return *this; }

    // assignment-level expression with long long
    Rational & operator+=(long long rhs);
    Rational & operator-=(long long rhs);
    Rational & operator*=(long long rhs);
    Rational & operator/=(long long rhs);

    // assignment-level expression with Rational
    Rational & operator+=(const Rational &rhs);
    Rational & operator-=(const Rational &rhs);
    Rational & operator*=(const Rational &rhs);
    Rational & operator/=(const Rational &rhs);

    // arithmetic operation with integer
    Rational operator+(long long rhs) const { return Rational(*this) += rhs; }
    Rational operator-(long long rhs) const { return Rational(*this) -= rhs; }
    Rational operator*(long long rhs) const { return Rational(*this) *= rhs; }
    Rational operator/(long long rhs) const { return Rational(*this) /= rhs; }

    // arithmetic operation with Rational
    Rational operator+(const Rational &r) const { return Rational(*this) += r; }
    Rational operator-(const Rational &r) const { return Rational(*this) -= r; }
    Rational operator*(const Rational &r) const { return Rational(*this) *= r; }
    Rational operator/(const Rational &r) const { return Rational(*this) /= r; }

    // comparison operator with integer
    bool operator!=(long long rhs) const { return den != 1 || num != rhs; }
    bool operator==(long long rhs) const { return num == rhs && den == 1; }

    bool operator<(long long rhs)  const;
    bool operator<=(long long rhs) const { return operator==(rhs) || operator<(rhs); }
    bool operator>(long long rhs)  const { return ! operator<=(rhs); }
    bool operator>=(long long rhs) const { return ! operator<(rhs); }

    // comparison operator with Rational
    bool operator==(const Rational &rhs) const {
        return num == rhs.num && den == rhs.den;
    }
    bool operator!=(const Rational &rhs) const {
        return num != rhs.num || den != rhs.den;
    }

    bool operator<(const Rational &rhs)  const;
    bool operator<=(const Rational &rhs) const {
        return operator==(rhs) || operator<(rhs);
    }
    bool operator>(const Rational &rhs)  const { return ! operator<=(rhs); }
    bool operator>=(const Rational &rhs) const { return ! operator<(rhs); }

    // conversion
    float  to_float()  const { return static_cast<float>(num)/den; }
    double to_double() const { return static_cast<double>(num)/den; }
    long long    to_int()    const { return num / den; }
};

inline std::ostream & operator<<(std::ostream &os, const Rational &q) {

    if (q.get_denom() == 1 )
        os << q.get_num();
    else
        os << "(/ " << q.get_num() << " " << q.get_denom() << ")";

    return os;
}

inline void Rational::normalize() {
    // avoid invalid number
    if ( den == 0 ) throw std::domain_error("0 denominator");

    // normalize 0
    if ( num == 0 ) {
        den = 1;
        return;
    }

    // normalize gcd
    long long g = gcd(num,den);
    num /= g;
    den /= g;

    // normalize sign
    if ( den < 0 ) {
        num = -num;
        den = -den;
    }

    assert( num != 0 || den == 1 );
    assert( gcd(num,den) == 1 );
    assert( den > 0 );
}

inline Rational & Rational::operator/=(long long rhs) {
    // integrity check
    if ( rhs == 0 ) throw std::domain_error("0 denominator");

    // avoid useless division
    if ( num != 0 ) {
        // multiply and normalize gcd
        long long g = gcd(num,rhs);
        num   /= g ;
        den *= rhs/g;

        // normalize sign
        if ( den < 0 ) {
            num   = -num;
            den = -den;
        }
    }

    assert( num != 0 || den == 1 );
    assert( gcd(num,den) == 1 );
    assert( den > 0 );
    return *this;
}

inline Rational & Rational::operator*=(long long rhs) {
    long long g = gcd(den,rhs);
    num *= rhs/g;
    den /= g;

    assert( num != 0 || den == 1 );
    assert( gcd(num,den) == 1 );
    assert( den > 0 );
    return *this;
}

inline Rational & Rational::operator-=(long long rhs) {
    num -= rhs * den;

    assert( num != 0 || den == 1 );
    assert( gcd(num,den) == 1 );
    assert( den > 0 );
    return *this;
}

inline Rational & Rational::operator+=(long long rhs) {
    num += rhs * den;
    assert( num != 0 || den == 1 );
    assert( gcd(num,den) == 1 );
    assert( den > 0 );
    return *this;
}

inline Rational & Rational::operator+=(const Rational &rhs) {
    // avoid self modification problem
    long long r_num = rhs.num, r_den = rhs.den;

    // add
    long long g = gcd(den,r_den);
    num = num * (r_den/g) + r_num * (den/g);
    den = den * (r_den/g);

    // normalize
    long long g2 = gcd(num,den);
    num /= g2;
    den /= g2;

    assert( num != 0 || den == 1 );
    assert( gcd(num,den) == 1 );
    assert( den > 0 );
    return *this;
}

inline Rational & Rational::operator-=(const Rational &rhs) {
    // avoid self modification problem
    long long r_num = rhs.num, r_den = rhs.den;

    // subtract
    long long g = gcd(den,r_den);
    num = num * (r_den/g) - r_num * (den/g);
    den = den * (r_den/g);

    // normalize
    long long g2 = gcd(num,den);
    num /= g2;
    den /= g2;

    assert( num != 0 || den == 1 );
    assert( gcd(num,den) == 1 );
    assert( den > 0 );
    return *this;
}

inline Rational & Rational::operator*=(const Rational &rhs) {
    // avoid self modification problem
    long long r_num = rhs.num, r_den = rhs.den;

    // multiply
    long long g1 = gcd(num,r_den);
    long long g2 = gcd(r_num,den);

    num = (num/g1) * (r_num/g2);
    den = (den/g2) * (r_den/g1);

    assert( num != 0 || den == 1 );
    assert( gcd(num,den) == 1 );
    assert( den > 0 );
    return *this;
}

inline Rational & Rational::operator/=(const Rational &rhs) {
    // integrity check
    if ( rhs == 0 ) throw std::domain_error("0 denominator");

    // avoid useless division
    if ( num != 0 ) {

        // avoid self modification problem
        long long r_num = rhs.num, r_den = rhs.den;

        // multiply and normalize gcd
        long long g1 = gcd(num,r_num);
        long long g2 = gcd(den,r_den);
        num = (num/g1) * ( r_den/g2);
        den = (den/g2) * ( r_num/g1);

        // normalize sign
        if ( den < 0 ) {
            num = -num;
            den = -den;
        }
    }

    assert( num != 0 || den == 1 );
    assert( gcd(num,den) == 1 );
    assert( den > 0 );
    return *this;
}

inline bool Rational::operator<(const Rational &rhs) const {
    // use mixed fraction for avoiding overflow.  e.g. 7/3 ~> 2+(1/3)
    long long q1 = num/den, r1 = num % den;
    long long q2 = rhs.num/rhs.den, r2 = rhs.num % rhs.den;
    
    // normalize negative number
    if ( r1 < 0 ) {
        r1 += den;
        q1--;
    }
    if ( r2 < 0 ) {
        r2 += rhs.den;
        q2--;
    }

    long long tmp1_n = num, tmp1_d = den, tmp2_n = rhs.num, tmp2_d = rhs.den;

    // required for swap '<' and '>' 
    bool reverse = false;
    while(true) {
        // if the quotient are different, use them to evaluate the comparison
        if ( q1 != q2 ) return reverse ? q1 > q2 : q1 < q2;

        // if a reminder is zero we can exit from the loop
        if ( r1 == 0 || r2 == 0 ) break;

        // otherwise we  move the comparison to the reminder
        // reverse numerator and denominator
        reverse = !reverse;
        tmp1_n = tmp1_d; tmp1_d = r1;
        tmp2_n = tmp2_d; tmp2_d = r2;

        // evaluate the new quotient and reminder
        q1 = tmp1_n / tmp1_d; r1 = tmp1_n % tmp1_d;
        q2 = tmp2_n / tmp2_d; r2 = tmp2_n % tmp2_d;
    }

    if ( r1 == r2 ) // both are zero so they are the same number!
        return false;

    // only one are zero, the zero is < to any quantities > 0
    return reverse ? r2 == 0 : r1 == 0;
}

inline bool Rational::operator<(long long rhs) const {
    // use mixed fraction for avoiding overflow.  e.g. 7/3 ~> 2+(1/3)
    long long quotient = num/den, reminder = num % den;
    // normalize negative number e.g 0 - (1/3) ~> -1 + (2/3)
    if ( reminder < 0 ) { quotient--; }

    return quotient < rhs;
}

// hash function
namespace std {
    template <> struct hash<Rational> {
        size_t operator()(const Rational &x) const { return x.get_hash(); }
    };
}

#endif // EOS_CORE_RATIONAL_HH
