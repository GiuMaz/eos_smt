/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef EOS_BOOLEAN_BOOLEAN_MODULE_HH
#define EOS_BOOLEAN_BOOLEAN_MODULE_HH

#include "clause.h"
#include "sort.h"
#include "term.h"
#include "literal.h"
#include "variable.h"
#include "theory_module.h"
#include <cassert>
#include <vector>
#include <string>
#include <unordered_set>

class BooleanModule : public TheoryModule {
    using ClausePtr = Clause*;
    using WatchMap = std::vector<std::vector<ClausePtr> >;

public:

    BooleanModule(SMTSolver &solver);
    ~BooleanModule() = default;

    void new_term_notify( Term t ) override;

    void propagate() override;

    void push() override;

    void pop() override;

    void make_decision(Variable) override;

    void get_conflict(std::vector<Variable>&) override;

    void get_justification(Variable, std::vector<Variable> &) override;

    void new_lemma(const std::vector<Term> &) override;

private:

    // Add a new clause to the problem. The clause is a list of literal.
    // a clause can be a learned clasue (from a lemma). otherwise is a clause
    // from the original problem
    bool add_clause(std::vector<Literal>& c, bool learnt);
    // build a clause
    bool new_clause(std::vector<Literal> & lits, bool learnt, ClausePtr &c_ref);

    // report a conflict on clause c
    void report_conflict(ClausePtr c);

    // decay the activity of clause.
    void clause_activity_decay();

    // remove clause from the system
    void remove_clause(ClausePtr c);
    void remove_from_vect( std::vector<ClausePtr> &v, ClausePtr c );

    // pretty printing
    std::string pprint_clause(const Clause &);

    // Tseitin transform
    Literal convert_to_cnf(Term t, bool learnt);
    void convert_or_to_cnf(Term t, bool learnt);
    void convert_xor_to_cnf(Term t, bool learnt);
    void convert_iff_to_cnf(Term t, bool learnt);
    void convert_ite_to_cnf(Term t, bool learnt);

    Literal get_literal(Variable var);

    literal_value_t get_assigned_value(const Literal &l) const;

    // assign a literal l with antecedent c (nullptr for decided)
    bool assign(Literal l, ClausePtr c);

    // get assignment level from trail
    size_t decision_level( Literal l );

    std::unordered_set<Term> converted_cache;

    // clauses
    std::vector<ClausePtr> clauses;
    std::vector<ClausePtr> learned;

    // watch list, used for propagation
    WatchMap watch_list;

    // store the clause in conflict, nullptr if there are no known conclict
    ClausePtr conflict_clause = nullptr;

    // keep track of position in trail
    size_t propagation_starting_pos = 0;

    // propagation justification
    std::vector<ClausePtr> antecedents;

    // chached values
    std::vector<ClausePtr> propagation_to_move;

    // parameter (TODO: move outside)
    double clause_activity_update = 1.0;
    double clause_decay_factor = 1.0 / 0.999;

    // propagated literal that must be undone during bactracking
    std::vector<Literal> propagated_lits;

    // backjump stacks
    std::vector<size_t> propagation_pos_stack;
    std::vector<size_t> propagated_lits_stack;
};

#endif // EOS_BOOLEAN_BOOLEAN_MODULE_HH
