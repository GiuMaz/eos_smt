/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef EOS_BOOLEAN_LITERAL_HH
#define EOS_BOOLEAN_LITERAL_HH

#include <string>
#include <set>
#include <vector>
#include <iostream>
#include <climits>

#include "variable.h"

/**
 * Literal class. Used to identify boolean variables inside the boolean module
 * It is organized in a way similar to Term, with the lsb used to identify the
 * polarity of the literal. A literal is 'signed' if its polarity is negative.
 *
 * Literal are related to a Variable, it have the same index but it also
 * store the polarity.
 * 
 * Note that full_index() is used when there is a distinction between positive
 * and negative literal (for example inside the watch list) while index() is
 * used when the info are related only to the value of the literal, ignoring
 * it's polarity (for example the antecedent for its assigment).
 */
class Literal {
    static const uint32_t ONE_MASK = 0x00000001;
    uint32_t value = UINT32_MAX; // default value, used for NULL_LIT
public:
    // constructor & destructor
    Literal() = default; 
    Literal(const Literal &) = default;
    Literal(Literal &&) = default;
    explicit Literal(uint32_t v) noexcept : value(v<<1) {}
    Literal(uint32_t v, bool s)  noexcept : value( (v<<1) | (s ? 1:0)) {}

    ~Literal() = default;

    // assignment and move
    Literal &operator=(const Literal &) = default;
    Literal &operator=(Literal &&) = default;

    // getter
    uint32_t index()      const { return value >> 1; }
    size_t   full_index() const { return static_cast<size_t>(value); }
    size_t   hash()       const { return static_cast<size_t>(value); }

    // build the Variable related to the literal.
    Variable get_variable() const { return Variable(index()); }

    // comparison operator
    bool operator==(const Literal &rhs) const { return value == rhs.value; }
    bool operator!=(const Literal &rhs) const { return value != rhs.value; }

    // true if the literal is positive, false if the literal is negated (signed)
    bool polarity()    const { return static_cast<bool>(~value & ONE_MASK); }
    // check polarity
    bool is_negative() const { return static_cast<bool>( value & ONE_MASK); }
    bool is_positive() const { return static_cast<bool>(~value & ONE_MASK); }

    // reversed polarity
    Literal operator! () const {
        Literal l; l.value = value ^ ONE_MASK; return l; }
};

/**
 * Possible value assigned to a literal
 */
enum literal_value_t {
    LIT_FALSE = 0,
    LIT_UNASSIGNED = 1,
    LIT_TRUE = 2
};

static const Literal NULL_LIT; // undefined literal

// usefull print method
inline std::ostream& operator<<(std::ostream &os, Literal const &l) {
    if ( l == NULL_LIT )
        os << "L_NULL";
    else
        os << (l.is_negative() ? "-L_" : "L_") << std::to_string(l.index());
    return os;
}

#endif // EOS_BOOLEAN_LITERAL_HH
