/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_BOOLEAN_CLAUSE_HH
#define EOS_BOOLEAN_CLAUSE_HH

#include <algorithm>
#include <assert.h>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include "literal.h"

/**
 * This class rappresent a clause of at least two literals.
 * the clause is intended to be bound to a specific instance of a module.
 *
 * A clause can be learned if it's added as a lemma during the searching
 * process. A learned clause had an activity value used for the evaluation
 * of the quality of the clause. It is possible to remove low activity clauses
 * to speed up the searching process.
 */
class Clause final {
using Iterator = Literal *;
using ConstIterator = const Literal *;

private:
    bool learned  :  1;
    uint64_t _size : 31;

    // not to be used directly, but only with allocate function
    Clause(bool l,const std::vector<Literal> &lits) :
        learned(l), _size(lits.size()) {
            std::copy(lits.begin(),lits.end(),this->begin());
            if ( learned ) get_activity() = 1.0;
        }

public:
    Clause(const Clause &) = delete;

    static Clause* allocate(const std::vector<Literal> &lits,
            bool learnt = false) {

        auto size = sizeof(Clause) + sizeof(Literal)*lits.size();
        if ( learnt ) size += sizeof(double);
        void* memory =  malloc(size);
        assert( memory != nullptr);
        return new (memory) Clause(learnt,lits);
    }

    static void deallocate(Clause* &c) {
        assert( c != nullptr);
        c->~Clause();
        free((void*)c);
        c = nullptr;
    }
    
    uint64_t size() const { return _size; }
    bool is_learned() const { return learned; }

    double &get_activity() {
        assert(is_learned());
        return *reinterpret_cast<double*>(end());
    }
    const double &get_activity() const {
        assert(is_learned());
        return *reinterpret_cast<const double*>(end());
    }

    Literal* get_data() {
        return reinterpret_cast<Literal*>(this+1);
    }
    const Literal* get_data() const {
        return reinterpret_cast<const Literal*>(this+1);
    }

    Iterator begin() { return get_data(); }
    Iterator end()   { return get_data() + _size; }
    ConstIterator begin() const { return get_data(); }
    ConstIterator end()   const { return get_data() + _size; }
    Literal& at(size_t i) { return get_data()[i]; }
    const Literal& at(size_t i) const { return get_data()[i]; }
    Literal& operator [] (size_t i) { return get_data()[i]; }
    const Literal& operator [] (size_t i) const { return get_data()[i]; }

    void update_activity(double value) { get_activity()+=value; }
    void renormalize_activity(double value) { get_activity()/=value; }

    // this metod can be used to reduce the size of the literal
    void shrink( size_t new_size) {
        assert( new_size <= _size);
        _size = new_size;
    }
};

inline std::ostream &operator<<( std::ostream &os,const Clause &c) {
    os << "clause [ ";
    for ( const auto &l : c )
        os << l << " ";
    os << "]";
    if ( c.is_learned() )
        os << " learned, activity: " << c.get_activity();
    return os;
}

#endif // EOS_BOOLEAN_CLAUSE_HH
