/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iomanip>
#include <iostream>
#include <set>
#include <vector>

#include "boolean_module.h"
#include "sort.h"
#include "term.h"
#include "literal.h"
#include "SMTSolver.h"
#include "printing.h"

BooleanModule::BooleanModule(SMTSolver &s):
    TheoryModule(s)
{
    ModuleId::set_name(get_id(),"BOOL");

    // handle all boolean term
    s.notify_sort_ownership(get_id(), BOOL_SORT);

    // handle specific term kind
    s.notify_term_ownership(get_id(), TERM_OR  );
    s.notify_term_ownership(get_id(), TERM_XOR );
    s.notify_term_ownership(get_id(), TERM_ITE );
    s.notify_term_ownership(get_id(), TERM_EQ  );

    // make decision on boolean variables
    s.notify_sort_decision(get_id(), BOOL_SORT );
}

literal_value_t BooleanModule::get_assigned_value(const Literal &l) const {
    Variable var = l.get_variable();
    if ( ! trail.has_value(var))
        return LIT_UNASSIGNED;
    else
        return trail.get_boolean_value(var) == l.polarity() ? LIT_TRUE : LIT_FALSE;
}

bool BooleanModule::add_clause(std::vector<Literal>& lits, bool learnt) {
    // build the new clause
    ClausePtr clause;
    bool conflict = new_clause(lits, learnt, clause);

    // if the clause is a conflict, return immediatly
    if ( conflict ) {
        trail.set_inconsistent(true);
        return true; // conflict
    }

    // clause is nullptr if the new clause is a unit
    if ( clause != nullptr ) {
        if ( learnt ) {
            // save in learned
            learned.push_back(clause);
        }
        else {
            // save in the original clause set
            clauses.push_back(clause);
        }
    }

    return false; // no conflict
}

bool BooleanModule::new_clause(std::vector<Literal> &c,
        bool learnt, ClausePtr &c_ref) {

    c_ref = nullptr;

    if ( ! learnt ) { // simplify if possible, learned clause doesn't need this
        size_t j = 0;
        for ( size_t i = 0; i < c.size(); i++ ) {
            // discard false literal
            if ( get_assigned_value( c[i] ) == LIT_FALSE ) continue;
            // already satisfied?
            if ( get_assigned_value( c[i] ) == LIT_TRUE  )
                return false; // no conflict, new clause is a nullptr
            // look for tautology or repetition
            bool add = true;
            for ( size_t k = i+1; k < c.size(); ++k ) {
                if ( c[k] == c[i] ) { // repetition?
                    add = false;      // don't add the repeated literal
                    break;
                }
                if ( c[k] == !(c[i]) ) // tautology?
                    return false; // no conflict, new clause is a nullptr 
            }
            if ( add ) c[j++] = c[i];
        }
        c.resize(j);
    }

    // an empty clause is a conflict
    if (c.empty()) return true; // conflict, new clause is a nullptr

    // a clause with 1 literal is a unit, simply assign it
    if ( c.size() == 1 ) {
        // true if the assignment is in conflict, false otherwise
        // the new clause is a nullptr (don't build clause of one literal)
        assert( trail.get_current_level() == 0 );
        return assign( c[0], nullptr );
    }

    // build the clause
    c_ref = Clause::allocate(c,learnt);

    if ( learnt ) { 

        // increase activity
        c_ref->update_activity( clause_activity_update );

        // find the first unasigned literal (there are at list one of them!)
        for ( auto it = c_ref->begin(); it != c_ref->end(); ++it) {
            Variable var = it->get_variable();
            if ( ! trail.has_value(var) ) {
                Literal tmp = *it;
                *it = c_ref->at(0);
                c_ref->at(0) = tmp;
                break;
            }
        }
        assert( ! trail.has_value(c_ref->at(0).get_variable()) );

        // pick a correct second literal to watch
        auto second = c_ref->begin()+1;
        Variable second_var = second->get_variable();

        if ( trail.has_value(second_var) ) {

            for ( auto it = c_ref->begin()+2; it != c_ref->end(); ++it) {
                Variable var_it = it->get_variable();
                // search for a second unasigned vars
                if ( ! trail.has_value(var_it) ) {
                    second = it;
                    break;
                }
                // otherwise save the highest level (for 1UP)
                if ( decision_level(*it) > decision_level(*second) )
                    second = it;
            }

            // swap
            Literal tmp = *second;
            *second = c_ref->at(1);
            c_ref->at(1) = tmp;
        }

        // is a unit?
        if (trail.has_value(c_ref->at(1).get_variable())) {
            assign( c_ref->at(0), c_ref );
        }
    }

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "BOOL: added new clause " << pprint_clause(*c_ref) << std::endl;
    }

    //  add to the watch list
    watch_list[c_ref->at(0).full_index()].push_back(c_ref);
    watch_list[c_ref->at(1).full_index()].push_back(c_ref);

    return false; // no conflict
}

size_t BooleanModule::decision_level( Literal l ) {
    Variable v(l.index());
    return trail.get_level(v);
}

bool BooleanModule::assign(Literal l, ClausePtr antecedent) {
    // already assigned ?
    if ( get_assigned_value(l) == LIT_TRUE )
        return false; // already assigned, no conflict
    if ( get_assigned_value(l) == LIT_FALSE )
        return true; // conflict!

    // assign in the trail
    trail.add_propagation( Variable(l.index()), l.is_positive(),
            get_id(), trail.get_current_level());

    // remember for backjump
    propagated_lits.push_back(l);

    // set antecedent
    antecedents[l.index()] = antecedent;

    return false; // no conflict found
}

void BooleanModule::new_term_notify( Term t ) {
    assert( term_table.sort_of(t) == bool_sort );

    // Convert to CNF all the subterm of t.
    // don't add the resulting literal like in a standard tseitin transform,
    // this is done by the main solver (we don't know the original polarity of
    // the term t)
    convert_to_cnf(t, false);
}

void BooleanModule::propagate() {

    while ( propagation_starting_pos < trail.size() ) {
        // extract the list of the opposite literal 
        // (they are false now, their watcher must be moved)
        propagation_to_move.clear();
        Variable var = trail.at(propagation_starting_pos++);

        // handle only boolean variables
        if ( ! variable_table.is_boolean(var) ) continue;

        assert( trail.has_value(var) );
        Literal  failed = get_literal(var);
        if ( trail.get_boolean_value(var) == true )
            failed = !failed;

        swap(propagation_to_move, watch_list[failed.full_index()]);

        if ( MESSAGE_LEVEL( STANDARD_MESSAGE ) ) {
            debug << "BOOL: propagate variable: " << variable_table.pprint(var) <<
                " value: " << trail.get_value(var) <<
                " ~> failed lit: " << failed << std::endl;

            debug << "BOOL: search new watch in clause:" << std::endl;
            for ( const auto &c : propagation_to_move)
                debug << pprint_clause(*c) << std::endl;

        }

        for (auto it = propagation_to_move.begin();
                it != propagation_to_move.end(); ++it) {

            // propagate effect on a clause
            Clause &c = **it; // usefull reference
            assert(c[0] == failed || c[1] == failed);

            // make sure the false literal is in position 1
            if ( c[0] == failed ) { c[0] = c[1]; c[1] = failed; }

            // if the clause is already solved, nothing need to be moved
            if (get_assigned_value(c[0]) == LIT_TRUE) {
                // reinsert inside the previous watch list
                watch_list[failed.full_index()].push_back(*it);
                continue; // move to the next
            }

            // search a new literal to watch
            bool foundt_new_watch = false;
            for ( size_t pos = 2; pos != c.size(); ++pos) {
                if ( get_assigned_value(c[pos]) != LIT_FALSE ) {
                    // swap value
                    c[1] = c[pos]; c[pos] = failed;
                    // insert in the new watch list
                    watch_list[c[1].full_index()].push_back(*it);
                    // move to the next
                    foundt_new_watch = true;
                    break;
                }
            }
            if ( foundt_new_watch ) continue; // move to the next

            // no new literal to watch, reinsert in the old position
            watch_list[failed.full_index()].push_back(*it);

            // the clause must be a conflict or a unit, try to assign the value
            bool conflict = assign(c[0],*it);

            if ( ! conflict ) continue; // no problem, move to the next

            if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                debug << "BOOL: find conflict in clause " <<
                    pprint_clause(**it) << std::endl;
            }

            // conflict found in propagation 
            report_conflict(*it);

            // reset the other literal to move (it is already set by propagate)
            copy(++it, propagation_to_move.end(),
                    back_inserter(watch_list[failed.full_index()]));

            return; // CONFLICT
        }
    }
}

void BooleanModule::report_conflict(ClausePtr c) {
    if ( c->is_learned() )
        c->update_activity(clause_activity_update);
    conflict_clause = c;
    trail.set_inconsistent(true);
    main_solver.report_conflict(get_id());

    clause_activity_decay();
}

void BooleanModule::clause_activity_decay() {

    // if big value is reached, a normalization is required
    if ( clause_activity_update > 1e100 ) {
        for ( auto & c : learned )
            c->renormalize_activity(clause_activity_update);
        clause_activity_update = 1.0;
    }

    clause_activity_update *= clause_decay_factor;
}

void BooleanModule::remove_from_vect(std::vector<ClausePtr> &v, ClausePtr c ) {
    for ( auto &i : v ) {
        if ( i == c ) {
            i = v.back();
            v.pop_back();
            return;
        }
    }
    assert(false);
}

void BooleanModule::remove_clause( ClausePtr c ) {
    remove_from_vect( watch_list[c->at(0).full_index()], c );
    remove_from_vect( watch_list[c->at(1).full_index()], c );
    Clause::deallocate( c );
}

Literal BooleanModule::get_literal(Variable var) {
    // allocate space if necessary
    assert( (antecedents.size()*2) == watch_list.size() );
    if ( antecedents.size() <= var.index() ) {
        antecedents.resize( var.index() + 1, nullptr );
        watch_list.resize( (var.index()+1) * 2);
    }
    return Literal(var.index());
}

void BooleanModule::push() {
    // save information on current level
    propagation_pos_stack.push_back(propagation_starting_pos);
    propagated_lits_stack.push_back(propagated_lits.size());
}

void BooleanModule::pop() {
    // retrieve information on current level
    assert( ! propagation_pos_stack.empty());
    propagation_starting_pos = propagation_pos_stack.back();
    propagation_pos_stack.pop_back();

    assert(! propagated_lits_stack.empty());
    size_t old_propagated_size = propagated_lits_stack.back();
    propagated_lits_stack.pop_back();

    // undo antecedents for propagated lits
    while ( propagated_lits.size() > old_propagated_size ) {
        assert(!propagated_lits.empty());
        antecedents[propagated_lits.back().index()] = nullptr;
        propagated_lits.pop_back();
    }
}

void BooleanModule::make_decision(Variable var) {
    assert( ! trail.has_value(var) );

    if ( trail.has_cached_value(var) ) {
        // try to use the cached values
        trail.add_decision(var, trail.get_cached_value(var), get_id());
    }
    else {
        // assign to false
        trail.add_decision(var, false_value, get_id());
    }
}

void BooleanModule::get_conflict(std::vector<Variable>& reason) {
    assert( conflict_clause != nullptr );
    assert( reason.empty() );

    // add all the variable of the failed clause
    for ( const auto &l : *conflict_clause ) {
        assert( trail.has_value(l.get_variable()) );
        reason.push_back(l.get_variable());
    }
}

void BooleanModule::get_justification(Variable assignment,
        std::vector<Variable> &justification) {

    if (MESSAGE_LEVEL(VERBOUSE_MESSAGE)) {
        debug << "BOOL: explain propagation of " <<
            variable_table.pprint(assignment) << std::endl;
    }

    // get the antecedent clause
    Literal var_lit = get_literal(assignment);
    Clause &clause = *antecedents[var_lit.index()];
    assert(clause[0].get_variable() == assignment);

    for ( const auto &l : clause ) {
        Variable v = l.get_variable();

        // ignore the implied variable
        if ( v == assignment ) continue;

        // add the other variable to the justification
        assert(trail.has_value(v));
        justification.push_back(v);
    }

    if ( clause.is_learned() )
        clause.update_activity(clause_activity_update);
}

void BooleanModule::new_lemma(const std::vector<Term> & lemma) {
    // convert lemma to cnf
    std::vector<Literal> lemma_lits;
    for (const auto &t: lemma) {
        // convert to cnf every term, remember the created clauses 
        lemma_lits.push_back( convert_to_cnf(t, true) );
    }

    // learn lemma
    add_clause(lemma_lits, true);
}

std::string BooleanModule::pprint_clause(const Clause &c) {
    std::string ret = "[ ";
    for ( const auto &l : c ) {
        Term t = variable_table.get_term(l.get_variable());
        if ( l.is_negative() )
            t = t.opposite();
        ret += term_table.pprint(t) + " ";
    }
    ret += "]";
    return ret;
}



//  CONVERSION TO CNF
// -------------------

/*
 * convert the given term to a CNF formula using Tseitin transformation.
 * This is the main function that call a specific function for every kind
 * of boolean term.
 * The function return the literal that identify the formula.
 */
Literal BooleanModule::convert_to_cnf(Term t, bool learnt) {

    assert( term_table.sort_of(t) == bool_sort );

    bool is_negative = t.is_negative();
    t = t.unsign();

    Variable var = variable_table.get_variable(t);
    Literal t_lit = get_literal(var);
    if ( is_negative ) t_lit = ! t_lit;

    if ( converted_cache.find(t) != converted_cache.end() )
        return t_lit;


    if ( MESSAGE_LEVEL(VERBOUSE_MESSAGE) ) {
        debug << "BOOL: convert term: " << t << " to cnf" << std::endl;
    }

    switch ( term_table.kind_of(t) ) {

        case TERM_OR:
            convert_or_to_cnf(t, learnt);
            break;

        case TERM_XOR:
            convert_xor_to_cnf(t, learnt);
            break;

        case TERM_EQ: {
            // if is an equality between boolean it's a iff, otherwise it's
            // only a literal
            Sort s = term_table.sort_of(term_table.get_composite_args(t)[0]);
            if ( s == bool_sort )
                convert_iff_to_cnf(t, learnt);
            }
            break;

        case TERM_ITE:
            convert_ite_to_cnf(t, learnt);
            break;

        default:
            // simple variable, nothing to convert
            break;
    }

    if ( MESSAGE_LEVEL(VERBOUSE_MESSAGE) ) {
        debug << "BOOL: converted term: " << t << " to lit " << t_lit << std::endl;
    }

    converted_cache.insert(t);
    return t_lit;
}

/*
 * Tseitin transformation of OR clause
 *
 * create a new literal l that identify the OR, and turn the relation if CNF
 * ( t1 v ... v tn ) <=> l
 * that in CNF is
 * ( ~l v t1 v ... v tn ) ^ ( l v ~t1 ) ^ ... ^ ( l v ~tn )
 */
void BooleanModule::convert_or_to_cnf(Term t, bool learnt) {
    assert( term_table.kind_of(t) == TERM_OR );

    const auto &composite = term_table.get_composite_args(t);
    Variable var = variable_table.get_variable(t);

    std::vector<Literal> new_clause;

    new_clause.push_back( ! get_literal(var) );
    for ( const auto &t : composite )
        new_clause.push_back(convert_to_cnf(t, learnt));

    add_clause(new_clause, learnt);

    new_clause.resize(2);
    new_clause[0] = get_literal(var);
    for ( const auto &l : composite ) {
        new_clause[1] = !convert_to_cnf(l, learnt);
        add_clause(new_clause, learnt);
    }
}

/*
 * Tseitin transformation of BINARY XOR clause
 *
 * create a new literal l that identify the XOR, and turn the relation if CNF
 * ( a xor b ) <=> l
 * that in CNF is
 * ( ~l v a v b ) ^ ( ~l v ~a v ~b ) ^ ( l v a v ~b ) ^ ( l v ~a v b )
 */
void BooleanModule::convert_xor_to_cnf(Term t, bool learnt) {

    assert( term_table.kind_of(t) == TERM_XOR );

    const auto &composite = term_table.get_composite_args(t);
    assert( composite.size() == 2 );

    std::vector<Literal> new_clause(3);

    // new lit
    Variable new_var = variable_table.get_variable(t);
    Literal l = get_literal(new_var);
    // xor component
    Literal a = convert_to_cnf( composite[0], learnt );
    Literal b = convert_to_cnf( composite[1], learnt );

    new_clause[0] = !l;
    new_clause[1] =  a;
    new_clause[2] =  b;
    add_clause(new_clause, learnt);

    new_clause[0] = !l;
    new_clause[1] = !a;
    new_clause[2] = !b;
    add_clause(new_clause, learnt);

    new_clause[0] =  l;
    new_clause[1] =  a;
    new_clause[2] = !b;
    add_clause(new_clause, learnt);

    new_clause[0] =  l;
    new_clause[1] = !a;
    new_clause[2] =  b;
    add_clause(new_clause, learnt);
}

/*
 * Tseitin transformation of BINARY IFF clause
 *
 * create a new literal l that identify the IFF, and turn the relation if CNF
 * ( a <=> b ) <=> l
 * that in CNF is
 * ( ~l v a v ~b ) ^ ( ~l v ~a v b ) ^ ( l v ~a v ~b ) ^ ( l v a v b )
 */
void BooleanModule::convert_iff_to_cnf(Term t, bool learnt) {

    assert( term_table.kind_of(t) == TERM_EQ );

    const auto &composite = term_table.get_composite_args(t);
    assert( composite.size() == 2 );

    std::vector<Literal> new_clause(3);

    // new lit
    Variable new_var = variable_table.get_variable(t);
    Literal l = get_literal(new_var);
    // equality component
    Literal a = convert_to_cnf( composite[0], learnt );
    Literal b = convert_to_cnf( composite[1], learnt );

    new_clause[0] = !l;
    new_clause[1] =  a;
    new_clause[2] = !b;
    add_clause(new_clause, learnt);

    new_clause[0] = !l;
    new_clause[1] = !a;
    new_clause[2] =  b;
    add_clause(new_clause, learnt);

    new_clause[0] =  l;
    new_clause[1] = !a;
    new_clause[2] = !b;
    add_clause(new_clause, learnt);

    new_clause[0] =  l;
    new_clause[1] =  a;
    new_clause[2] =  b;
    add_clause(new_clause, learnt);
}

/*
 * Tseitin transformation of if-then-else terms
 *
 * l <=> (ite c a b)
 * in CNF is
 * ( ~l v a v b ) ^ ( ~l v c v b ) ^ ( ~l v ~c v a ) ^ 
 * ( l v ~c v ~a ) ^ ( l v c v ~b )
 *
 */
void BooleanModule::convert_ite_to_cnf(Term t, bool learnt) {

    assert( term_table.kind_of(t) == TERM_ITE );

    const auto &composite = term_table.get_composite_args(t);
    assert( composite.size() == 3 );

    std::vector<Literal> new_clause(3);

    // new variable for the ite
    Variable new_var = variable_table.get_variable(t);
    Literal l = get_literal(new_var);

    // convert the component
    // if ( c ) then a else b
    Literal c = convert_to_cnf( composite[0], learnt );
    Literal a = convert_to_cnf( composite[1], learnt );
    Literal b = convert_to_cnf( composite[2], learnt );

    new_clause[0] = !l;
    new_clause[1] =  a;
    new_clause[2] =  b;
    add_clause(new_clause, learnt);

    new_clause[0] = !l;
    new_clause[1] =  c;
    new_clause[2] =  b;
    add_clause(new_clause, learnt);

    new_clause[0] = !l;
    new_clause[1] = !c;
    new_clause[2] =  a;
    add_clause(new_clause, learnt);

    new_clause[0] =  l;
    new_clause[1] = !c;
    new_clause[2] = !a;
    add_clause(new_clause, learnt);

    new_clause[0] =  l;
    new_clause[1] =  c;
    new_clause[2] = !b;
    add_clause(new_clause, learnt);
}

