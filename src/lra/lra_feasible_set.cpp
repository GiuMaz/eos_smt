/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lra_feasible_set.h"
#include <algorithm>
#include "printing.h"



//  TRAIL MANIPULATION
// ---------------------

uint32_t LRAFeasibleSet::last_equality_pos(Variable var) {
    auto it = last_equality.find(var);
    if ( it == last_equality.end() )
        return NO_POS;
    else
        return it->second;
}

uint32_t LRAFeasibleSet::last_lower_pos(Variable var) {
    auto it = last_lower.find(var);
    if ( it == last_lower.end() )
        return NO_POS;
    else
        return it->second;
}

uint32_t LRAFeasibleSet::last_upper_pos(Variable var) {
    auto it = last_upper.find(var);
    if ( it == last_upper.end() )
        return NO_POS;
    else
        return it->second;
}

// add a new equality_trail to an equality set
void LRAFeasibleSet::set_new_eq(
        Variable var, bool is_eq, Rational val, Variable reason) {

    // compute position
    uint32_t old_i = last_equality_pos(var);
    uint32_t new_i = equality_trail.size();

    // build equality 'var ~ val'
    equality_trail.push_back({var, is_eq, val, reason}); 

    // update position
    old_pos_eq.push_back(old_i);
    last_equality[var] = new_i;
}

void LRAFeasibleSet::set_new_upper(Variable var, bool is_strict,
        Rational val, Variable reason) {

    // compute position
    uint32_t old_i = last_upper_pos(var);
    uint32_t new_i = upper_trail.size();

    // build relation 'var < val'
    upper_trail.push_back({var, is_strict, val, reason}); 

    // update position
    old_pos_up.push_back(old_i);
    last_upper[var] = new_i;
}

void LRAFeasibleSet::set_new_lower(Variable var, bool is_strict,
        Rational val, Variable reason) {

    // compute position
    uint32_t old_i = last_lower_pos(var);
    uint32_t new_i = lower_trail.size();

    // build relation 'val <= var'
    lower_trail.push_back({var, is_strict, val, reason}); 

    // update position
    old_pos_low.push_back(old_i);
    last_lower[var] = new_i;
}



//  FEASIBLE SET
// --------------

// a feasible set use the trail to identify the current assignments
LRAFeasibleSet::LRAFeasibleSet(Trail &t, TermTable &tt, VariableTable &vt,
        const ModuleId &m):
    main_trail(t),
    term_table(tt),
    variable_table(vt),
    id(m)
{}

// set disequality between a variable and a given value.
// it return true iff it found a conflict, otherwise it return false.
// A conflict can happen if a variable is set to be equal and disequal
// to the same value.
bool LRAFeasibleSet::set_disequality(Variable var, Rational val, Variable reason) {

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "LRA: set " << variable_table.pprint(var) << " != " << val <<
            " because " << variable_table.pprint(reason) << std::endl;
    }

    // check for a justified equality or a redundant disequality
    auto pos = last_equality_pos(var);
    while ( pos != NO_POS ) {
        // get the last relation for the variable
        const auto &eq = equality_trail[pos];

        // if is an equality the value must be different from the value
        // of the disequality, otherwise a conflict arise.
        // if the value is different don't add anything, it is not required.
        // This also keep the equality in the first position
        if ( eq.is_eq ) {
            if ( eq.val == val ) {
                build_eq_conflict(var, eq.reason, reason);

                assert( !conflict_reason.empty() );
                return true; // conflict
            }
            else 
                return false; // no conflict
        }
        // check if we already known about the disequality
        else if ( eq.val == val )
            return false; // no conflict, disequality already set

        pos = old_pos_eq[pos];
    }

    // check if we can ignore the disequality because is outside the bound
    // check upper bound
    auto pos_up = last_upper_pos(var);
    if ( pos_up != NO_POS ) {
        // get the last relation for the variable
        const auto &up = upper_trail[pos_up];

        // check that the disequality is under the upper bound
        if ( val > up.val || ( val == up.val && up.is_strict ) )
            return false; // no conflict, useless disequality
    }

    // check lower bound
    auto pos_low = last_lower_pos(var);
    if ( pos_low != NO_POS ) {
        // get the last relation for the variable
        const auto &low = lower_trail[pos_low];

        // check that the disequality is over the lower bound
        if ( val < low.val || ( val == low.val && low.is_strict ) )
            return false; // no conflict, useless disequality
    }

    /*
    // check for unjustified equality
    if (main_trail.has_value(var)) {
        if ( main_trail.get_value(var).get_rational_value() == val ) {
            build_eq_conflict(var, eq.reason, reason);

            assert( !conflict_reason.empty() );
            return true; // conflict
        }
    }
    */

    // set the disequality
    set_new_eq(var,false,val,reason);

    return false; // no conflict
}

// set equality between a variable and a given value.
// it return true iff it found a conflict, otherwise it return false.
// A conflict can happen if a variable is set to be equal and disequal
// to the same value.
bool LRAFeasibleSet::set_equality( Variable var, Rational val, Variable reason ) {

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "LRA: set " << variable_table.pprint(var) << " == " << val <<
            " because " << variable_table.pprint(reason) << std::endl;
    }

    auto pos = last_equality_pos(var);

    // check that direct equality and disequality are consistent
    while ( pos != NO_POS ) {
        // get the last relation for the variable
        const auto &eq = equality_trail[pos];

        // if is an equality the value must be the same, otherwise a we are
        // in conflict.
        if ( eq.is_eq ) {
            if ( eq.val != val ) {
                // build the conflict from the incompatible info
                build_eq_conflict(var, reason, eq.reason);

                assert(!conflict_reason.empty());
                return true; // conflict
            }
            else 
                return false; // no conflict
        }
        // compare with disequality
        // if a disequality have the same value as the equality we are also
        // in conflict
        else if ( eq.val == val ) {
            // build the conflict from the incompatible info
            build_eq_conflict(var, reason, eq.reason);

            assert(!conflict_reason.empty());
            return true; // conflict
        }

        // move to the next
        pos = old_pos_eq[pos];
    }

    // check upper bound
    auto pos_up = last_upper_pos(var);
    if ( pos_up != NO_POS ) {

        // get the last relation for the variable
        const auto &up = upper_trail[pos_up];

        // check that the equality is under the upper bound
        if ( val > up.val || ( val == up.val && up.is_strict ) ) {
            // the upper bound and the new equality are in conflict now
            build_eq_and_ge_conflict(var, reason, up.reason,true);

            assert( !conflict_reason.empty() );
            return true; // conflict
        }
    }

    // check lower bound
    auto pos_low = last_lower_pos(var);
    if ( pos_low != NO_POS ) {

        // get the last relation for the variable
        const auto &low = lower_trail[pos_low];

        // check that the equality is over the lower bound
        if ( val < low.val || ( val == low.val && low.is_strict ) ) {
            // the lower bound and the equality are in conflict now
            build_eq_and_ge_conflict(var, reason, low.reason,false);

            assert( !conflict_reason.empty() );
            return true; // conflict
        }
    }

    // everything ok, set the equality
    set_new_eq(var,true,val,reason);

    return false; // no conflict
}

bool LRAFeasibleSet::set_upper_strict(Variable var, Rational val, Variable reason) {

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "LRA: set " << variable_table.pprint(var) << " < " << val <<
            " because " << variable_table.pprint(reason) << " -> " <<
            (main_trail.get_boolean_value(reason)?"true":"false") << std::endl;
    }

    assert(term_table.kind_of(variable_table.get_term(reason)) == TERM_ARITH_GE);
    assert(!main_trail.get_boolean_value(reason));

    // we already have an upper bound?
    auto up_pos = last_upper_pos(var);
    if ( up_pos != NO_POS ) {
        // get the last relation for the variable
        const auto &up = upper_trail[up_pos];

        // we have already a better upper bound?
        if ( up.val < val || (up.val == val && up.is_strict) )
            return false; // no conflict
    }

    // check for already set equality to be consistent
    auto eq_pos = last_equality_pos(var);
    if ( eq_pos != NO_POS ) {
        const auto &eq = equality_trail[eq_pos];

        // if there is an equality, is it outside of the new bound?
        if ( eq.is_eq ) {
            if ( eq.val >= val ) {
                // the equality and the new disequality are in conflict now
                build_eq_and_ge_conflict(var, eq.reason, reason,true);

                assert( !conflict_reason.empty() );
                return true; // conflict
            }
            else {
                // useless
                return false; // no conflict
            }
        }
    }

    // check that the lower bound is consistent
    auto low_pos = last_lower_pos(var);
    if ( low_pos != NO_POS ) {
        // get the last relation for the variable
        const auto &low = lower_trail[low_pos];

        if ( low.val >= val ) {
            // conflict! resolve with the Fourier Motzkin elimination
            build_fm_conflict(var,low.reason,low.is_strict,reason,true);

            assert( !conflict_reason.empty() );
            return true; // conflict
        }
    }

    // update the upper bound
    set_new_upper(var, true, val, reason);

    return false; // no conflict
}

bool LRAFeasibleSet::set_upper_eq(Variable var, Rational val, Variable reason) {

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "LRA: set " << variable_table.pprint(var) << " <= " << val <<
            " because " << variable_table.pprint(reason) << " -> " <<
            (main_trail.get_boolean_value(reason)?"true":"false") << std::endl;
    }

    assert(term_table.kind_of(variable_table.get_term(reason)) == TERM_ARITH_GE);
    assert(main_trail.get_boolean_value(reason));

    // we already have an upper bound?
    auto up_pos = last_upper_pos(var);
    if ( up_pos != NO_POS ) {
        // get the last relation for the variable
        const auto &up = upper_trail[up_pos];

        // we have already a better upper bound?
        if ( up.val <= val )
            return false; // no conflict
    }

    // check for already set equality to be consistent
    auto eq_pos = last_equality_pos(var);
    if ( eq_pos != NO_POS ) {
        const auto &eq = equality_trail[eq_pos];

        // if there is an equality, is it outside of the new bound?
        if ( eq.is_eq ) {
            if (eq.val > val ) {
                // the equality and the new disequality are in conflict now
                build_eq_and_ge_conflict(var, eq.reason, reason, true);

                assert( !conflict_reason.empty() );
                return true; // conflict
            }
            else {
                // the bound is not usefull, we already have an equality
                return false; // no conflict
            }
        }
    }

    // check that the lower bound is consistent
    auto low_pos = last_lower_pos(var);
    if ( low_pos != NO_POS ) {
        // get the last relation for the variable
        const auto &low = lower_trail[low_pos];

        if ( low.val >= val ) {
            // if the two bound are non-strict and equal we propagate an
            // equality, otherwise we have a conflict

            if ( low.val == val && !low.is_strict ) {
                // is var != val ?
                while ( eq_pos != NO_POS ) {
                    const auto &eq = equality_trail[eq_pos];
                    assert( !eq.is_eq );

                    // look for a disequality
                    if ( eq.val == val ) {
                        // a <= x <= a and x != a
                        build_disequality_conflict(
                                var, eq.reason, low.reason, reason);
                        assert( !conflict_reason.empty() );
                        return true; // conflict
                    }
                }
            }
            else {
                // the two bound are strictly inconsistent
                // conflict! resolve with the Fourier Motzkin elimination
                build_fm_conflict(var, low.reason, low.is_strict, reason,false);

                assert( !conflict_reason.empty() );
                return true; // conflict
            }
        }
    }

    // update the upper bound
    set_new_upper(var, false, val, reason);

    return false; // no conflict
}

bool LRAFeasibleSet::set_lower_strict(Variable var, Rational val, Variable reason) {
    assert(term_table.kind_of(variable_table.get_term(reason)) == TERM_ARITH_GE);
    assert(!main_trail.get_boolean_value(reason));

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "LRA: set " << val << " < " << variable_table.pprint(var) <<
            " because " << variable_table.pprint(reason) << " -> " <<
            (main_trail.get_boolean_value(reason)?"true":"false") << std::endl;
    }

    // we already have an lower bound?
    auto low_pos = last_lower_pos(var);
    if ( low_pos != NO_POS ) {
        // get the last relation for the variable
        const auto &low = lower_trail[low_pos];

        // we have already a better lower bound?
        if ( low.val > val || (low.val == val && low.is_strict) )
            return false; // no conflict
    }

    // check for already set equality to be consistent
    auto eq_pos = last_equality_pos(var);
    if ( eq_pos != NO_POS ) {
        const auto &eq = equality_trail[eq_pos];

        // if there is an equality, is it outside of the new bound?
        if ( eq.is_eq ) {
            if ( eq.val <= val ) {
                // the equality and the new disequality are in conflict now
                build_eq_and_ge_conflict(var, eq.reason, reason, false);

                assert( !conflict_reason.empty() );
                return true; // conflict
            }
            else {
                // useless
                return false; // no conflict
            }
        }
    }

    // check that the upper bound is consistent
    auto up_pos = last_upper_pos(var);
    if ( up_pos != NO_POS ) {
        // get the last relation for the variable
        const auto &up = upper_trail[up_pos];

        if ( up.val <= val ) {
            // conflict! resolve with the Fourier Motzkin elimination
            build_fm_conflict(var, reason, true, up.reason, up.is_strict);

            assert( !conflict_reason.empty() );
            return true; // conflict
        }
    }

    // update the lower bound
    set_new_lower(var, true, val, reason);

    return false; // no conflict
}

bool LRAFeasibleSet::set_lower_eq(Variable var, Rational val, Variable reason) {
    assert(term_table.kind_of(variable_table.get_term(reason)) == TERM_ARITH_GE);
    assert(main_trail.get_boolean_value(reason) == true);

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "LRA: set " << val << " <= " << variable_table.pprint(var) <<
            " because " << variable_table.pprint(reason) << " -> " <<
            (main_trail.get_boolean_value(reason)?"true":"false") << std::endl;
    }

    // we already have an lower bound?
    auto low_pos = last_lower_pos(var);
    if ( low_pos != NO_POS ) {
        // get the last relation for the variable
        const auto &low = lower_trail[low_pos];

        // we have already a better lower bound?
        if ( low.val >= val )
            return false; // no conflict
    }

    // check for already set equality to be consistent
    auto eq_pos = last_equality_pos(var);
    if ( eq_pos != NO_POS ) {
        const auto &eq = equality_trail[eq_pos];

        // if there is an equality, is it outside of the new bound?
        if ( eq.is_eq ) {
            if ( eq.val < val ) {
                // the equality and the new disequality are in conflict now
                build_eq_and_ge_conflict(var, eq.reason, reason, false);

                assert( !conflict_reason.empty() );
                return true; // conflict
            }
            else{
                // useless
                return false; // no conflict
            }
        }
    }

    // check that the upper bound is consistent
    auto up_pos = last_upper_pos(var);
    if ( up_pos != NO_POS ) {
        // get the last relation for the variable
        const auto &up = upper_trail[up_pos];

        if ( up.val <= val ) {
            if ( up.val == val && !up.is_strict ) {
                // is var != val ?
                while ( eq_pos != NO_POS ) {
                    const auto &eq = equality_trail[eq_pos];
                    assert( !eq.is_eq );

                    // look for a disequality
                    if ( eq.val == val ) {
                        // disequality conflict
                        build_disequality_conflict(
                                var, eq.reason, reason, up.reason);
                        assert( !conflict_reason.empty() );
                        return true; // conflict
                    }
                }
            }
            else {
                // the two bound are strictly inconsistent
                // conflict! resolve with the Fourier Motzkin elimination
                build_fm_conflict(var, reason, false, up.reason, up.is_strict);

                assert( !conflict_reason.empty() );
                return true; // conflict
            }
        }
    }

    // update the lower bound
    set_new_lower(var, false, val, reason);

    return false; // no conflict
}

// reason for equality
/*
Variable LRAFeasibleSet::get_equality_reason(Variable var) {
    auto pos = last_equality_pos(var);

    // iterate all the known relation about var until an equality is foundt
    while ( pos != NO_POS ) {
        const auto &eq = equality_trail[pos];
        if ( eq.is_eq )
            return eq.reason;
        pos = old_pos_eq[pos];
    }

    // no reason set
    return NULL_VAR;
}
*/

Rational LRAFeasibleSet::get_equality_value(Variable var) {
    auto pos = last_equality_pos(var);

    while ( pos != NO_POS ) {
        const auto &eq = equality_trail[pos];
        if ( eq.is_eq )
            return eq.val;
        pos = old_pos_eq[pos];
    }

    // dummy
    assert(false);
    return Rational(0);
}

// return a feasible value for the variable var
Rational LRAFeasibleSet::get_feasible_value( Variable var ) {

    // last equality set for the var
    auto pos_eq = last_equality_pos(var);

    // store all the != (if any)
    std::unordered_set<Rational> distincts;

    if ( pos_eq != NO_POS ) {
        const auto &eq = equality_trail[pos_eq];

        // if we already have an equality, return that
        if ( eq.is_eq )
            return equality_trail[pos_eq].val;

        // otherwise build the distinct set
        do {
            const auto &eq = equality_trail[pos_eq];
            assert(!eq.is_eq);

            distincts.insert(eq.val);

            pos_eq = old_pos_eq[pos_eq];
        } while ( pos_eq != NO_POS );
    }

    // try to use cached value
    if (main_trail.has_cached_value(var)) {
        Rational cached_val =
            main_trail.get_cached_value(var).get_rational_value();
        
        if ( is_value_in_bound(var,cached_val) &&
                distincts.find(cached_val) == distincts.end() )
            return cached_val;
    }

    // build a new value
    auto pos_low = last_lower_pos(var);
    bool has_lower = (pos_low != NO_POS);

    auto pos_up = last_upper_pos(var);
    bool has_upper = (pos_up != NO_POS);

    if ( !has_lower && !has_upper ) {
        // start from zero and increase
        Rational ret(0);
        while ( distincts.find(ret) != distincts.end() ) {
            ret += 1;
        }
        return ret;
    }
    else if ( has_lower && !has_upper ) {
        // build bound
        Rational lower_bound = lower_trail[pos_low].val;
        bool is_strict = lower_trail[pos_low].is_strict;

        // start at the lower bound and increase
        Rational ret = is_strict ? lower_bound+1 : lower_bound;
        while ( distincts.find(ret) != distincts.end() ) {
            ret += 1;
        }
        return ret;
    }
    else if ( !has_lower && has_upper ) {
        // build bound
        Rational upper_bound = upper_trail[pos_up].val;
        bool is_strict = upper_trail[pos_up].is_strict;

        // start under the upper bound and decrease
        Rational ret = is_strict ? upper_bound-1 : upper_bound;
        while ( distincts.find(ret) != distincts.end() ) {
            ret -= 1;
        }
        return ret;
    }
    else {
        // both bonds set
        Rational lower_bound = lower_trail[pos_low].val;
        Rational upper_bound = upper_trail[pos_up].val;

        Rational ret = (lower_bound + upper_bound)/2;
        while ( distincts.find(ret) != distincts.end() ) {
            ret = (ret + upper_bound) / 2;
        }
        return ret;
    }
}

bool LRAFeasibleSet::is_value_in_bound(Variable var, Rational val) {

    // check lower
    auto pos_low = last_lower_pos(var);
    if (pos_low != NO_POS) {
        Rational lower_bound = lower_trail[pos_low].val;
        bool is_strict = lower_trail[pos_low].is_strict;

        if ( val < lower_bound || ( val == lower_bound && is_strict ) )
            return false;
    }

    // check upper
    auto pos_up = last_upper_pos(var);
    if (pos_up != NO_POS) {
        Rational upper_bound = upper_trail[pos_up].val;
        bool is_strict = upper_trail[pos_up].is_strict;

        if ( val > upper_bound || ( val == upper_bound && is_strict ) )
            return false;
    }

    // everything ok!
    return true;
}

// backtracking support
void LRAFeasibleSet::push() {
    // equality
    assert(equality_trail.size() == old_pos_eq.size());
    trail_eq_stack.push_back(equality_trail.size());

    // lower
    assert(lower_trail.size() == old_pos_low.size());
    trail_low_stack.push_back(lower_trail.size());

    // upper
    assert(upper_trail.size() == old_pos_up.size());
    trail_up_stack.push_back(upper_trail.size());
}

void LRAFeasibleSet::pop() {
    size_t old_size;

    // restore equality
    old_size = trail_eq_stack.back(); trail_eq_stack.pop_back();
    while ( equality_trail.size() > old_size ) {
        // recompute position
        const auto &eq = equality_trail.back();
        last_equality[eq.var] = old_pos_eq.back();

        if (MESSAGE_LEVEL(VERBOUSE_MESSAGE)) {
            debug << "LRA: pop " << variable_table.pprint(eq.var) <<
                (eq.is_eq?" == ":" != ") << eq.val << std::endl;

        }

        // pop the value
        old_pos_eq.pop_back();
        equality_trail.pop_back();
    }

    // restore lower
    old_size = trail_low_stack.back(); trail_low_stack.pop_back();
    while ( lower_trail.size() > old_size ) {
        // recompute position
        const auto &l = lower_trail.back();
        last_lower[l.var] = old_pos_low.back();

        if (MESSAGE_LEVEL(VERBOUSE_MESSAGE)) {
            debug << "LRA: pop " << variable_table.pprint(l.var) <<
                (l.is_strict?" > ":" >= ") << l.val << std::endl;

        }
        // pop the value
        old_pos_low.pop_back();
        lower_trail.pop_back();
    }

    // restore upper
    old_size = trail_up_stack.back(); trail_up_stack.pop_back();
    while ( upper_trail.size() > old_size ) {
        // recompute position
        const auto &u = upper_trail.back();
        last_upper[u.var] = old_pos_up.back();

        if (MESSAGE_LEVEL(VERBOUSE_MESSAGE)) {
            debug << "LRA: pop " << variable_table.pprint(u.var) <<
                (u.is_strict?" < ":" <= ") << u.val << std::endl;

        }

        // pop the value
        old_pos_up.pop_back();
        upper_trail.pop_back();
    }
}



//  CONFLICT
// ----------

void LRAFeasibleSet::get_conflict_reason(std::vector<Variable> &reason) {
    reason.clear();
    std::swap(reason,conflict_reason);
}

PolynomialPtr LRAFeasibleSet::join_polynomial_in_var(
        const Polynomial &left, const Polynomial &right, Variable common) {

    std::vector<Monomial> new_pol;

    // find the normalization value
    Rational norm_left(0), norm_right(0);

    for ( const auto &m : left ) {
        Variable v = variable_table.get_variable(m.indet);
        if ( v != common ) continue;

        // positive
        norm_left = (Rational(1) / m.coeff);
        break;
    }
    for ( const auto &m : right ) {
        Variable v = variable_table.get_variable(m.indet);
        if ( v != common ) continue;

        // negative
        norm_right = -(Rational(1) / m.coeff);
        break;
    }
    // we must have found a norm for the common var
    assert(norm_left  != 0);
    assert(norm_right != 0);

    // build (norm_left * left) + (norm_right * right) == 0
    // we use an add because norm_right is already negated

    size_t i1, i2;
    for ( i1=0, i2=0; i1 < left.size() && i2 < right.size(); ) {

        if ( left[i1].indet == right[i2].indet ) {
            // if there are two instance of the same variable join their coeff
            Rational new_coeff =
                (left[i1].coeff * norm_left) + (right[i2].coeff * norm_right);

            // common should not be inside the newly created polynomial
            assert( variable_table.get_term(common) != left[i1].indet ||
                    new_coeff == 0);

            // insert only if the coefficent is non-zero
            if ( new_coeff != 0 )
                new_pol.push_back( { new_coeff , left[i1].indet } );

            // move both ahead
            ++i1;
            ++i2;
        }

        // keep the indeterminant ordered
        else if (left[i1].indet.index() < right[i2].indet.index()) {
            // insert and element from left
            new_pol.push_back({ left[i1].coeff * norm_left, left[i1].indet });
            ++i1;
        }
        else {
            // insert an element from right
            new_pol.push_back({ right[i2].coeff * norm_right, right[i2].indet });
            ++i2;
        }
    }

    // insert the remaining elements
    assert( i1 == left.size() || i2 == right.size() );
    for (; i1 < left.size(); ++i1)
        new_pol.push_back({ left[i1].coeff * norm_left, left[i1].indet });
    for (; i2 < right.size(); ++i2)
        new_pol.push_back({ right[i2].coeff * norm_right, right[i2].indet });

    // allocate the polynomial
    PolynomialPtr p = Polynomial::allocate(new_pol);

    return p;
}

// handles conflict of the kinds:
//   a <= var and var <=  b and a == c and b == c but var != c
void LRAFeasibleSet::build_disequality_conflict( Variable var, Variable diseq,
        Variable lower, Variable upper) {

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "LRA: a <= x, x <= b, x != a" << std::endl;
    }

    // get the polynomials
    Term eq_term = variable_table.get_term(diseq);
    Term low_term = variable_table.get_term(lower);
    Term up_term = variable_table.get_term(upper);

    const Polynomial &poly_eq = term_table.get_polynomial(eq_term);
    const Polynomial &poly_low = term_table.get_polynomial(low_term);
    const Polynomial &poly_up  = term_table.get_polynomial(up_term);

    // build lower >= distinct

    // create the new polynomial for the relation
    auto p1 = join_polynomial_in_var(poly_low, poly_eq, var);
    Term low_minus_eq = term_table.add_polynomial(p1);

    // if p is already inside the system, it can change!
    const Polynomial &new_poly_1 = term_table.get_polynomial(low_minus_eq);

    Term new_dis1 = term_table.add_binary_ge(low_minus_eq,zero_term);
    Variable new_var1 = variable_table.get_variable(new_dis1.unsign() );

    // if the new polynomial is not assigned assign it to true on the trial
    if ( ! main_trail.has_value( new_var1 ) ) {

        // find the correct level
        size_t level = 0;
        for ( const auto &m : new_poly_1 ) {
            if ( m.indet != NULL_TERM ) {
                Variable var = variable_table.get_variable(m.indet);
                size_t var_level = main_trail.get_level(var);
                if ( var_level > level )
                    level = var_level;
            }
        }

        // set the relation to false in the trail
        main_trail.add_propagation(new_var1, new_dis1.is_negative(), id, level);
    }

    
    // build distinct >= upper

    // create the new polynomial for the relation
    auto p2 = join_polynomial_in_var(poly_up, poly_eq, var);
    Term up_minus_eq = term_table.add_polynomial(p2);

    // if p is already inside the system, it can change!
    const Polynomial &new_poly_2 = term_table.get_polynomial(up_minus_eq);

    Term new_dis2 = term_table.add_binary_ge(up_minus_eq,zero_term);
    Variable new_var2 = variable_table.get_variable(new_dis2.unsign() );

    // if the new polynomial is not assigned assign it to true on the trial
    if ( ! main_trail.has_value( new_var2 ) ) {

        // find the correct level
        size_t level = 0;
        for ( const auto &m : new_poly_2 ) {
            if ( m.indet != NULL_TERM ) {
                Variable var = variable_table.get_variable(m.indet);
                size_t var_level = main_trail.get_level(var);
                if ( var_level > level )
                    level = var_level;
            }
        }

        // set the relation to false in the trail
        if ( new_dis2.is_positive() )
            main_trail.add_propagation(new_var2, false, id, level);
        else
            main_trail.add_propagation(new_var2, true, id, level);
    }

    // add to conflict
    conflict_reason.push_back(new_var1);
    conflict_reason.push_back(new_var2);
    conflict_reason.push_back(diseq);
    conflict_reason.push_back(lower);
    conflict_reason.push_back(upper);
}

// handles conflict of the kinds:
//  - var == a and var <  b but !(a <  b)
//  - var == a and var <= b but !(a <= b)
//  - var == a and var >  b but !(a >  b)
//  - var == a and var >= b but !(a >= b)
// Build and propagate the last relation with the Furier Motzkin rule and 
// build a conflict between this new formula and the current assignment.
void LRAFeasibleSet::build_eq_and_ge_conflict(
        Variable var, Variable equality, Variable greater_eq, bool is_upper) {

    // if is true in the trail is a >= or a <=, if is false is a < or a >
    bool is_strict = ! main_trail.get_boolean_value(greater_eq);

    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "LRA: conflict between equality and disequality" << std::endl;
    }

    // get the equality term
    assert( main_trail.has_value(equality) );
    Term var_eq = variable_table.get_term(equality);
    assert(term_table.kind_of(var_eq) == TERM_ARITH_EQ);

    assert( main_trail.has_value(greater_eq) );
    Term var_ge = variable_table.get_term(greater_eq);
    assert(term_table.kind_of(var_ge) == TERM_ARITH_GE);

    // get the polynomials
    const Polynomial &poly_a = term_table.get_polynomial(var_eq);
    const Polynomial &poly_b = term_table.get_polynomial(var_ge);

    PolynomialPtr p = join_polynomial_in_var(poly_b,poly_a,var);
    Term new_poly_term = term_table.add_polynomial(p);

    // if p is already inside the system, it can change!
    const Polynomial &new_poly = term_table.get_polynomial(new_poly_term);

    // build the new term
    Term new_term;
    if ( is_upper && is_strict ) {
        // x == a and x <  b => a <  b
        new_term = term_table.add_binary_lt(new_poly_term,zero_term);
    }
    else if ( is_upper && ! is_strict ) {
        // x == a and x <= b => a <= b
        new_term = term_table.add_binary_le(new_poly_term,zero_term);
    }
    else if ( ! is_upper && is_strict ) {
        // x == a and x >  b => a >  b
        new_term = term_table.add_binary_gt(new_poly_term,zero_term);
    }
    else {
        // x == a and x >= b => a >= b
        new_term = term_table.add_binary_ge(new_poly_term,zero_term);
    }

    // build the proper variable
    Variable new_var = variable_table.get_variable(new_term.unsign());

    // build the conflict
    conflict_reason.push_back( equality );
    conflict_reason.push_back( greater_eq );
    conflict_reason.push_back( new_var );

    // if the new polynomial is not assigned assign it to true on the trial
    if ( ! main_trail.has_value( new_var ) ) {

        // find the correct level
        size_t level = 0;
        for ( const auto &m : new_poly ) {
            if ( m.indet != NULL_TERM ) {
                Variable var = variable_table.get_variable(m.indet);
                size_t var_level = main_trail.get_level(var);
                if ( var_level > level )
                    level = var_level;
            }
        }

        // set the relation to false in the trail
        if ( new_term.is_positive() )
            main_trail.add_propagation(new_var, false, id,
                    level);
        else
            main_trail.add_propagation(new_var, true, id,
                    level);
    }
}

// handles conflict of the kinds:
//  - a <  var and var <  b but !(a <  b)
//  - a <= var and var <  b but !(a <  b)
//  - a <  var and var <= b but !(a <  b)
//  - a <= var and var <= b but !(a <= b)
// Build and propagate the last relation with the Furier Motzkin rule and 
// build a conflict between this new formula and the current assignment.
void LRAFeasibleSet::build_fm_conflict(Variable var,
        Variable lower_reason, bool lower_strict,
        Variable upper_reason, bool upper_strict) {
    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "LRA: conflict between bounds" << std::endl;
    }

    // the new disequality is strict if one of the two is strict
    bool new_strict = lower_strict || upper_strict;

    // get the polynomials
    Term lr_term = variable_table.get_term(lower_reason);
    Term ur_term = variable_table.get_term(upper_reason);
    const Polynomial &poly_low = term_table.get_polynomial(lr_term);
    const Polynomial &poly_up  = term_table.get_polynomial(ur_term);

    // create the new polynomial for the relation
    auto p = join_polynomial_in_var(poly_up, poly_low, var);
    Term new_poly_term = term_table.add_polynomial(p);

    // if p is already inside the system, it can change!
    const Polynomial &new_poly = term_table.get_polynomial(new_poly_term);

    // create the < 0 or <= 0 relation
    Term new_dis;
    if (new_strict)
        new_dis = term_table.add_binary_lt(new_poly_term,zero_term);
    else
        new_dis = term_table.add_binary_le(new_poly_term,zero_term);

    Variable new_var = variable_table.get_variable( new_dis.unsign() );

    // build the conflict
    conflict_reason.push_back( lower_reason );
    conflict_reason.push_back( upper_reason );
    conflict_reason.push_back( new_var );

    // if the new polynomial is not assigned assign it to the correct value.
    if ( ! main_trail.has_value( new_var ) ) {

        // find the correct level
        size_t level = 0;
        for ( const auto &m : new_poly ) {
            if ( m.indet != NULL_TERM ) {
                Variable var = variable_table.get_variable(m.indet);
                size_t var_level = main_trail.get_level(var);
                if ( var_level > level )
                    level = var_level;
            }
        }

        // set the relation to false in the trail
        if ( new_dis.is_positive() ) {
            main_trail.add_propagation(new_var, false, id, level);
        }
        else {
            main_trail.add_propagation(new_var, true, id, level);
        }
    }
}

// handles conflict of the kinds:
//  - var == a and var == b but a != b
//  - var != a and var == b but a == b
//  - var == a and var != b but a == b
// Build and propagate the last (dis)equality in the correct way and build the
// conflict between this new formula and the current assignment.
void LRAFeasibleSet::build_eq_conflict(Variable var, Variable first, Variable second) {
    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
        debug << "LRA: conflict between equalities" << std::endl;
    }

    // get the equality term
    assert( main_trail.has_value(first) );
    Term var_eq_a = variable_table.get_term(first);

    assert( main_trail.has_value(second) );
    Term var_eq_b = variable_table.get_term(second);

    assert(term_table.kind_of(var_eq_a) == TERM_ARITH_EQ);
    assert(term_table.kind_of(var_eq_b) == TERM_ARITH_EQ);

    // get the polynomials
    const Polynomial &poly_a = term_table.get_polynomial(var_eq_a);
    const Polynomial &poly_b = term_table.get_polynomial(var_eq_b);

    PolynomialPtr p = join_polynomial_in_var(poly_a,poly_b,var);
    Term new_poly_term = term_table.add_polynomial(p);

    // don't use p, take a reference to the internal polynomial
    const Polynomial &new_poly = term_table.get_polynomial(new_poly_term);

    // add the third relation as an equality, negate it if necessary.
    Term a_eq_b = term_table.add_binary_equality( new_poly_term,zero_term);
    Variable eq_var = variable_table.get_variable(a_eq_b.unsign());

    // add the new disequality to the conflict
    conflict_reason.push_back( eq_var );
    conflict_reason.push_back( first );
    conflict_reason.push_back( second );

    // if the new polynomial is not assigned assign it to the wrong value.
    if ( ! main_trail.has_value( eq_var ) ) {

        // find the correct level
        size_t level = 0;
        for ( const auto &m : new_poly ) {
            if ( m.indet != NULL_TERM ) {
                Variable variable = variable_table.get_variable(m.indet);
                size_t var_level = main_trail.get_level(variable);
                if ( var_level > level )
                    level = var_level;
            }
        }

        // if both the other two relations are equality, this should be
        // another equality, so set it to false
        if ( main_trail.get_boolean_value(first) &&
                main_trail.get_boolean_value(second) ) {
            main_trail.add_propagation(eq_var, false, id, level);
        }

        // otherwise is a disequality, set it to true
        else {
            main_trail.add_propagation(eq_var, true, id, level);
        }
    }
}

