/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_LRA_LRA_MODULE_HH
#define EOS_LRA_LRA_MODULE_HH

#include "sort.h"
#include "term.h"
#include "variable.h"
#include "theory_module.h"
#include "compact_vector.h"
#include "lra_feasible_set.h"

class LRAModule : public TheoryModule {
public:

    LRAModule(SMTSolver &solver);
    ~LRAModule() = default;

    void new_term_notify( Term t ) override;

    void propagate() override;

    void push() override;

    void pop() override;

    void make_decision(Variable) override;

    void get_conflict(std::vector<Variable>&) override;

    void get_justification(Variable, std::vector<Variable> &) override;

private:

    // clause for polynomial equality
    using EqClause = compact_vector_with_header<Variable,Variable>;
    using EqClausePtr = EqClause *;

    // clause for polynomial <=
    using GeClause = compact_vector_with_header<Variable,Variable>;
    using GeClausePtr = GeClause *;

    void new_equality(Term t);
    void new_greater_eq(Term t);
    void report_conflict();
    void propagate_eq(Variable var);
    void propagate_ge(Variable var);
    Rational get_real_value(Variable var);

    // equality clauses and their watch list
    std::vector<EqClausePtr> eq_clauses;
    std::unordered_map<Variable, std::vector<EqClausePtr>> eq_watchs;

    // equality clauses and their watch list
    std::vector<GeClausePtr> ge_clauses;
    std::unordered_map<Variable, std::vector<GeClausePtr>> ge_watchs;

    // store the current conclict clause
    std::vector<Variable> conflict_reason;

    // keep track of last position seen in trail by this module
    size_t propagation_starting_pos = 0;

    // feasible set for real variables
    LRAFeasibleSet feasible_set;

    // backtracking support
    std::vector<size_t> propagation_pos_stack;
    std::vector<Variable> all_real_vars;

    // caches and tmps
    std::vector<EqClausePtr>  to_move_eq;
    std::vector<EqClausePtr>  to_move_ge;
};

#endif // EOS_LRA_LRA_MODULE_HH
