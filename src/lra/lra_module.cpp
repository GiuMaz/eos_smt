/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lra_module.h"
#include "SMTSolver.h"
#include "printing.h"



//  OPERANDS
// ----------

// v1 < v2 on the current trail if:
// - v1 is unasigned and v2 is assigned
// - both v1 and v2 are unasigned and v1.index() < v2.index()
// - both v1 and v2 are assigned but v2 is assigned at a lower level
// - both v1 and v2 are assigend at the same level and v1.index() < v2.index()
// it is false otherwise
struct CompareByTrail {

    CompareByTrail(const Trail &t) : trail(t) {}
    const Trail &trail;

    bool operator()(const Variable &v1, const Variable &v2) {
        if ( ! trail.has_value(v1) ) {

            if ( trail.has_value(v2) )
                return true;

            return v1.index() < v2.index();
        }
        else {

            if ( ! trail.has_value(v2) )
                return false;

            // order by decreasing level
            if (trail.get_level(v1) > trail.get_level(v2))
                return true;

            if (trail.get_level(v1) == trail.get_level(v2))
                return v1.index() < v2.index();

            return false;
        }
    }
};



//  LINEAR ARITHMETIC SOLVER
// --------------------------

LRAModule::LRAModule(SMTSolver &s) :
    TheoryModule(s),
    feasible_set(s.get_trail(), s.get_term_table(), s.get_variable_table(),get_id())
{
    // Uninterpreted Function module UF
    ModuleId::set_name(get_id(),"LRA");

    // handle all boolean term
    s.notify_sort_ownership(get_id(), REAL_SORT);

    // handle specific term kind
    s.notify_term_ownership(get_id(), TERM_UNINTERPRETED);
    s.notify_term_ownership(get_id(), TERM_ARITH_EQ);
    s.notify_term_ownership(get_id(), TERM_ARITH_GE);

    // make decision on boolean variables
    s.notify_sort_decision(get_id(), REAL_SORT);
}

void LRAModule::new_term_notify(Term t) {

    if (MESSAGE_LEVEL(STANDARD_MESSAGE))
        debug << "LRA: add term " << term_table.pprint(t) << std::endl;

    auto kind = term_table.kind_of(t);

    switch (kind) {
        // add every real unint term to the feasible set
        case TERM_UNINTERPRETED:
            all_real_vars.push_back(variable_table.get_variable(t));
            break;

        // add clause for arith EQ
        case TERM_ARITH_EQ:
            new_equality(t);
            break;

        // add clause for arith GE
        case TERM_ARITH_GE:
            new_greater_eq(t);
            break;

        default:
            debug << "unknown term: " << term_table.pprint(t) << std::endl;
            assert(false);
            break;
    }
}

void LRAModule::new_equality(Term t) {
    // get equality argument
    const Polynomial &pol = term_table.get_polynomial(t);

    // build variables
    Variable eq_var = variable_table.get_variable(t);

    std::vector<Variable> vars;

    // add the equality
    vars.push_back(eq_var);
    // add the real variables
    for ( const auto &m : pol ) {
        // insert everything except for the constant real value
        if ( m.indet != NULL_TERM ) {
            assert( m.coeff != Rational(0) );
            vars.push_back( variable_table.get_variable(m.indet) );
        }
    }

    // at least the equality and one non-const variable
    assert(vars.size() >= 2);

    // sort the variable by level in trail
    std::sort(vars.begin(), vars.end(), CompareByTrail(trail));

    // build equality clause, it store the equality on the header an
    // a vector of the uninterpreted term in the clause
    EqClausePtr ref = EqClause::allocate(eq_var, vars);
    eq_clauses.push_back(ref);

    // add to watch list
    eq_watchs[vars[0]].push_back(ref);
    eq_watchs[vars[1]].push_back(ref);

    // search for fully assigned and unit clause, note that unasigned variables
    // are at the begin as a consegunce of the sort

    if ( ! trail.has_value(vars[1]) ) {
        // at least two unasigned variables, nothing to do
        return;
    }

    // if is fully assigned checks that the clause is consistent
    if ( trail.has_value(vars[0]) ) {
        // check if the boolean equality is consinstent with the value
        // assigned to the components

        Rational result;
        for ( const auto &m : pol ) {
            if ( m.indet != NULL_TERM ) {
                Variable var = variable_table.get_variable(m.indet);
                Rational val = get_real_value(var);
                result += ( val * m.coeff );
            }
            else {
                result += m.coeff;
            }
        }
        bool is_equal = trail.get_boolean_value(eq_var);
        bool is_equal_to_zero = ( result == Rational(0) );

        if (  (is_equal && !is_equal_to_zero) ||
                (!is_equal && is_equal_to_zero) ) {
            // conflict!
            if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                debug << "LRA: conflict! invalid equality" << std::endl;
            }
            conflict_reason.clear();

            // add all the member of the clause on conclict to the reason
            for ( const auto &v : *ref )
                conflict_reason.push_back(v);

            report_conflict();
        }

        // everything ok, simply return
        return;
    }

    // it must be a unit, assign it

    // if we have all the variables, set the relation properly
    if ( vars[0] == eq_var ) {

        // compute the result of the polynomial and the max level of the
        // assignment
        Rational result;
        size_t level = 0;
        for ( const auto &m : pol ) {
            if ( m.indet != NULL_TERM ) {
                // get variable and save the max level
                Variable var    = variable_table.get_variable(m.indet);
                level = std::max(level,trail.get_level(var));

                // compute result
                Rational val = get_real_value(var);
                result += ( val * m.coeff );
            }
            else {
                // add the constant to the result
                result += m.coeff;
            }
        }

        // is the pol == 0 correct?
        bool val = ( result == Rational(0) );
 
        if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
            debug << "LRA: value of " << variable_table.pprint(eq_var) <<
                " is " << result << std::endl;
        }

        // propagate
        trail.add_propagation(eq_var, val, get_id(), level);
        return;
    }

    // we have the equality relation and all but one real variable assigned,
    // assign a value accordingly to the relation

    Rational result_without_one;
    Rational unas_coeff;
    bool is_equal = trail.get_boolean_value(eq_var);

    for ( const auto &m : pol ) {
        if ( m.indet == NULL_TERM ) {
            // add the constant to the result
            result_without_one += m.coeff;
            continue;
        }
        // get variable 
        Variable var = variable_table.get_variable(m.indet);

        // the unasigned one, skip it
        if ( var == vars[0] ) {
            unas_coeff = m.coeff;
            continue;
        }

        // compute result
        Rational val = get_real_value(var);
        result_without_one += ( val * m.coeff );
    }

    // the value should be the opposite of the rest for the relation to be
    // an equality
    Rational val = - (Rational(1) / unas_coeff) * result_without_one;

    // update feasible set
    bool conflict;
    if ( is_equal )
        conflict = feasible_set.set_equality(vars[0], val , eq_var);
    else
        conflict = feasible_set.set_disequality(vars[0], val, eq_var);

    // contradictory relation
    if ( conflict ) {
        if (MESSAGE_LEVEL(STANDARD_MESSAGE))
            debug << "LRA: conflict! failed transitivity" << std::endl;

        // get the conflict for the infeasible equalities/disequalities
        conflict_reason.clear();
        feasible_set.get_conflict_reason(conflict_reason);

        report_conflict();
    }
}

void LRAModule::new_greater_eq(Term t) {
    // get equality argument
    const Polynomial &pol = term_table.get_polynomial(t);

    // build variables
    Variable ge_var = variable_table.get_variable(t);

    std::vector<Variable> vars;

    // add the equality
    vars.push_back(ge_var);

    // add the real variables
    for ( const auto &m : pol ) {
        // insert everything except for the constant real value
        if ( m.indet != NULL_TERM ) {
            assert( m.coeff != Rational(0) );
            vars.push_back( variable_table.get_variable(m.indet) );
        }
    }

    // at least the equality and one non-const variable
    assert(vars.size() >= 2);

    // sort the variable by level in trail
    std::sort(vars.begin(), vars.end(), CompareByTrail(trail));

    // build equality clause, it store the equality on the header an
    // a vector of the uninterpreted term in the clause
    GeClausePtr ref = GeClause::allocate(ge_var, vars);
    ge_clauses.push_back(ref);

    // add to watch list
    ge_watchs[vars[0]].push_back(ref);
    ge_watchs[vars[1]].push_back(ref);

    // search for fully assigned and unit clause, note that unasigned variables
    // are at the begin as a consegunce of the sort

    if ( ! trail.has_value(vars[1]) ) {
        // at least two unasigned variables, nothing to do
        return;
    }

    // if is fully assigned checks that the clause is consistent
    if ( trail.has_value(vars[0]) ) {
        // check if the greater equal is consinstent with the value
        // assigned to the components
        Rational result;
        for ( const auto &m : pol ) {
            if ( m.indet != NULL_TERM ) {
                Variable var = variable_table.get_variable(m.indet);
                Rational val = get_real_value(var);
                result += ( val * m.coeff );
            }
            else {
                result += m.coeff;
            }
        }

        // is a >= or a < ?
        bool is_ge = trail.get_boolean_value(ge_var);

        // check if the relation hold
        bool is_correct = is_ge ? result >= Rational(0) : result < Rational(0);

        if ( ! is_correct ) {
            // conflict!
            if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                debug << "LRA: conflict! invalid <=" << std::endl;
            }

            // add all the member of the clause on conclict to the reason
            // (both the real variable and the <= relation)
            conflict_reason.clear();
            for ( const auto &v : *ref )
                conflict_reason.push_back(v);

            report_conflict();
        }

        // everythings ok, simply return
        return;
    }

    // it must be a unit, assign it

    // if we have all the variables, set the relation properly
    if ( vars[0] == ge_var ) {

        // compute the result of the polynomial and the max level of the
        // assignment
        Rational result;
        size_t level = 0;
        for ( const auto &m : pol ) {
            if ( m.indet != NULL_TERM ) {
                // get variable
                Variable var = variable_table.get_variable(m.indet);

                level = std::max(level,trail.get_level(var));

                // compute result
                Rational val = get_real_value(var);
                result += ( val * m.coeff );
            }
            else {
                // add the constant to the result
                result += m.coeff;
            }
        }

        // is the pol >= 0 correct?
        bool val = ( result >= Rational(0) );

        if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
            debug << "LRA: value of " << variable_table.pprint(ge_var) <<
                " is " << result << std::endl;
        }

        // propagate
        trail.add_propagation(ge_var, val, get_id(), level);
        return;
    }

    // we have the equality relation and all but one real variable assigned,
    // assign a value accordingly to the relation

    // calculate the value without the unasigned var
    Rational result_without_one;
    Rational unas_coeff;
    for ( const auto &m : pol ) {
        if ( m.indet == NULL_TERM ) {
            // add the constant to the result
            result_without_one += m.coeff;
            continue;
        }
        // get variable 
        Variable var = variable_table.get_variable(m.indet);

        // the unasigned one, skip it
        if ( var == vars[0] ) {
            unas_coeff = m.coeff;
            continue;
        }

        // compute result
        Rational val = get_real_value(var);
        result_without_one += ( val * m.coeff );
    }

    // is the relation true?
    bool is_ge = trail.get_boolean_value(ge_var);

    // the value should be the opposite of the rest for the relation to be
    // an equality
    Rational bound = - (Rational(1) / unas_coeff) * result_without_one;
    // update feasible set
    bool conflict;
    if ( is_ge ) {
        // to set a bound in xi
        // a1*x1 + ... ai*xi ... + an*xn >= 0   =>
        // ai*xi >= -(a1*x1 + ...  + an*xn)     =>
        // xi ~ -(a1*x1 + ...  + an*xn)/ai
        // with ~ as >= if ai is >0, // <= oterwhise.
        if ( unas_coeff > 0 )
            conflict = feasible_set.set_lower_eq(vars[0], bound , ge_var);
        else
            conflict = feasible_set.set_upper_eq(vars[0], bound , ge_var);
    }
    else {
        // if the relation is negated we have ! >= ~> <
        // so we have
        // xi ~ -(a1*x1 + ...  + an*xn)/ai
        // with ~ as < if ai is >0, > otherwise
        if ( unas_coeff > 0 )
            conflict = feasible_set.set_upper_strict(vars[0], bound, ge_var);
        else
            conflict = feasible_set.set_lower_strict(vars[0], bound , ge_var);
    }


    // contradictory relation
    if ( conflict ) {
        if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
            debug << "LRA: conflict! failed transitivity" << std::endl;
        }

        conflict_reason.clear();
        feasible_set.get_conflict_reason(conflict_reason);
        report_conflict();
    }
}

void LRAModule::report_conflict() {
    trail.set_inconsistent(true);
    main_solver.report_conflict(get_id());
}

void LRAModule::propagate() {
    // propagate all the variable assigned since the last prop
    while ( propagation_starting_pos < trail.size() ) {

        Variable var = trail.at(propagation_starting_pos++);

        if ( ! trail.is_consistent() ) break;
        propagate_eq(var);

        if ( ! trail.is_consistent() ) break;
        propagate_ge(var);
    }
}

void LRAModule::propagate_eq(Variable var) {
    // get the variable to propagate
    to_move_eq.clear();

    auto it = eq_watchs.find(var);
    if ( it == eq_watchs.end() )
        return; // nothing to move

    std::swap(it->second, to_move_eq);

    if ( MESSAGE_LEVEL( STANDARD_MESSAGE ) ) {
        debug << "LRA: propagate variable: " << variable_table.pprint(var) <<
            " value " << trail.get_value(var) << std::endl;

        debug << "LRA: search new watch in clause:" << std::endl;
        for ( const auto &c : to_move_eq) {
            debug << "\t" << "[ ";
            for ( const auto &v : *c )
                debug << variable_table.pprint(v) << " ";
            debug << "]" << std::endl;
        }
    }

    for (auto it = to_move_eq.begin(); it != to_move_eq.end(); ++it) {

        if ( ! trail.is_consistent() ) {
            // conflict during the propagation, reset the list
            eq_watchs[var].insert(eq_watchs[var].begin(), it, to_move_eq.end()); 
            // exit
            break;
        }

        EqClause & clause = **it;

        // normalize assigned var in position 1
        if ( clause[0] == var ) {
            clause[0] = clause[1];
            clause[1] = var;
        }
        assert(clause[1] == var);

        bool foundt_new_watch = false;
        for ( auto v = clause.begin()+2; v != clause.end(); ++v ) {
            if ( !trail.has_value(*v) ) {
                // found new watch
                clause[1] = *v;
                *v = var;
                eq_watchs[clause[1]].push_back(*it);
                foundt_new_watch = true;
                break;
            }
        }

        // if a new watch is foundt skip to the next clause
        if ( foundt_new_watch ) continue;

        // otherwise it must be a unit or a fully assigned clause
        // reinsert in the previous watch list, we don't move anything
        eq_watchs[var].push_back(*it);

        // get the equality from the clause
        Variable eq_var = clause.get_header();
        const Polynomial &pol = term_table.get_polynomial(
                variable_table.get_term(eq_var));

        // everything assigned? check for consistency
        if ( trail.has_value(clause[0]) ) {
            Rational result;
            for ( const auto &m : pol ) {
                if ( m.indet != NULL_TERM ) {
                    // get variable
                    Variable var = variable_table.get_variable(m.indet);

                    // compute result
                    Rational val = get_real_value(var);
                    result += ( val * m.coeff );
                }
                else {
                    // add the constant to the result
                    result += m.coeff;
                }
            }

            // is the pol == 0 correct?
            bool is_eq_to_zero = ( result == Rational(0) );

            if ( trail.get_boolean_value(eq_var) != is_eq_to_zero )  {

                // conflict!
                if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                    debug << "LRA: conflict! invalid equality" << std::endl;
                }

                // add all the member of the clause on conclict to the reason
                // (both the real variable and the equality)
                conflict_reason.clear();

                // add all the member of the clause on conclict to the reason
                for ( const auto &v : clause )
                        conflict_reason.push_back(v);

                report_conflict();
            }

            // move to the next, if there were a conflict we have to clear
            // the rest of the list
            continue;
        }

        // if clause[0] is a relation
        if ( clause[0] == eq_var ) {
            // compute the result of the polynomial and the max level of the
            // assignment
            Rational result;
            size_t level = 0;
            for ( const auto &m : pol ) {
                if ( m.indet != NULL_TERM ) {
                    // get variable
                    Variable var = variable_table.get_variable(m.indet);
                    level = std::max(level,trail.get_level(var));

                    // compute result
                    Rational val = get_real_value(var);
                    result += (val * m.coeff);
                }
                else {
                    // add the constant to the result
                    result += m.coeff;
                }
            }

            // is the pol == 0 correct?
            bool is_eq_to_zero = ( result == Rational(0) );

            if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                debug << "LRA: value of " << variable_table.pprint(eq_var) <<
                    " is " << result << std::endl;
            }

            trail.add_propagation(eq_var, is_eq_to_zero, get_id(), level);
        }

        // if the clause[0] is an uninterpreted term
        else {
            // calculate the current value of the polynomial and evaluate
            // the expected relation with 0
            Rational result_without_one;
            Rational unas_coeff;
            bool is_equal = trail.get_boolean_value(eq_var);

            for ( const auto &m : pol ) {
                if ( m.indet == NULL_TERM ) {
                    // add the constant to the result
                    result_without_one += m.coeff;
                    continue;
                }
                // get variable 
                Variable var = variable_table.get_variable(m.indet);

                // the unasigned one, skip it
                if ( var == clause[0] ) {
                    unas_coeff = m.coeff;
                    continue;
                }

                // compute result
                Rational val = get_real_value(var);;
                result_without_one += ( val * m.coeff );
            }

            // the value should be the opposite of the rest for the relation to be
            // an equality
            Rational val = - (Rational(1) / unas_coeff) * result_without_one;

            // update feasible set
            bool conflict;
            if ( is_equal )
                conflict = feasible_set.set_equality(clause[0], val, eq_var);
            else
                conflict = feasible_set.set_disequality(clause[0], val, eq_var);

            // contradictory relation
            if ( conflict ) {

                if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                    debug << "LRA: conflict! failed transitivity" << std::endl;
                }

                conflict_reason.clear();
                feasible_set.get_conflict_reason(conflict_reason);
                report_conflict();
            }
        }
    }
}

void LRAModule::propagate_ge(Variable var) {
    // get the variable to propagate
    to_move_ge.clear();

    auto it = ge_watchs.find(var);
    if ( it == ge_watchs.end() )
        return; // nothing to move

    std::swap(it->second, to_move_ge);

    if ( MESSAGE_LEVEL( STANDARD_MESSAGE ) ) {
        debug << "LRA: propagate variable: " << variable_table.pprint(var) <<
            " value " << trail.get_value(var) << std::endl;

        debug << "LRA: search new watch in clause:" << std::endl;
        for ( const auto &c : to_move_ge) {
            debug << "\t" << "[ ";
            for ( const auto &v : *c )
                debug << variable_table.pprint(v) << " ";
            debug << "]" << std::endl;
        }
    }

    for (auto it = to_move_ge.begin(); it != to_move_ge.end(); ++it) {

        if ( ! trail.is_consistent() ) {
            // conflict during the propagation, reset the list
            ge_watchs[var].insert(ge_watchs[var].begin(), it, to_move_ge.end()); 
            // exit
            break;
        }

        GeClause & clause = **it;

        // normalize assigned var in position 1
        if ( clause[0] == var ) {
            clause[0] = clause[1];
            clause[1] = var;
        }
        assert(clause[1] == var);

        bool foundt_new_watch = false;
        for ( auto v = clause.begin()+2; v != clause.end(); ++v ) {
            if ( !trail.has_value(*v) ) {
                // found new watch
                clause[1] = *v;
                *v = var;
                ge_watchs[clause[1]].push_back(*it);
                foundt_new_watch = true;
                break;
            }
        }

        // if a new watch is foundt skip to the next clause
        if ( foundt_new_watch ) continue;

        // otherwise it must be a unit or a fully assigned clause
        // reinsert in the previous watch list, we don't move anything
        ge_watchs[var].push_back(*it);

        // get the info from the clause
        Variable ge_var = clause.get_header();
        const Polynomial &pol = term_table.get_polynomial(
                variable_table.get_term(ge_var));

        // everything assigned? check for consistency
        if ( trail.has_value(clause[0]) ) {
            Rational result;
            for ( const auto &m : pol ) {
                if ( m.indet != NULL_TERM ) {
                    // get variable
                    Variable var = variable_table.get_variable(m.indet);

                    // compute result
                    Rational val = get_real_value(var);
                    result += ( val * m.coeff );
                }
                else {
                    // add the constant to the result
                    result += m.coeff;
                }
            }

            // is the pol >= 0 correct?
            bool is_ge_zero = ( result >= Rational(0) );

            if ( trail.get_boolean_value(ge_var) != is_ge_zero )  {

                // conflict!
                if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                    debug << "LRA: conflict! invalid >=" << std::endl;
                }

                // add all the member of the clause on conclict to the reason
                // (both the real variable and the >=)
                conflict_reason.clear();

                // add all the member of the clause on conclict to the reason
                // (both the real variable and the >=), use the reason if
                // possible
                for ( const auto &v : clause )
                        conflict_reason.push_back(v);

                report_conflict();
            }

            // move to the next, if there were a conflict we have to clear
            // the rest of the list
            continue;
        }

        // if clause[0] is a relation
        if ( clause[0] == ge_var ) {
            // compute the result of the polynomial and the max level of the
            // assignment
            Rational result;
            size_t level = 0;
            for ( const auto &m : pol ) {
                if ( m.indet != NULL_TERM ) {
                    // get variable
                    Variable var = variable_table.get_variable(m.indet);
                    level = std::max(level,trail.get_level(var));

                    // compute result
                    Rational val = get_real_value(var);
                    result += (val * m.coeff);
                }
                else {
                    // add the constant to the result
                    result += m.coeff;
                }
            }

            // is the pol >= 0 correct?
            bool is_ge_zero = ( result >= Rational(0) );

            trail.add_propagation(ge_var, is_ge_zero, get_id(), level);
        }

        // if the clause[0] is an uninterpreted term
        else {
            // calculate the current value of the polynomial and evaluate
            // the expected relation with 0
            Rational result_without_one;
            Rational unas_coeff;

            for ( const auto &m : pol ) {
                if ( m.indet == NULL_TERM ) {
                    // add the constant to the result
                    result_without_one += m.coeff;
                    continue;
                }
                // get variable 
                Variable var = variable_table.get_variable(m.indet);

                // the unasigned one, skip it
                if ( var == clause[0] ) {
                    unas_coeff = m.coeff;
                    continue;
                }

                // compute result
                Rational val = get_real_value(var);
                result_without_one += ( val * m.coeff );
            }

            // the value should be the opposite of the rest for the relation to be
            // an equality
            Rational bound = - (Rational(1) / unas_coeff) * result_without_one;
            bool is_ge = trail.get_boolean_value(ge_var);

            // update feasible set
            bool conflict;
            if ( is_ge ) {
                // to set a bound in xi
                // a1*x1 + ... ai*xi ... + an*xn >= 0   =>
                // ai*xi >= -(a1*x1 + ...  + an*xn)     =>
                // xi ~ -(a1*x1 + ...  + an*xn)/ai
                // with ~ as >= if ai is >0, // <= oterwhise.
                if ( unas_coeff > 0 )
                    conflict = feasible_set.set_lower_eq(clause[0], bound , ge_var);
                else
                    conflict = feasible_set.set_upper_eq(clause[0], bound , ge_var);
            }
            else {
                // if the relation is negated we have ! >= ~> <
                // so we have
                // xi ~ -(a1*x1 + ...  + an*xn)/ai
                // with ~ as < if ai is >0, > otherwise
                if ( unas_coeff > 0 )
                    conflict = feasible_set.set_upper_strict(
                            clause[0], bound, ge_var);
                else
                    conflict = feasible_set.set_lower_strict(
                            clause[0], bound, ge_var);
            }

            // contradictory relation
            if ( conflict ) {

                if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                    debug << "LRA: conflict! failed transitivity" << std::endl;
                }

                conflict_reason.clear();
                feasible_set.get_conflict_reason(conflict_reason);
                report_conflict();
            }
        }
    }
}

void LRAModule::make_decision(Variable var) {
    auto val = feasible_set.get_feasible_value(var);
    trail.add_decision(var, Value(val), get_id());
}

void LRAModule::get_conflict(std::vector<Variable>& reason) {
    reason.clear();
    std::swap(reason,conflict_reason);
}

void LRAModule::get_justification(Variable v, std::vector<Variable> & reasons) {
    // get the polynomial
    Term t = variable_table.get_term(v);
    const auto &poly = term_table.get_polynomial(t);

    // add the reasons for the polynomail value, use the reason of the equality
    // when possible
    for ( const auto &m : poly ) {
        if ( m.indet != NULL_TERM ) {
            Variable indet  = variable_table.get_variable(m.indet);
            reasons.push_back( indet );
        }
    }
    assert( !reasons.empty() );
}

void LRAModule::push() {
    // save data on stack
    propagation_pos_stack.push_back(propagation_starting_pos);

    // push in other components
    feasible_set.push();
}

void LRAModule::pop() {
    // undo position in trail
    assert( !propagation_pos_stack.empty() );
    propagation_starting_pos = propagation_pos_stack.back();
    propagation_pos_stack.pop_back();

    // pop in other components
    feasible_set.pop();
}

Rational LRAModule::get_real_value(Variable var) {
    if ( trail.has_value(var) )
        return trail.get_value(var).get_rational_value();
    else
        return feasible_set.get_equality_value(var);
}
