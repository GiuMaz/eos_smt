/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_SRC_LRA_LRA_FEASIBLE_SET_HH
#define EOS_SRC_LRA_LRA_FEASIBLE_SET_HH

#include "variable.h"
#include "value.h"
#include "term.h"
#include "trail.h"
#include "module_id.h"

#include <unordered_map>
#include <vector>

class LRAFeasibleSet {
public:

    // a feasible set use the trail to identify the current assignments
    LRAFeasibleSet(Trail &, TermTable &, VariableTable &,const ModuleId &m);

    // return a feasible value for the variable var
    Rational get_feasible_value(Variable var);

    // set disequality between a variable and a given value.
    // it return true iff it found a conflict, otherwise it return false.
    // A conflict can happen if a variable is set to be equal and disequal
    // to the same value.
    bool set_disequality(Variable var, Rational val, Variable reason);

    // set equality between a variable and a given value.
    // it return true iff it found a conflict, otherwise it return false.
    // A conflict can happen if a variable is set to be equal and disequal
    // to the same value.
    bool set_equality(Variable var, Rational val, Variable reason);

    // set an UPPER bound of the kind variable < value.
    // it return true iff it found a conflict, otherwise it return false.
    bool set_upper_strict(Variable var, Rational val, Variable reason);

    // set an UPPER bound of the kind variable <= value.
    // it return true iff it found a conflict, otherwise it return false.
    bool set_upper_eq(Variable var, Rational val, Variable reason);

    // set a lower bound of the kind value <= variable.
    // it return true iff it found a conflict, otherwise it return false.
    bool set_lower_eq(Variable var, Rational val, Variable reason);

    // set a lower bound of the kind value < variable.
    // it return true iff it found a conflict, otherwise it return false.
    bool set_lower_strict(Variable var, Rational val, Variable reason);

    // return a reason for the equality, if no reason are set return NULL_VAL
    Variable get_equality_reason(Variable var);

    // return the value to the euality, it must be set
    Rational get_equality_value(Variable var);

    // build a conflict
    void get_conflict_reason(std::vector<Variable> &reason);

    // backtracking support
    void push();
    void pop();

private:

    // identify an equality in the trail in the form:
    // var ~ val, where ~ can be == or !=
    // this also store a reason for the relation. 
    struct Equality {
        // variable
        Variable var;
        // true if is an equality, false if is a disequality
        bool is_eq;
        // equal or disequal to this value
        Rational val;
        // reason for the relation
        Variable reason;
    };

    struct Bound {
        // variable
        Variable var;
        // is a strict disequality
        bool is_strict;
        // equal or disequal to this value
        Rational val;
        // reason for the relation
        Variable reason;
    };

    // get the offset in the different trails, if the variable have no known
    // value this return NO_POS
    uint32_t last_equality_pos(Variable var);
    uint32_t last_lower_pos(Variable var);
    uint32_t last_upper_pos(Variable var);

    // add a new element to the equality trail
    void set_new_eq(Variable var, bool is_eq, Rational val, Variable reason);
    void set_new_upper(Variable var, bool is_strict, Rational val, Variable reason);
    void set_new_lower(Variable var, bool is_strict, Rational val, Variable reason);

    // build a Fourier-Motzkin elimination when the upper and the lower bounds
    // of var are inconsistent
    void build_fm_conflict(Variable var, Variable lower_reason,
            bool lower_strict, Variable upper_reason, bool upper_strict);

    // build a conflict from two incompatible equalies
    void build_eq_conflict(Variable var, Variable first, Variable second);

    // build a conflict between and equality and one of the bound
    void build_eq_and_ge_conflict(
            Variable var, Variable equality, Variable greater_eq, bool is_upper);

    // conflict when a <= x <= b, a == b and x != a
    void build_disequality_conflict(
            Variable var,Variable eq, Variable lower, Variable upper);

    // check if a value for a variable is inside the bound
    bool is_value_in_bound(Variable var, Rational val);

    // join two polynomial eliminating a common variable var
    PolynomialPtr join_polynomial_in_var( const Polynomial &left,
            const Polynomial &right, Variable var);

    // usefull reference
    Trail &main_trail;
    TermTable &term_table;
    VariableTable &variable_table;
    const ModuleId &id;

    static const uint32_t NO_POS = UINT32_MAX;

    // store the current conclict clause
    std::vector<Variable> conflict_reason;

    // trail of equality
    std::vector<Equality> equality_trail;

    // trail for b <= x < a
    // note that only the lower bound is strickt
    std::vector<Bound> lower_trail;
    std::vector<Bound> upper_trail;

    // if the feasible set known an equality or a disequality about
    // a variable this map store the position of the last relation about it
    std::unordered_map<Variable,uint32_t> last_equality;

    std::unordered_map<Variable,uint32_t> last_lower;
    std::unordered_map<Variable,uint32_t> last_upper;

    // remember the position of the last known relation about var,
    // is set to NO_POS if we haven't any previous information
    std::vector<uint32_t> old_pos_eq;

    std::vector<uint32_t> old_pos_low;
    std::vector<uint32_t> old_pos_up;

    // remember the size of the equality trail between push and pop
    std::vector<size_t> trail_eq_stack;
    std::vector<size_t> trail_low_stack;
    std::vector<size_t> trail_up_stack;
};

#endif // EOS_SRC_LRA_LRA_FEASIBLE_SET_HH
