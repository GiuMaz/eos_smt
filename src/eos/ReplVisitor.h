/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_REPL_VISITOR_HH
#define EOS_REPL_VISITOR_HH

#include <antlr4-runtime.h>
#include <iostream>
#include <string>

#include "SMTLIBv2BaseVisitor.h"
#include "ExceptionErrorListener.h"
#include "SMTSolver.h"

namespace smtlibv2 {

class  ReplVisitor : SMTLIBv2BaseVisitor {
public:
    explicit ReplVisitor(SMTSolver &s);

    antlrcpp::Any visitStart(SMTLIBv2Parser::StartContext *ctx) override;

    antlrcpp::Any visitScript(SMTLIBv2Parser::ScriptContext *ctx) override;

    antlrcpp::Any visitSymbol(SMTLIBv2Parser::SymbolContext *ctx) override;

    antlrcpp::Any visitTerm(SMTLIBv2Parser::TermContext *ctx) override;

    antlrcpp::Any visitCommand(SMTLIBv2Parser::CommandContext *ctx) override;

    antlrcpp::Any visitSort(SMTLIBv2Parser::SortContext *ctx) override;

    antlrcpp::Any visitNumeral(SMTLIBv2Parser::NumeralContext *ctx) override;

    antlrcpp::Any visitDecimal(SMTLIBv2Parser::DecimalContext *ctx) override;

    antlrcpp::Any visitHexadecimal(SMTLIBv2Parser::HexadecimalContext *ctx) override;

    antlrcpp::Any visitBinary(SMTLIBv2Parser::BinaryContext *ctx) override;

    antlrcpp::Any visitSpec_constant(SMTLIBv2Parser::Spec_constantContext *ctx) override;

    antlrcpp::Any visitString(SMTLIBv2Parser::StringContext *ctx) override;

    antlrcpp::Any visitVar_binding(SMTLIBv2Parser::Var_bindingContext *ctx) override;

    void set_info_enabled(bool s) { enable_info = s; }

private:
    bool enable_info;

    SortTable &sort_table;
    TermTable &term_table;
    SMTSolver &solver;
};

}

#endif // EOS_REPL_VISITOR_HH
