/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "repl.h"
#include "printing.h"
#include "ExceptionErrorListener.h"
#include "error_handling.h"


using std::cout; using std::endl;

SMTv2Repl::SMTv2Repl( SMTSolver &s, bool en_info ) :
    enable_info(en_info), state(REPL_START), solver(s), visitor(s), error_listener() {}

void SMTv2Repl::exec( SMTv2Command c ) {

    // stateless command that don't use and don't modify the state
    switch ( c.kind ) {
        case COMMAND_EXIT:
            exec_exit();
            return;

        case COMMAND_SET_INFO:
            exec_set_info(c.args.info);
            return;

        case COMMAND_ERROR:
            std::cout << "(error \"" << c.args.str->data() << "\")" << std::endl;
            exit(0);
            return;

        case COMMAND_UNSUPPORTED:
            std::cout << "(usupported \"" << c.args.str->data() << "\")" << std::endl;
            return;

        default:
            break; // must be a state-specific command
    }

    // statefull command
    switch ( state ) {

        // start state is used to set a specific logic
        case REPL_START:
            switch ( c.kind ) {
                case COMMAND_SET_LOGIC:
                    // set logic and move to assertion state
                    state = REPL_ASSERT;
                    exec_set_logic(c.args.logic);
                    return;

                default:
                    std::cout << "(error \"logic is not set\")" << std::endl;
                    exit(0);
                    return;
            }
            break;

        // assert state is used to build the problem before a check-sat command
        case REPL_ASSERT:
            switch ( c.kind ) {
                case COMMAND_SET_LOGIC:
                    std::cout << "(error \"logic already set\")" << std::endl;
                    exit(0);
                    return;

                case COMMAND_ASSERT:
                    exec_assert(c.args.term);
                    return;

                case COMMAND_CHECK_SAT:
                    // move to SAT or UNSAT based on the result
                    if ( exec_check_sat() )
                        state = REPL_SAT;
                    else
                        state = REPL_UNSAT;
                    return;

                case COMMAND_DECLARE_CONST:
                    exec_declare_const(c.args.dec_const);
                    return;

                case COMMAND_DEFINE_CONST:
                    exec_define_const(c.args.def_const);
                    return;

                case COMMAND_DECLARE_FUN:
                    exec_declare_fun(c.args.dec_fun);
                    return;

                case COMMAND_DECLARE_SORT:
                    exec_declare_sort(c.args.dec_sort);
                    return;

                default:
                    std::cout << "(error \"invalid command in this state\")" << std::endl;
                    exit(0);
                    return;
            }
            break;

        // after check-sat when the problem is SAT
        case REPL_SAT:
            switch ( c.kind ) {
                case COMMAND_GET_MODEL:
                    exec_get_model();
                    return;

                default:
                    std::cout << "(error \"invalid command in this state\")" << std::endl;
                    exit(0);
                    return;
            }
            break;

        // after check-sat when the problem is UNSAT
        case REPL_UNSAT:
            switch ( c.kind ) {
                default:
                    std::cout << "(error \"invalid command in this state\")" << std::endl;
                    exit(0);
                    return;
            }
            break;

        default:
            return;
    }
}

void SMTv2Repl::exec_assert( Term t ) {
    if ( solver.get_term_table().sort_of(t) != bool_sort ) {
        cout << "(error \"assertion of non-boolean property\")" << endl;
        exit(0);
    }
    else 
        assertions.push_back(t);
}

bool SMTv2Repl::exec_check_sat() {

    solver.asserts(assertions);
    bool satisfiable = solver.check_sat();
    //bool satisfiable = false;
    if ( satisfiable ) {
        cout << "(sat)";
    }
    else {
        cout << "(unsat)";
    }
    cout << endl;

    return satisfiable;
}

void SMTv2Repl::exec_declare_const(declare_const_body_t args) {
    if ( args.sort == NULL_SORT ) {
        std::cout << "(error: \"use of undeclared sort\")" << std::endl;
        exit(0);
    }
    else {
        solver.get_term_table().check_add_unint_term(args.name, args.sort);
        solver.get_term_table().add_unint_term(args.name, args.sort);
    }
}

void SMTv2Repl::exec_define_const(define_const_body_t args) {
    if ( args.sort == NULL_SORT ) {
        std::cout << "(error: \"use of undeclared sort\")" << std::endl;
        exit(0);
    }
    else {
        // declare the constant
        solver.get_term_table().check_add_unint_term(args.name, args.sort);
        Term u = solver.get_term_table().add_unint_term(args.name, args.sort);
        // define the constant
        assertions.push_back(
                solver.get_term_table().add_binary_equality(u,args.t));
    }
}

void SMTv2Repl::exec_declare_fun(declare_function_body_t args) {
    Sort fun_s = solver.get_sort_table().add_function_signature(args.sort);
    solver.get_term_table().check_add_unint_term(args.name, fun_s);
    solver.get_term_table().add_unint_term(args.name, fun_s);
}

void SMTv2Repl::exec_declare_sort(declare_sort_body_t args) {
    if ( args.arity != 0 )
        cout << "(unsupported)" << endl;
    else
        solver.get_sort_table().declare_named_sort( args.name );
}

void SMTv2Repl::exec_exit() {
    exit(0);
}

void SMTv2Repl::exec_get_model() {
    // get model
    auto model = solver.get_model();
    standard << "(model \n" << model << ")" << std::endl;
}

void SMTv2Repl::exec_set_logic(smt_logic_t l) {
    solver.set_logic(l);
}

void SMTv2Repl::exec_set_info(set_info_body_t i) {
    if ( enable_info ) {

        // print info
        std::cout << "info " << i.keyword->data();
        if ( i.attribute_value )
            std::cout << " value " << i.attribute_value->data();
        std::cout << std::endl;

        // clean-up memory
        compact_string::deallocate(i.keyword);
        compact_string::deallocate(i.attribute_value);
    }
}

void SMTv2Repl::interactive_exec() {

    std::string line;
    int line_counter = 1;
    bool continue_repl = true;

    while ( continue_repl )  {
        std::cout << "eos[" << (line_counter++) << "]: ";

        if ( ! std::getline(std::cin,line) ) {
            std::cout << std::endl;
            continue_repl = false;
            continue;
        }

        if ( line.empty() ) continue;

        std::istringstream ss(line);
        antlr4::ANTLRInputStream input(ss);

        smtlibv2::SMTLIBv2Lexer lexer(&input);
        lexer.removeErrorListeners();
        lexer.addErrorListener(&error_listener);

        antlr4::CommonTokenStream  tokens(&lexer);
        smtlibv2::SMTLIBv2Parser parser(&tokens);    
        parser.removeErrorListeners();
        parser.addErrorListener(&error_listener);

        try {
            smtlibv2::SMTLIBv2Parser::CommandContext *c = parser.command();
            SMTv2Command cmd = visitor.visitCommand(c);
            exec( cmd );
        }
        catch (parsing_exception &e) {
            cout << "(error \"" << e.what() << "\")" << endl;
            exit(0);
        }
        catch (invalid_command_exception &e) {
            cout << "(error \"" << e.what() << "\")" << endl;
            exit(0);
        }

    }
}

void SMTv2Repl::file_exec( std::istream &stream) {

    antlr4::ANTLRInputStream input(stream);
    smtlibv2::SMTLIBv2Lexer lexer(&input);
    antlr4::CommonTokenStream  tokens(&lexer);
    smtlibv2::SMTLIBv2Parser parser(&tokens);    

    smtlibv2::SMTLIBv2Parser::StartContext *tree = parser.start();

    for ( auto &c : tree->script()->command() ) {
        try {
            SMTv2Command cmd = visitor.visitCommand(c);
            exec( cmd );
        }
        catch (parsing_exception &e) {
            cout << "(error \"" << e.what() << "\")" << endl;
            exit(0);
        }
        catch (invalid_command_exception &e) {
            cout << "(error \"" << e.what() << "\")" << endl;
            exit(0);
        }
    }
}
