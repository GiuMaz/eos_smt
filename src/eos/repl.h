/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_REPL_HH
#define EOS_REPL_HH

#include <iostream>
#include <fstream>
#include <sstream>

#include <antlr4-runtime.h>
#include "SMTLIBv2Lexer.h"
#include "SMTLIBv2Parser.h"
#include "ReplVisitor.h"

#include "compact_vector.h"
#include "sort.h"
#include "term.h"
#include "SMTSolver.h"



enum repl_state_t {
    REPL_START,
    REPL_ASSERT,
    REPL_SAT,
    REPL_UNSAT
};

enum repl_cmd_kind_t {
    // error
    COMMAND_ERROR,
    COMMAND_UNSUPPORTED,

    // supported
    COMMAND_ASSERT,
    COMMAND_CHECK_SAT,
    COMMAND_DECLARE_CONST,
    COMMAND_DEFINE_CONST,
    COMMAND_DECLARE_FUN,
    COMMAND_DECLARE_SORT,
    COMMAND_EXIT,
    COMMAND_GET_MODEL,
    COMMAND_SET_LOGIC,

    // unsupported
    COMMAND_CHECK_SAT_ASSUMING,
    COMMAND_DECLARE_DATATYPE,
    COMMAND_DECLARE_DATATYPES,
    COMMAND_DEFINE_FUN,
    COMMAND_DEFINE_FUNS_REC,
    COMMAND_DEFINE_FUN_REC,
    COMMAND_DEFINE_SORT,
    COMMAND_ECHO,
    COMMAND_GET_ASSERTION,
    COMMAND_GET_ASSIGNMENT,
    COMMAND_GET_INFO,
    COMMAND_GET_OPTION,
    COMMAND_GET_PROOF,
    COMMAND_GET_UNSAT_ASSUMPTIONS,
    COMMAND_GET_UNSAT_CORE,
    COMMAND_GET_VALUE,
    COMMAND_POP,
    COMMAND_PUSH,
    COMMAND_RESET,
    COMMAND_RESET_ASSERTIONS,
    COMMAND_SET_INFO,
    COMMAND_SET_OPTION
};

struct declare_const_body_t {
    Sort         sort;
    compact_string *name;
};

struct define_const_body_t {
    Sort            sort;
    compact_string *name;
    Term t;
};

struct set_info_body_t {
    compact_string *keyword;
    compact_string *attribute_value;
};

struct declare_sort_body_t {
    uint32_t       arity;
    compact_string *name;
};

struct declare_function_body_t {
    compact_string  *name;
    function_signature_t *sort;
};

union repl_cmd_body_t {

    int         integer;
    Term      term;
    smt_logic_t logic;

    compact_string *str;

    declare_sort_body_t     dec_sort;
    declare_function_body_t dec_fun;
    declare_const_body_t    dec_const;
    define_const_body_t     def_const;
    set_info_body_t         info;

    repl_cmd_body_t(): integer(0) {}
    explicit repl_cmd_body_t(int i): integer(i) {}
    explicit repl_cmd_body_t(Term t): term(t) {}
    explicit repl_cmd_body_t(smt_logic_t l): logic(l) {}

    explicit repl_cmd_body_t(compact_string *s): str(s) {}

    explicit repl_cmd_body_t(declare_sort_body_t     d): dec_sort(d) {}
    explicit repl_cmd_body_t(declare_function_body_t d): dec_fun(d) {}
    explicit repl_cmd_body_t(declare_const_body_t    d): dec_const(d) {}
    explicit repl_cmd_body_t(define_const_body_t     d): def_const(d) {}

    explicit repl_cmd_body_t(set_info_body_t i): info(i) {}
};



struct SMTv2Command {
    repl_cmd_kind_t kind;
    repl_cmd_body_t args;
};

class SMTv2CommandManager {
public:
    static SMTv2Command make_assert(Term t) {
        return { COMMAND_ASSERT, repl_cmd_body_t(t) };
    }

    static SMTv2Command make_check_sat() {
        return { COMMAND_CHECK_SAT, {} };
    }

    static SMTv2Command make_declare_const(compact_string *n, Sort s) {
        declare_const_body_t dc = {s,n};
        return { COMMAND_DECLARE_CONST, repl_cmd_body_t(dc) };
    }

    static SMTv2Command make_define_const(compact_string *n, Sort s, Term t) {
        define_const_body_t dc = {s,n,t};
        return { COMMAND_DEFINE_CONST, repl_cmd_body_t(dc) };
    }

    static SMTv2Command make_declare_fun(compact_string *n, function_signature_t *s) {
        declare_function_body_t df = {n,s};
        return { COMMAND_DECLARE_FUN, repl_cmd_body_t(df) };
    }

    static SMTv2Command make_declare_sort(compact_string *n, uint32_t a) {
        declare_sort_body_t ds = {a,n};
        return { COMMAND_DECLARE_SORT, repl_cmd_body_t(ds) };
    }

    static SMTv2Command make_exit() {
        return { COMMAND_EXIT, {} };
    }

    static SMTv2Command make_get_model() {
        return { COMMAND_GET_MODEL, {} };
    }

    static SMTv2Command make_set_logic(smt_logic_t l) {
        return { COMMAND_SET_LOGIC, repl_cmd_body_t(l) };
    }

    static SMTv2Command make_set_info(compact_string *key, compact_string * attr) {
        return { COMMAND_SET_INFO, repl_cmd_body_t({key, attr}) };
    }

    static SMTv2Command make_error(const std::string &message) {
        repl_cmd_body_t b = repl_cmd_body_t(compact_string::allocate(message));
        return { COMMAND_ERROR, b };
    }

    static SMTv2Command make_unsupported(const std::string &message) {
        repl_cmd_body_t b = repl_cmd_body_t(compact_string::allocate(message));
        return { COMMAND_UNSUPPORTED, b };
    }
};


 
class SMTv2Repl {

public:
    SMTv2Repl( SMTSolver &s, bool en_info = false );

    void interactive_exec();

    void file_exec( std::istream &stream);

private:

    bool enable_info; // true for info printing

    void exec( SMTv2Command c );

    void exec_assert( Term t );
    bool exec_check_sat();
    void exec_declare_const(declare_const_body_t args);
    void exec_define_const(define_const_body_t args);
    void exec_declare_fun(declare_function_body_t args);
    void exec_declare_sort(declare_sort_body_t args);
    void exec_exit();
    void exec_set_logic(smt_logic_t l);
    void exec_set_info(set_info_body_t i);
    void exec_get_model();

    repl_state_t state;

    SMTSolver &solver;
    smtlibv2::ReplVisitor visitor;
    ExceptionErrorListener error_listener;

    std::vector<Term> assertions;
};

#endif // EOS_REPL_HH
