/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ReplVisitor.h"
#include <vector>
#include <memory>
#include <set>
#include "term.h"
#include "rational.h"
#include "repl.h"

using namespace smtlibv2;
using namespace antlrcpp;

using std::cout; using std::endl;

ReplVisitor::ReplVisitor(SMTSolver &s) :
    SMTLIBv2BaseVisitor(),
    enable_info(false),
    sort_table(s.get_sort_table()),
    term_table(s.get_term_table()),
    solver(s) {}

Any ReplVisitor::visitCommand( SMTLIBv2Parser::CommandContext *ctx) {

    if (ctx->cmd_setLogic()) {
        auto sym = visitSymbol(ctx->symbol(0)).as<std::string>();
        return SMTv2CommandManager::make_set_logic(name_to_logic[sym]);
    }

    else if (ctx->cmd_declareSort()) {
        // support only declare sort of size 0
        auto sym = compact_string::allocate(
                visitSymbol(ctx->symbol(0)).as<std::string>());
        int  num = visitNumeral(ctx->numeral());
        return SMTv2CommandManager::make_declare_sort(sym, num);
    }

    else if (ctx->cmd_declareFun()) {

        // function symbol
        auto sym = compact_string::allocate(
                visitSymbol(ctx->symbol(0)).as<std::string>());

        size_t fun_size = ctx->sort().size(); // args + 1 for the codomain

        if ( fun_size == 1 ) { // simple const
            auto sort = visitSort(ctx->sort(0)).as<Sort>();
            return SMTv2CommandManager::make_declare_const(sym, sort);
        }
        else {
            function_signature_t *fun = function_signature_t::allocate(fun_size);

            // visit the n sorts of the domain + the single sort of the codomain
            for ( size_t i = 0; i < fun_size; ++i)
                fun->at(i) = visitSort(ctx->sort(i)).as<Sort>();

            return SMTv2CommandManager::make_declare_fun(sym, fun);
        }
    }

    else if (ctx->cmd_defineFun()) {
        // ONLY supported for function without arguments, otherwise a forall
        // is required

        // function declaration
        auto def = ctx->function_def();
        assert( def != nullptr);

        if ( def->sorted_var().size() > 0 )
            throw parsing_exception("definition of function require a forall");

        // function symbol
        auto sym = compact_string::allocate(
                visitSymbol(def->symbol()).as<std::string>());

        auto sort = visitSort(def->sort()).as<Sort>();

        Term t = visitTerm(def->term());

        return SMTv2CommandManager::make_define_const(sym, sort, t);
    }

    else if (ctx->cmd_declareConst()) {
        auto sym = compact_string::allocate(
                visitSymbol(ctx->symbol(0)).as<std::string>());
        auto sort = visitSort(ctx->sort(0)).as<Sort>();

        return SMTv2CommandManager::make_declare_const(sym, sort);
    }

    else  if (ctx->cmd_assert()) {
        Term t = visitTerm(ctx->term().front()).as<Term>();
        return SMTv2CommandManager::make_assert(t);
    }

    else if (ctx->cmd_checkSat()) {
        return SMTv2CommandManager::make_check_sat();
    }

    else if (ctx->cmd_getModel()) {
        return SMTv2CommandManager::make_get_model();
    }

    else if (ctx->cmd_exit()) {
        return SMTv2CommandManager::make_exit();
    }

    else if (ctx->cmd_setInfo()) {
        if (enable_info) {
            compact_string *keyword = 
                compact_string::allocate(ctx->attribute()->keyword()->getText());

            compact_string *attribute_value = nullptr;

            if (ctx->attribute()->attribute_value())
                attribute_value = compact_string::allocate(
                        ctx->attribute()->attribute_value()->getText());

            return SMTv2CommandManager::make_set_info(keyword, attribute_value);
        }
        else {
            return SMTv2CommandManager::make_set_info(nullptr, nullptr);
        }
    }

    else {
        // UNSUPORTED FEATURES:
        // get-info, get-assertions, declare-dataytpe/s, check-sat-assuming,
        // define-fun-rec/s, define-sort, echo, get-assignment, get-option,
        // get-proof, get-unsat-assumptions, get-unsat-core, get-value, push,
        // pop, reset,  reset-assertions, set-option
        return SMTv2CommandManager::make_unsupported(
                std::string("unsupported command ") + ctx->children[1]->getText());
    }
}

Any ReplVisitor::visitSymbol(SMTLIBv2Parser::SymbolContext *ctx) {
    return ctx->getText();
}

Any ReplVisitor::visitStart(SMTLIBv2Parser::StartContext *ctx) {
    return visitScript(ctx->script());
}

Any ReplVisitor::visitScript(SMTLIBv2Parser::ScriptContext *ctx) {
    for ( auto c : ctx->command() )
        visitCommand(c);
    return nullptr;
}

Any ReplVisitor::visitSort(SMTLIBv2Parser::SortContext *ctx) {
    if (ctx->ParOpen()) {
        throw parsing_exception("composite sort unsupported");
    }
    else {
        return solver.get_sort_table().get_named_sort(ctx->getText());
    }
}

Any ReplVisitor::visitNumeral(SMTLIBv2Parser::NumeralContext *ctx) {
    return std::stoi(ctx->getText());
}

Any ReplVisitor::visitDecimal(SMTLIBv2Parser::DecimalContext *ctx) {
    return Rational(ctx->getText());
}

Any ReplVisitor::visitHexadecimal(SMTLIBv2Parser::HexadecimalContext *ctx) {
    // convert smt hex format to c++ hex format
    std::string str_num = ctx->getText();
    str_num[0] = '0'; // #x... to 0x...

    // read hex number
    unsigned int num;
    std::istringstream iss(str_num);
    iss >> std::hex >> num;
    return num;
}

Any ReplVisitor::visitSpec_constant(SMTLIBv2Parser::Spec_constantContext *ctx) {
    // integer number
    if ( ctx->numeral() ) {
        int num = visitNumeral(ctx->numeral());
        return Rational(num);
    }

    // decimal number
    if ( ctx->decimal() ) {
        return visitDecimal(ctx->decimal());
    }

    // hexadecimal integer
    if ( ctx->hexadecimal() ) {
        unsigned int num = visitHexadecimal(ctx->hexadecimal());
        return Rational(num);
    }

    // binary integer
    if ( ctx->binary() ) {
         unsigned int num = visitBinary(ctx->binary());
         return Rational(num);
    }

    // string
    if ( ctx->string() )
        return visitString(ctx->string());

    assert(false);
    return nullptr;
}

Any ReplVisitor::visitBinary(SMTLIBv2Parser::BinaryContext *ctx) {
    std::string str_num = ctx->getText();
    unsigned int num = 0, pow_of_2 = 1;

    auto iter = str_num.rbegin();
    while ( iter != str_num.rend() && *iter != 'b') {
        if ( *iter == '1' )
            num += pow_of_2;
        pow_of_2 = pow_of_2 << 1;
        ++iter;
    }

    return num;
}

Any ReplVisitor::visitString(SMTLIBv2Parser::StringContext *ctx) {
    return ctx->getText();
}

Any ReplVisitor::visitVar_binding(SMTLIBv2Parser::Var_bindingContext *ctx) {
    compact_string *name = compact_string::allocate(ctx->symbol()->getText());
    Term t =  visitTerm(ctx->term());

    term_table.add_local_name_to_term(name,t);
    return nullptr;
}

/*
 * It is too expansive to write visitTerm as a recursive function. This method
 * implement a stack to visit all the subterm before computing the value of
 * a term.
 *
 * The stack use the following component:
 *  - context_stack save the context of a term, if the term don't have subterms
 *    (like numbers and variables) the context is computed directly, otherwise
 *    is required to compute all the subterms before.
 *  - done_stack is of the same size of context stack and have a value set to
 *    true iff the context_stack has all its subterms already in the context
 *    stack
 *  - term_stack save the computed term, computing a complex term consumes all
 *    its subterms.
 *  - term_size_stack save the size of the term stack at the beggining of the
 *    computing of the subterms of a terms. When the term is finally computed
 *    all the terms before this value are ignored
 *  - let_stack is used to save when we are inside a local context with some
 *    local names declared
 */
Any ReplVisitor::visitTerm(SMTLIBv2Parser::TermContext *global_ctx) {

    std::vector<SMTLIBv2Parser::TermContext *> context_stack;
    std::vector<bool> done_stack;
    std::vector<bool> let_stack;
    std::vector<Term> term_stack;
    std::vector<size_t> term_size_stack;

    // buffer for the arguments
    std::vector<Term> args;

    context_stack.push_back(global_ctx);
    done_stack.push_back(false);

    while ( !context_stack.empty() ) {
        assert( context_stack.size() == done_stack.size() );

        // pick last context
        auto ctx  = context_stack.back();
        bool done = done_stack.back();

        // terminal case: numeric constant
        if ( ctx->spec_constant() ) {
            auto number = visitSpec_constant(ctx->spec_constant());
            if ( number.is<std::string>() )
                throw parsing_exception("expected a number");

            Rational q = number.as<Rational>();

            // push numeric constant on the term stack
            term_stack.push_back(term_table.add_real_constant(q));

            // nothing more to do
            assert(!context_stack.empty());
            context_stack.pop_back();
            assert(!done_stack.empty());
            done_stack.pop_back();
        }
        // composite case: visit the arguments first, when they are done
        // produce a term
        else if ( ctx->ParOpen(0) != nullptr && ctx->qual_identifer() != nullptr) {

            // if the argument aren't already processed, put them in the stack
            if ( ! done ) {

                // mark the curren term as completed, save the size of the
                // term stack
                done_stack.back() = true;
                term_size_stack.push_back(term_stack.size());

                const std::vector<SMTLIBv2Parser::TermContext *> &terms = ctx->term();
                // visit all arguments (put them in reverse order in the stack)
                for(auto it = terms.rbegin(); it != terms.rend(); ++it) {
                    context_stack.push_back(*it);
                    done_stack.push_back(false);
                }

                // don't pop the context, process the new stack before
                continue;
            }

            // the arguments are processed, produce a new term

            // move the argument from the stack to the buffer
            args.clear();
            auto it = term_stack.begin() + term_size_stack.back();
            std::move(it, term_stack.end(), std::back_inserter(args));
            term_stack.resize(term_size_stack.back());

            // pop the processed term
            assert( done_stack.back() == true );
            assert(!context_stack.empty());
            context_stack.pop_back();
            assert(!done_stack.empty());
            done_stack.pop_back();
            assert(!term_size_stack.empty());
            term_size_stack.pop_back();

            // find operation
            const auto &op = ctx->qual_identifer()->getText(); 

            // TODO: ottimizzare i vari confronti trasformando in enum
            if ( op == "not") {
                term_table.check_not(args);
                term_stack.push_back(term_table.add_not(args[0]));
            }
            else if ( op == "or") {
                term_table.check_or(args);
                term_stack.push_back(term_table.add_or(args));
            }
            else if ( op == "and") {
                term_table.check_and(args);
                term_stack.push_back(term_table.add_and(args));
            }
            else if ( op == "=>") {
                term_table.check_implication(args);
                term_stack.push_back(term_table.add_implication(args));
            }
            else if ( op == "xor") {
                term_table.check_xor(args);
                term_stack.push_back(term_table.add_xor(args));
            }
            else if ( op == "=") {
                term_table.check_equality(args);
                term_stack.push_back(term_table.add_equality(args));
            }
            else if ( op == "distinct") {
                term_table.check_distinct(args);
                term_stack.push_back(term_table.add_distinct(args));
            }
            else if ( op == "+") {
                term_table.check_sum(args);
                term_stack.push_back(term_table.add_sum(args));
            }
            else if ( op == "-") {
                // - a[0]
                if ( args.size() == 1 ) {
                    term_table.check_negated_arith_term(args[0]);
                    term_stack.push_back(term_table.add_negated_arith_term(args[0]));
                }
                // a[0] - a[1] - ...
                else {
                    term_table.check_sub(args);
                    term_stack.push_back(term_table.add_sub(args));
                }
            }
            else if ( op == "/") {
                term_table.check_division(args);
                term_stack.push_back(term_table.add_division(args));
            }
            else if ( op == "*") {
                term_table.check_multiplication(args);
                term_stack.push_back(term_table.add_multiplication(args));
            }
            else if ( op == "<") {
                // < only between two terms, otherwise is and and of <
                assert(args.size() >= 2);

                if ( args.size() == 2 ) {
                    term_table.check_binary_lt(args[0],args[1]);
                    term_stack.push_back(term_table.add_binary_lt(args[0],args[1]));
                }
                else {
                    std::vector<Term> conj;
                    for ( size_t i = 0; i < args.size()-1; ++i) {
                        term_table.check_binary_lt(args[i],args[i+1]);
                        conj.push_back( term_table.add_binary_lt(args[i],args[i+1]) );
                    }
                    term_stack.push_back(term_table.add_and(conj));
                }
            }
            else if ( op == "<=") {
                // <= only between two terms, otherwise is and and of <=
                assert(args.size() >= 2);

                if ( args.size() == 2 ) {
                    term_table.check_binary_le(args[0],args[1]);
                    term_stack.push_back(term_table.add_binary_le(args[0],args[1]));
                }
                else {
                    std::vector<Term> conj;
                    for ( size_t i = 0; i < args.size()-1; ++i) {
                        term_table.check_binary_le(args[i],args[i+1]);
                        conj.push_back( term_table.add_binary_le(args[i],args[i+1]) );
                    }
                    term_stack.push_back(term_table.add_and(conj));
                }
            }
            else if ( op == ">") {
                // > only between two terms, otherwise is and and of >
                assert(args.size() >= 2);

                if ( args.size() == 2 ) {
                    term_table.check_binary_gt(args[0],args[1]);
                    term_stack.push_back(term_table.add_binary_gt(args[0],args[1]));
                }
                else {
                    std::vector<Term> conj;
                    for ( size_t i = 0; i < args.size()-1; ++i) {
                        term_table.check_binary_gt(args[i],args[i+1]);
                        conj.push_back( term_table.add_binary_gt(args[i],args[i+1]) );
                    }
                    term_stack.push_back(term_table.add_and(conj));
                }
            }
            else if ( op == ">=") {
                // >= only between two terms, otherwise is and and of >=
                assert(args.size() >= 2);

                if ( args.size() == 2 ) {
                    term_table.check_binary_ge(args[0],args[1]);
                    term_stack.push_back(term_table.add_binary_ge(args[0],args[1]));
                }
                else {
                    std::vector<Term> conj;
                    for ( size_t i = 0; i < args.size()-1; ++i) {
                        term_table.check_binary_ge(args[i],args[i+1]);
                        conj.push_back( term_table.add_binary_ge(args[i],args[i+1]) );
                    }
                    term_stack.push_back(term_table.add_and(conj));
                }
            }
            else if ( op == "ite") {
                term_table.check_ite(args);
                term_stack.push_back(term_table.add_ite(args));
            }
            else {
                // it must be a function application
                Term fun = term_table.get_named_term(op);
                term_table.check_fun_app(op, args);
                term_stack.push_back(term_table.add_fun_app(fun, args));
            }
        }
        else if ( ctx->qual_identifer()) {
            const std::string &name = ctx->getText();
            // true and false are special reserved words
            if ( name == "true" )
                term_stack.push_back(true_term);
            else if ( name == "false" )
                term_stack.push_back(false_term);
            else {
                // it must be a named term
                Term named_term = term_table.get_named_term(ctx->getText());
                if ( named_term == NULL_TERM ) {
                    throw parsing_exception(
                            "use of undeclared symbol " + ctx->getText());
                }

                term_stack.push_back(named_term);
            }

            // nothing more to do, pop the context
            assert(!context_stack.empty());
            context_stack.pop_back();
            assert(!done_stack.empty());
            done_stack.pop_back();
            continue;
        }
        else  {
            if ( ctx->GRW_Let() ) {

                // if the argument aren't already processed, put them in the stack
                if ( ! done ) {
                    // mark the curren term as completed, save the size of the
                    // term stack
                    done_stack.back() = true;
                    let_stack.push_back(false);
                    term_size_stack.push_back(term_stack.size());

                    const auto &binding = ctx->var_binding();
                    // compute all the term on the let
                    for(auto it = binding.rbegin(); it != binding.rend(); ++it) {
                        context_stack.push_back((*it)->term());
                        done_stack.push_back(false);
                    }

                    // don't pop the context, process the new stack before
                }
                // term processed, bind them to their name and process the
                // local context
                else if ( let_stack.back() == false ) {

                    // create a local context
                    let_stack.back() = true;
                    term_table.push_local_context();

                    // move the argument from the stack to the buffer
                    args.clear();
                    auto it = term_stack.begin() + term_size_stack.back();
                    std::move(it, term_stack.end(), std::back_inserter(args));
                    term_stack.resize(term_size_stack.back());

                    // bind local variable
                    assert( ctx->var_binding().size() == args.size() );
                    for ( size_t i = 0; i < args.size(); ++i ) {
                        // get name
                        compact_string *name = compact_string::allocate(
                                ctx->var_binding(i)->symbol()->getText());
                        // bind name to term
                        term_table.add_local_name_to_term(name,args[i]);
                    }

                    // visit term t in the local context
                    context_stack.push_back(ctx->term(0));
                    done_stack.push_back(false);
                }
                else {
                    // undo local context
                    term_table.pop_local_context();

                    // the stack should have the term evaluated in the
                    // local context
                    assert( term_size_stack.back() == (term_stack.size()-1)  );

                    // pop let
                    assert(!let_stack.empty());
                    let_stack.pop_back();

                    // pop the processed term
                    assert( done_stack.back() == true );
                    assert(!context_stack.empty());
                    context_stack.pop_back();
                    assert(!done_stack.empty());
                    done_stack.pop_back();
                    assert(!term_size_stack.empty());
                    term_size_stack.pop_back();
                }
            }
            else if ( ctx->GRW_Exclamation() ) {
                // TODO: save the name of the term

                // move the the subterm
                assert(!context_stack.empty());
                context_stack.pop_back();
                assert(!done_stack.empty());
                done_stack.pop_back();

                context_stack.push_back(ctx->term(0));
                done_stack.push_back(false);
            }
            else {
                // GRW_Forall, GRW_Exists, GRW_Match, 
                throw parsing_exception("unsupported operator "+ctx->children[1]->getText());
            }
        }
    }

    assert( context_stack.empty()  );
    assert( done_stack.empty()  );
    assert( term_size_stack.empty()  );
    assert( let_stack.empty()  );
    assert( term_stack.size() == 1 );
    return term_stack[0];
}

