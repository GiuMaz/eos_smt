/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <fstream>

#include "SMTSolver.h"
#include "boolean_module.h"
#include "uf_module.h"
#include "lra_module.h"
#include "term.h"
#include "sort.h"
#include "repl.h"
#include "printing.h"

int main(int argc, char* argv[]) {

    // set the verbosity level
    set_message_level(STANDARD_MESSAGE);
    set_default_debug_stream();
    set_default_standard_stream();

    bool repl_mode = (argc < 2);

    SortTable sort_table;
    TermTable term_table(sort_table);

    SMTSolver solver(term_table);
    BooleanModule boolean_mod(solver);
    UFModule uf_mod(solver);
    LRAModule lra_mod(solver);

    SMTv2Repl repl(solver);

    if ( repl_mode ) {
        repl.interactive_exec();
    }
    else {
        std::ifstream stream;
        stream.open(argv[1]);

        if ( argc > 2 ) {
            for ( int i = 2; i < argc; ++i)
                std::cout << "ignored argument " << argv[i] << std::endl;
        } 

        if ( !stream ) {
            std::cout << "invalide file " << argv[1] << std::endl;
            return 1;
        }

        repl.file_exec(stream);
    }

    return 0;
}

