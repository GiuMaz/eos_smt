/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "uf_module.h"
#include "SMTSolver.h"
#include "printing.h"



//  OPERANDS
// ----------

// v1 < v2 on the current trail if:
// - v1 is unasigned and v2 is assigned
// - both v1 and v2 are unasigned and v1.index() < v2.index()
// - both v1 and v2 are assigned but v2 is assigned at a lower level
// - both v1 and v2 are assigend at the same level and v1.index() < v2.index()
// it is false otherwise
struct CompareByTrail {

    CompareByTrail(const Trail &t) : trail(t) {}
    const Trail &trail;

    bool operator()(const Variable &v1, const Variable &v2) {
        if ( ! trail.has_value(v1) ) {

            if ( trail.has_value(v2) )
                return true;

            return v1.index() < v2.index();
        }
        else {

            if ( ! trail.has_value(v2) )
                return false;

            // order by decreasing level
            if (trail.get_level(v1) > trail.get_level(v2))
                return true;

            if (trail.get_level(v1) == trail.get_level(v2))
                return v1.index() < v2.index();
        }

        return false;
    }
};



//  UNINTERPRETED FUNCTION AND EQUALITY SOLVER
// --------------------------------------------

UFModule::UFModule(SMTSolver &s):
    TheoryModule(s),
    rep_table(s.get_trail(), s.get_term_table(),s.get_variable_table()),
    feasible_set(s.get_trail(), s.get_term_table(), s.get_sort_table(), s.get_variable_table())
{
    // Uninterpreted Function module UF
    ModuleId::set_name(get_id(),"UF");

    // handle all boolean term
    s.notify_sort_ownership(get_id(), UNINTERPRETED_SORT);

    // handle specific term kind
    s.notify_term_ownership(get_id(), TERM_EQ);
    s.notify_term_ownership(get_id(), TERM_FUN_APPLICATION);
    s.notify_term_ownership(get_id(), TERM_UNINTERPRETED);

    // make decision on boolean variables
    s.notify_sort_decision(get_id(), UNINTERPRETED_SORT);
}

void UFModule::new_term_notify(Term t) {

    if ( MESSAGE_LEVEL(VERBOUSE_MESSAGE) ) {
        debug << "UF: add term " << term_table.pprint(t) << std::endl;
    }

    auto kind = term_table.kind_of(t);

    switch (kind) {
        case TERM_UNINTERPRETED:
            all_variables.push_back( variable_table.get_variable(t) );
            break;
        case TERM_EQ:
            new_equality(t);
            break;
        case TERM_FUN_APPLICATION:
            new_fun_app(t);
            break;
        default:
            assert(false);
            break;
    }
}

void UFModule::new_equality(Term t) {
    // get equality argument
    const auto &comp = term_table.get_composite_args(t);

    // build variables
    Variable eq_var = variable_table.get_variable(t);
    Variable left   = variable_table.get_variable(comp[0].unsign());
    Variable right  = variable_table.get_variable(comp[1].unsign());

    // handle only equalities between uninterpreted sorts, the other are
    // handled by specific modules
    if (sort_table.kind_of(term_table.sort_of(comp[0])) != UNINTERPRETED_SORT)
        return;

    // sort the variable by level in trail
    std::vector<Variable> vars = {eq_var, left, right};
    std::sort(vars.begin(), vars.end(), CompareByTrail(trail));

    // build equality clause
    EqClausePtr ref = EqClause::allocate(vars);
    eq_clauses.push_back(ref);

    // add to watch list
    eq_watchs[vars[0]].push_back(ref);
    eq_watchs[vars[1]].push_back(ref);

    // search for fully assigned and unit clause, note that unasigned clause
    // are at the begin

    if ( ! trail.has_value(vars[1]) ) {
        // at least two unasigned variables, nothing to do
        return;
    }

    // if is fully assigned checks that the clause is consistent
    if ( trail.has_value(vars[0]) ) {
        // check if the boolean equality is consinstent with the value
        // assigned to the two component
        if ( trail.get_boolean_value(eq_var) !=
                (trail.get_value(left) == trail.get_value(right)) ) {
            if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                debug << "UF: conflict! invalid equality equality" << std::endl;
            }
            conflict_reason.clear();
            conflict_reason.push_back(eq_var);
            conflict_reason.push_back(left);
            conflict_reason.push_back(right);
            report_conflict();
        }
        return;
    }

    // it must be a unit, assign it

    // if we have the two variables, set the relation properly
    if ( vars[0] == eq_var ) {
        auto level = std::max(trail.get_level(left), trail.get_level(right));
        bool val = trail.get_value(left) == trail.get_value(right);
        trail.add_propagation(eq_var, val, get_id(), level);
        return;
    }
    // we have the equality relation and one of the variables, update the
    // feasible set accordingly

    // get the value
    const Variable &assigned = vars[1] == eq_var ? vars[2] : vars[1];
    Value val = trail.get_value(assigned);
    bool is_eq = trail.get_boolean_value(eq_var);

    // find if equality or disequality
    bool conflict;

    // update feasible set
    if ( is_eq )
        conflict = feasible_set.set_equality(vars[0], val , eq_var);
    else
        conflict = feasible_set.set_disequality(vars[0], val, eq_var);

    // contradictory relation
    if ( conflict ) {
        conflict_reason.clear();
        if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
            debug << "UF: conflict! failed transitivity" << std::endl;
        }
        feasible_set.build_conflict_reason(vars[0], conflict_reason);

        assert( conflict_reason.size() == 3 );
        // add a propagation if a member is not assigned
        if ( ! trail.has_value(conflict_reason[2]) ) {
            // assign the equality on the trail in the proper level
            // to create the conflict
            bool val_a = trail.get_boolean_value(conflict_reason[0]);
            bool val_b = trail.get_boolean_value(conflict_reason[1]);

            Term t = variable_table.get_term(conflict_reason[2]);
            const auto &comp = term_table.get_composite_args(t);
            size_t level = std::max(
                    trail.get_level(variable_table.get_variable(comp[0])),
                    trail.get_level(variable_table.get_variable(comp[1])));

            //size_t level = 0;
            trail.add_propagation(conflict_reason[2],
                    val_a != val_b, get_id(), level);
        }

        report_conflict();
    }
}

void UFModule::new_fun_app(Term t) {
    // build the variable
    Variable app_var = variable_table.get_variable(t);
    all_application.push_back(app_var);

    // get all the application arguments
    std::vector<Variable> args;
    const auto &comp = term_table.get_composite_args(t);

    for (size_t i = 0; i < comp.size()-1; ++i)
        args.push_back(variable_table.get_variable(comp[i].unsign()));

    // sort by trail values
    std::sort(args.begin(), args.end(), CompareByTrail(trail));

    // build an application clause
    AppClausePtr ref = AppClause::allocate(app_var,args);
    app_clauses.push_back(ref);
    // watch one argument for find fulla assignemnt
    app_watchs[args[0]].push_back(ref);

    // it's fully assigned ?
    if ( trail.has_value(args[0]) )
        rep_table.get_representant(app_var);
}

void UFModule::propagate() {
    // propagate all the variable assigned since the last prop
    while ( propagation_starting_pos < trail.size() ) {
        Variable var = trail.at(propagation_starting_pos++);

        if ( ! trail.is_consistent() ) break;
        propagate_fun_app(var);

        if ( ! trail.is_consistent() ) break;
        propagate_eq(var);
    }
}

void UFModule::propagate_fun_app(Variable var) {
    // get the variable to propagate
    to_move_app.clear();

    auto it = app_watchs.find(var);
    if ( it != app_watchs.end() ) {

        std::swap(it->second, to_move_app);

        if ( MESSAGE_LEVEL( STANDARD_MESSAGE ) ) {
            debug << "UF: propagate variable: " << var << " value " <<
                trail.get_value(var) << std::endl;

            debug << "UF: search new watch in clause:" << std::endl;
            for ( const auto &c : to_move_app)
                debug << "\t" << *c << std::endl;
        }

        for (auto it = to_move_app.begin(); it != to_move_app.end(); ++it) {
            if ( ! trail.is_consistent() ) {
                // conflict during the propagation, reset the list
                app_watchs[var].insert(app_watchs[var].begin(), it, to_move_app.end()); 
                // exit
                break;
            }

            AppClause & clause = **it;
            Variable fun_app = clause.get_header();

            // search a new variable to watch
            assert( trail.has_value(clause[0]) );
            bool foundt_new_watch = false;
            for ( size_t i = 1; i < clause.size(); ++i ) {
                if ( ! trail.has_value(clause[i])) {
                    // found new watch
                    foundt_new_watch = true;
                    clause[0] = clause[i];
                    clause[i] = var;
                    app_watchs[clause[0]].push_back(*it);
                    break;
                }
            }

            // if we found a new watch move to the next one
            if (foundt_new_watch) continue;

            // reset to the previous position, we don't move anything
            app_watchs[clause[0]].push_back(*it);

            // get the representant or build a new one
            Variable rep = rep_table.get_representant(fun_app);
            assert( rep != NULL_VAR );

            if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                debug << "UF: representant of " <<
                    variable_table.pprint(fun_app) << " is " <<
                    variable_table.pprint(rep) << std::endl;
            }

            // is the function application assigned to something in the trail?
            if ( trail.has_value(fun_app) ) {
                Variable val_rep = get_val_of_rep(rep);
                if ( val_rep == NULL_VAR ) {
                    // if we haven't a value assigned to the representat, se the
                    // current assignment to it
                    set_val_of_rep(rep,fun_app);
                }
                else {
                    // check that the two assignemnt are consistent, otherwise
                    // we are in conflict
                    if ( trail.get_value(val_rep) != trail.get_value(fun_app) ) {
                        if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                            debug << "UF: conflict! inconsistent function application"
                                << std::endl;
                        }
                        build_fun_app_conflict(fun_app, val_rep);
                        report_conflict();
                    }
                }
            }
        }
    }

    // if the var to propagate is a function application per se, check if
    // it had a value.
    Term var_term = variable_table.get_term(var);
    if ( term_table.kind_of(var_term) == TERM_FUN_APPLICATION ) {

        Variable rep = rep_table.get_representant(var);

        // is fully assigned?
        if ( rep != NULL_VAR ) {

            Variable val_rep = get_val_of_rep(rep);
            if ( val_rep == NULL_VAR ) {
                // if we haven't a value assigned to the representat, set the
                // current assignment to it
                set_val_of_rep(rep,var);
            }
            else {
                // check that the two assignemnt are consistent, otherwise
                // we are in conflict
                if ( trail.get_value(val_rep) != trail.get_value(var) ) {
                    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                        debug << "UF: conflict! inconsistent function application 2"
                            << std::endl;
                    }
                    build_fun_app_conflict(var, val_rep);
                    report_conflict();
                }
            }
        }
    }
}

/*
 * the congruence closure rule says that
 * x1 = y1 ^ .... ^ xn = yn => f(x1 ... xn) = f(y1 ... yn)
 * so the new lemma must be
 * ~(x1 = y1) v .... v ~(xn = yn) v f(x1 ... xn) = f(y1 ... yn)
 * and the reason of the conflict is
 * x1 = y1 ^ .... ^ xn = yn ^ ~(f(x1 ... xn) = f(y1 ... yn))
 */
void UFModule::build_fun_app_conflict(Variable l, Variable r) {
    conflict_reason.clear();

    // the function have different value for the same arguments
    assert(trail.has_value(l));
    assert(trail.has_value(r));
    assert(trail.get_value(l) != trail.get_value(r));

    Term left  = variable_table.get_term(l);
    Term right = variable_table.get_term(r);
    assert(left != right);

    // if boolean, directly encode the equality
    assert(term_table.sort_of(left) == term_table.sort_of(right));
    if ( term_table.sort_of(left) == bool_sort ) {
        conflict_reason.push_back(l);
        conflict_reason.push_back(r);
    }
    // otherwise encode with an equality
    else {
        Term new_eq = term_table.add_binary_equality(left,right);
        Variable eq_var = variable_table.get_variable(new_eq);
        // the equality is false in the conflict
        conflict_reason.push_back(eq_var);

        if ( ! trail.has_value(eq_var) ) {
            // assign the equality on the trail in the proper level
            // to create the conflict
            size_t level = std::max(trail.get_level(l), trail.get_level(r));

            //size_t level = 0;
            trail.add_propagation(eq_var, false, get_id(), level);
        }
    }

    // now set the equality between the various arguments

    const auto &comp_left  = term_table.get_composite_args(left);
    const auto &comp_right = term_table.get_composite_args(right);
    assert(comp_left.size() == comp_right.size());
    assert(comp_left[comp_left.size()-1] == comp_right[comp_right.size()-1]);

    // for every arguments
    for (size_t i = 0; i < comp_left.size()-1; ++i) {
        Term t1 = comp_left[i];
        Term t2 = comp_right[i];
        Variable v1 = variable_table.get_variable(t1);
        Variable v2 = variable_table.get_variable(t2);

        // they have the same value
        assert(trail.has_value(v1));
        assert(trail.has_value(v2));
        assert(trail.get_value(v1) == trail.get_value(v2));

        assert(term_table.sort_of(t1) == term_table.sort_of(t2));
        if ( term_table.sort_of(t1) == bool_sort ) {
            conflict_reason.push_back(v1);
            conflict_reason.push_back(v2);
        }
        // otherwise encode with an equality
        else {
            Term new_eq = term_table.add_binary_equality(t1,t2);
            // it is possible that the two application have an argument
            // that is exactly the same, don't add it to the conflict
            // because it is trivial
            if ( new_eq != true_term ) {
                Variable eq_var = variable_table.get_variable(new_eq);
                conflict_reason.push_back(eq_var);

                if ( ! trail.has_value(eq_var) ) {
                    // assign the equality on the trail in the proper level
                    // to create the conflict
                    size_t level = std::max(trail.get_level(v1), trail.get_level(v2));

                    //size_t level = 0;
                    trail.add_propagation(eq_var, true, get_id(), level);
                }
            }
        }
    }
}

void UFModule::propagate_eq(Variable var) {
    // get the variable to propagate
    to_move_eq.clear();

    auto it = eq_watchs.find(var);
    if ( it == eq_watchs.end() )
        return; // nothing to move

    std::swap(it->second, to_move_eq);

    if ( MESSAGE_LEVEL( VERBOUSE_MESSAGE ) ) {
        debug << "UF: propagate variable: " << var << " value " <<
            trail.get_value(var) << std::endl;

        debug << "UF: search new watch in clause:" << std::endl;
        for ( const auto &c : to_move_eq)
            debug << "\t" << *c << std::endl;
    }

    for (auto it = to_move_eq.begin(); it != to_move_eq.end(); ++it) {

        if ( ! trail.is_consistent() ) {
            // conflict during the propagation, reset the list
            eq_watchs[var].insert(eq_watchs[var].begin(), it, to_move_eq.end()); 
            // exit
            break;
        }

        EqClause & clause = **it;
        
        // get the equality from the clause
        Variable eq_var;
        if (term_table.kind_of(variable_table.get_term(clause[0])) == TERM_EQ)
            eq_var = clause[0];
        else if (term_table.kind_of(variable_table.get_term(clause[1])) == TERM_EQ)
            eq_var = clause[1];
        else
            eq_var = clause[2];
        assert(term_table.kind_of(variable_table.get_term(eq_var)) == TERM_EQ);

        // normalize assigned var in position 1
        if ( clause[0] == var ) {
            clause[0] = clause[1];
            clause[1] = var;
        }
        assert(clause[1] == var);

        if ( ! trail.has_value(clause[2]) ) {
            // found new watch
            clause[1] = clause[2];
            clause[2] = var;
            eq_watchs[clause[1]].push_back(*it);
            continue;
        }

        // it must be a unit or a fully assigned clause

        // reinsert in the previous watch list, we don't move anything
        eq_watchs[var].push_back(*it);

        // if clause[0] is a relation
        if ( clause[0] == eq_var ) {
            // the two component of the equality are assigned
            Variable left  = clause[1];
            Variable right = clause[2];

            // get theire values
            assert(trail.has_value(left));
            assert(trail.has_value(right));
            Value val_left  = trail.get_value(left);
            Value val_right = trail.get_value(right);

            // if the equality is aready set check that the assignment is consistent
            if ( trail.has_value( eq_var ) ) {
                if (trail.get_boolean_value(eq_var) != (val_left == val_right)) {
                    if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                        debug << "UF: conflict! incompatible equality" << std::endl;
                    }
                    conflict_reason.clear();
                    conflict_reason.push_back(eq_var);
                    conflict_reason.push_back(left);
                    conflict_reason.push_back(right);
                    report_conflict();
                }
            }
            else {
                // else propagate the equalities
                auto level = std::max(trail.get_level(left), trail.get_level(right));
                bool val = trail.get_value(left) == trail.get_value(right);
                trail.add_propagation(eq_var, val, get_id(), level);
            }
        }
        // if the clause[0] is an uninterpreted term
        else {
            // the two component of the equality are assigned
            Variable left  = clause[0];
            Variable right = clause[1] == eq_var ? clause[2] : clause[1];

            // get the values
            assert(trail.has_value(right));
            assert(trail.has_value(eq_var));
            Value val = trail.get_value(right);
            bool is_eq = trail.get_boolean_value(eq_var);

            bool conflict = false;
            if ( is_eq ) {
                // set equality in feasible set, return true if is not
                // a feasible equality
                conflict = feasible_set.set_equality(left,val,eq_var);
            }
            else {
                // set equality in feasible set, return ture if is an
                // incompatible disequality
                conflict = feasible_set.set_disequality(left,val,eq_var);
            }

            if ( conflict ) {
                if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                    debug << "UF: conflict! failed transitivity" << std::endl;
                }
                conflict_reason.clear();
                feasible_set.build_conflict_reason(left, conflict_reason);

                assert( conflict_reason.size() == 3 );
                // add a propagation if a member is not assigned
                if ( ! trail.has_value(conflict_reason[2]) ) {
                    // assign the equality on the trail in the proper level
                    // to create the conflict
                    bool val_a = trail.get_boolean_value(conflict_reason[0]);
                    bool val_b = trail.get_boolean_value(conflict_reason[1]);

                    Term t = variable_table.get_term(conflict_reason[2]);
                    const auto &comp = term_table.get_composite_args(t);
                    size_t level = std::max(
                            trail.get_level(variable_table.get_variable(comp[0])),
                            trail.get_level(variable_table.get_variable(comp[1])));

                    //size_t level = 0;
                    trail.add_propagation(conflict_reason[2]
                            , val_a != val_b, get_id(), level);
                }

                report_conflict();
            }
        }
    }
}

void UFModule::make_decision(Variable var) {
    auto val = feasible_set.get_feasible_value(var);
    trail.add_decision(var, Value(val), get_id());
}

void UFModule::get_conflict(std::vector<Variable>& reason) {
    reason.clear();
    std::move(conflict_reason.begin(), conflict_reason.end(),
            std::back_inserter(reason));
}

void UFModule::get_justification(Variable var, std::vector<Variable> &reason) {
    Term t = variable_table.get_term(var);
    const auto &comp = term_table.get_composite_args(t);
    reason.push_back(variable_table.get_variable(comp[0]));
    reason.push_back(variable_table.get_variable(comp[1]));
}

void UFModule::push() {

    // save data on stack
    propagation_pos_stack.push_back(propagation_starting_pos);
    app_size_stack.push_back(all_application.size());
    assigned_rep_stack.push_back(assigned_rep.size());

    // push in other components
    rep_table.push();
    feasible_set.push();
}

void UFModule::pop() {
    // undo position in trail
    propagation_starting_pos = propagation_pos_stack.back();
    propagation_pos_stack.pop_back();

    // undo registered application
    assert(!app_size_stack.empty());
    size_t old_app_size = app_size_stack.back();
    app_size_stack.pop_back();
    while ( all_application.size() > old_app_size )
        all_application.pop_back();


    // undo value assignemnt to representant
    assert(!assigned_rep_stack.empty());
    size_t old_assigned_rep = assigned_rep_stack.back();
    assigned_rep_stack.pop_back();
    while ( assigned_rep.size() > old_assigned_rep ) {
        Variable var = assigned_rep.back();
        assigned_rep.pop_back();
        auto it = rep_to_val.find( var );
        assert ( it != rep_to_val.end() );
        rep_to_val.erase(it);
    }

    // pop in other components
    rep_table.pop();
    feasible_set.pop();
}

void UFModule::report_conflict() {
    trail.set_inconsistent(true);
    main_solver.report_conflict(get_id());
}

Variable UFModule::get_val_of_rep(Variable var) {
    auto it = rep_to_val.find(var);
    if ( it != rep_to_val.end() )
        return it->second;
    else
        return NULL_VAR;
}

void UFModule::set_val_of_rep(Variable rep, Variable rep_val) {
    assert( get_val_of_rep(rep) == NULL_VAR );
    rep_to_val[rep] = rep_val;
    assigned_rep.push_back(rep);
}
