/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "feasible_set.h"
#include <algorithm>
#include <printing.h>

// a feasible set use the trail to identify the current assignments
FeasibleSet::FeasibleSet(const Trail &t, TermTable &tt,
        SortTable &st, VariableTable &vt):
    main_trail(t),
    term_table(tt),
    sort_table(st),
    variable_table(vt)
{}

// Get the index of the last known equality about a variable.
// Return NO_POS if we haven't any info on the variable
uint32_t FeasibleSet::pos_in_trail(Variable var) {
    auto it = last_equality.find(var);
    if ( it == last_equality.end() )
        return NO_POS;
    else
        return it->second;
}

// return a feasible value for the variable var
UninterpretedValue FeasibleSet::get_feasible_value( Variable var ) {

    // sort of the uninterpreted values
    Sort sort = term_table.sort_of(variable_table.get_term(var));
    assert( sort_table.kind_of(sort) == UNINTERPRETED_SORT );

    auto pos = pos_in_trail(var);

    // we know nothing about the value, we can choose everything
    if ( pos == NO_POS )
        return UninterpretedValue(sort,0); // minimum

    // last equality set for the var
    auto eq = equality_trail[pos];

    // if already equal to something return the value.
    // equality are always in the first position
    if ( eq.is_eq )
        return eq.val.get_uninterpreted();

    // all the known equality should be inequality, save all of them
    std::vector<UninterpretedValue> tmp;
    do {
        eq = equality_trail[pos];
        assert(!eq.is_eq);
        assert(eq.val.is_uninterpreted());
        tmp.push_back( eq.val.get_uninterpreted() );
        pos = old_pos[pos];
    } while ( pos != NO_POS );

    // order the sort by ascending ids (all the sort are equal)
    std::sort(tmp.begin(), tmp.end(),
            [](const UninterpretedValue &x, const UninterpretedValue &y) {
            return x.get_id() < y.get_id(); });

    // use the minimum id possible
    uint32_t min_value = 0;
    for ( const auto &val : tmp) {
        if ( val.get_id() != min_value ) break;
        ++min_value;
    }

    // return a proper uninterpreted value
    return UninterpretedValue(sort, min_value);
}

// set disequality between a variable and a given value.
// it return true iff it found a conflict, otherwise it return false.
// A conflict can happen if a variable is set to be equal and disequal
// to the same value.
bool FeasibleSet::set_disequality( Variable var, Value val, Variable reason ) {

    auto pos = pos_in_trail(var);
    bool found_a_conflict = false;

    while ( pos != NO_POS ) {
        // get the last relation for the variable
        const auto &eq = equality_trail[pos];

        // if is an equality the value must be different from the value
        // of the disequality, otherwise a conflict arise.
        // if the value is different don't add anything, it is not required.
        // This also keep the equality in the first position
        if ( eq.is_eq ) {
            if ( eq.val == val ) {
                found_a_conflict =  true;
                break;
            }
            else 
                return false; // no conflict
        }
        // check if we already known about the disequality
        else if ( eq.val == val )
            return false; // no conflict, disequality already set

        pos = old_pos[pos];
    }

    // set the disequality, even if we are in conflict (we need it to
    // build a conflict clause)
    set_new_element(var,false,val,reason);

    return found_a_conflict;
}

// set equality between a variable and a given value.
// it return true iff it found a conflict, otherwise it return false.
// A conflict can happen if a variable is set to be equal and disequal
// to the same value.
bool FeasibleSet::set_equality( Variable var, Value val, Variable reason ) {

    bool found_a_conflict = false;

    auto pos = pos_in_trail(var);
    while ( pos != NO_POS ) {
        // get the last relation for the variable
        const auto &eq = equality_trail[pos];

        // if is an equality the value must be the same, otherwise a we are
        // in conflict.
        // if a disequality have the same value as the equality we are also
        // in fonclict
        if ( eq.is_eq ) {
            if ( eq.val != val ) {
                found_a_conflict = true;
                break;
            }
            else 
                return false; // no conflict
        }
        // compare with disequality
        else if ( eq.val == val ) {
            found_a_conflict = true;
            break;
        }

        pos = old_pos[pos];
    }

    // set the equality
    set_new_element(var,true,val,reason);

    return found_a_conflict;
}

// add a new equality_trail to an equality set
void FeasibleSet::set_new_element(
        Variable var, bool is_eq, Value val, Variable reason) {
    // compute position
    uint32_t old_i = pos_in_trail(var);
    uint32_t new_i = equality_trail.size();

    // build equality 'var ~ val'
    equality_trail.push_back({var, is_eq, val, reason}); 

    // update position
    old_pos.push_back(old_i);
    last_equality[var] = new_i;
}

// backtracking support
void FeasibleSet::push() {
    trail_stack.push_back(equality_trail.size());
}

void FeasibleSet::pop() {
    size_t old_size;
    old_size = trail_stack.back(); trail_stack.pop_back();

    // undo equality and disequality of this level
    while ( equality_trail.size() > old_size ) {
        // recompute position
        const auto &eq = equality_trail.back();
        last_equality[eq.var] = old_pos.back();

        // pop the value
        old_pos.pop_back();
        equality_trail.pop_back();
    }
}

// reason for equality
Variable FeasibleSet::get_equality_reason(Variable var) {
    auto pos = pos_in_trail(var);

    // iterate all the known relation about var until an equality is foundt
    while ( pos != NO_POS ) {
        const auto &eq = equality_trail[pos];
        if ( eq.is_eq )
            return eq.reason;
        pos = old_pos[pos];
    }

    // we must find a reason
    assert(false);
    return NULL_VAR;
}

// build a conflict
void FeasibleSet::build_conflict_reason(Variable var, std::vector<Variable> &reason) {
    auto pos = pos_in_trail(var);
    // we must have some equalities in a conflict 
    assert( pos != NO_POS ); 

    // remember the first one
    const auto &first = equality_trail[pos];

    // go back
    pos = old_pos[pos];

     do {
        const auto &current = equality_trail[pos];

        // FIRST CASE: we have two equalities for different values.
        // so first should be var == a, current var == b but a != b
        // the reason of the conflict should be: [ var==a, var==b, a!=b ]
        if ( first.is_eq && current.is_eq ) {
            if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                debug << "UF: conflict1" << std::endl;
            }
            // it should be impossible to have duplicates
            assert( first.val != current.val );

            // we have asserted the two incompatible reason at true
            assert(main_trail.get_boolean_value(first.reason));
            assert(main_trail.get_boolean_value(current.reason));

            build_conflict(first,current,reason);
            return;
        }

        // SECOND CASE: we have that var == a and var != b, but a == b
        // the reason of the conflict should be: [ var==a, var!=b, a==b ]
        if (first.is_eq && !current.is_eq && first.val == current.val) {
            if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                debug << "UF: conflict2" << std::endl;
            }
            assert( main_trail.get_boolean_value(first.reason));
            assert( ! main_trail.get_boolean_value(current.reason));

            build_conflict(first,current,reason);
            return;
        }

        // THIRD CASE: is the mirrored case of the second.
        // the reason of the conflict should be: [ var!=a, var==b, a==b ]
        if ( ! first.is_eq && current.is_eq ) {
            if (MESSAGE_LEVEL(STANDARD_MESSAGE)) {
                debug << "UF: conflict3" << std::endl;
            }
            // if the last known relation is not an equality, the equality
            // is just before the last one
            assert( first.val == current.val );
            assert( ! main_trail.get_boolean_value(first.reason));
            assert( main_trail.get_boolean_value(current.reason));

            build_conflict(first,current,reason);
            return;
        }
        pos = old_pos[pos];
    } while ( pos != NO_POS );

    // we must have foundt a reason for the conflict
    assert(false);
}

// we have var == a, var == b, a == b but one of the three is falsified in
// the current assignment, we have to build a reason for the conflict that
// countains two equality and one disequality
void FeasibleSet::build_conflict(const Equality &first, const Equality &second,
        std::vector<Variable> &reason) {
    // incompatible value for the same variable
    assert( first.var == second.var );

    // get the equality term
    assert( main_trail.has_value(first.reason) );
    Term var_eq_a = variable_table.get_term(first.reason);
    assert( main_trail.has_value(second.reason) );
    Term var_eq_b = variable_table.get_term(second.reason);
    assert(term_table.kind_of(var_eq_a) == TERM_EQ);
    assert(term_table.kind_of(var_eq_b) == TERM_EQ);

    // get the member of the relation
    Term var_term = variable_table.get_term(first.var);

    const auto &comp_a = term_table.get_composite_args(var_eq_a);
    Term a_term = comp_a[0] == var_term ? comp_a[1] : comp_a[0];

    const auto &comp_b = term_table.get_composite_args(var_eq_b);
    Term b_term = comp_b[0] == var_term ? comp_b[1] : comp_b[0];

    // build the last equality
    Term a_eq_b = term_table.add_binary_equality(a_term,b_term);
    Variable eq_var = variable_table.get_variable(a_eq_b);

    reason.push_back( first.reason );
    reason.push_back( second.reason );
    reason.push_back( eq_var );
}

