/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_UF_REPRESENTANT_HH
#define EOS_UF_REPRESENTANT_HH

#include "term.h"
#include "variable.h"
#include "trail.h"

#include <unordered_map>

/*
 * equality between fully assigned function application
 */
class AppEquality {
    const Trail &trail;
    TermTable &term_table;
    VariableTable &variable_table;
public:

    AppEquality(const Trail &t, TermTable &tt, VariableTable &vt):
        trail(t), term_table(tt), variable_table(vt) {}

    bool operator() (const Variable &v1, const Variable &v2) const;
};

/*
 * hash of a fully assigned function application
 */
class AppHasher {
    const Trail &trail;
    TermTable &term_table;
    VariableTable &variable_table;
public:

    AppHasher(const Trail &t, TermTable &tt, VariableTable &vt):
        trail(t), term_table(tt), variable_table(vt) {}

    std::size_t operator() (const Variable &v) const;
};

/*
 * This table maps every fully assigned function application to their
 * representant. Used to implement the congruence closure.
 */
class RepresentantTable {
public:
    RepresentantTable(const Trail&t, TermTable &tt,  VariableTable &vt);

    // return the representant of a fully assigned function application.
    // if the variable is not fully assigned return NULL_VAR.
    // If the variable doesn't have a representant, set it to be the
    // new representant of its congruence class.
    Variable get_representant(Variable var);

    // return true iff the variable is a fully assigned function application
    // in the current trail
    bool is_fully_assigned(Variable var);

    // backtrack support
    void push();
    void pop();

private:
    // usefull references
    const Trail &trail;
    TermTable &term_table;
    VariableTable &variable_table;

    // mapping from variable to representant
    std::unordered_map<Variable, Variable, AppHasher, AppEquality> rep_map;

    // trail of representant assignment for backtracking
    std::vector<Variable> rep_trail;
    // stack for backtracking
    std::vector<std::vector<Variable>::size_type> size_stack;
};

#endif // EOS_UF_REPRESENTANT_HH
