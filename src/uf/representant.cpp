/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "representant.h"
#include "printing.h"

/*
 * compare fully assigned function aplication.
 * two function application are equal iff:
 * - the are an application of the same function (with the same arity)
 * - all their argument evaluate to the same value
 */
bool AppEquality::operator() (const Variable &v1, const Variable &v2) const {
    // get term
    Term t1 = variable_table.get_term(v1);
    assert( term_table.kind_of(t1) == TERM_FUN_APPLICATION );
    Term t2 = variable_table.get_term(v2);
    assert( term_table.kind_of(t2) == TERM_FUN_APPLICATION );

    const auto &comp1 = term_table.get_composite_args(t1);
    const auto &comp2 = term_table.get_composite_args(t2);

    // the two must be the same function with the same number of arguments
    if ( comp1.size() != comp2.size() ||
            comp1[comp1.size()-1] != comp2[comp2.size()-1] )
        return false;

    // check the arguments
    for ( size_t i = 0; i < comp1.size()-1; ++i) {
        Variable arg1 = variable_table.get_variable(comp1[i].unsign());
        Variable arg2 = variable_table.get_variable(comp2[i].unsign());

        assert( trail.has_value(arg1) );
        assert( trail.has_value(arg2) );

        // different valus
        if ( trail.get_value(arg1) != trail.get_value(arg2) )
            return false;
    }

    return true;
}

/*
 * hashing of a fully assigned function application
 * the hash considere the value of all the arguments
 */
std::size_t AppHasher::operator() (const Variable &v) const {
    // get term
    Term t = variable_table.get_term(v);
    assert( term_table.kind_of(t) == TERM_FUN_APPLICATION );

    const auto &comp = term_table.get_composite_args(t);

    // based of boost hash combiner
    static const size_t hash_combiner = 0x9e3779b9;
    size_t seed = comp[comp.size()-1].hash() << 16;

    // check the arguments
    for ( size_t i = 0; i < comp.size()-1; ++i) {
        Variable arg = variable_table.get_variable(comp[i].unsign());
        assert( trail.has_value(arg) );
        const Value &val = trail.get_value(arg);
        seed ^= val.hash() + hash_combiner + (seed << 6) + (seed >> 2);
    }

    return seed;
}

RepresentantTable::RepresentantTable(
        const Trail&t, TermTable &tt,  VariableTable &vt):
    trail(t),
    term_table(tt),
    variable_table(vt),
    rep_map(16,AppHasher(t,tt,vt),AppEquality(t,tt,vt))
{}

// return the representant of a fully assigned function application.
// if the variable is not fully assigned return NULL_VAR.
// If the variable doesn't have a representant, set it to be the
// new representant of its congruence class.
Variable RepresentantTable::get_representant(Variable var) {

    if (! is_fully_assigned(var) )
        return NULL_VAR;

    auto it = rep_map.find(var);
    if ( it == rep_map.end() ) {
        rep_trail.push_back(var);
        rep_map[var] = var;
        return var;
    }
    else
        return it->second;
}

// save the number of assigned representant
void RepresentantTable::push() {
    size_stack.push_back(rep_trail.size());
}

// undo the representant of the last level
void RepresentantTable::pop() {
    auto old_size = size_stack.back(); size_stack.pop_back();

    while ( rep_trail.size() > old_size ) {
        auto var = rep_trail.back(); rep_trail.pop_back();
        assert( rep_map.find(var) != rep_map.end() );
        rep_map.erase(rep_map.find(var));
    }
}

// return true iff the variable is a fully assigned function application
// in the current trail
bool RepresentantTable::is_fully_assigned(Variable var) {
    Term t = variable_table.get_term(var);
    assert( term_table.kind_of(t) == TERM_FUN_APPLICATION );

    const auto &comp = term_table.get_composite_args(t);

    // check if one of the argument are not assigned
    for ( size_t i = 0; i < comp.size()-1; ++i) {
        if ( ! trail.has_value( variable_table.get_variable(comp[i].unsign())) ) {
            return false;
        }
    }

    return true;
}

