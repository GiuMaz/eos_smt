/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_UF_UF_MODULE_HH
#define EOS_UF_UF_MODULE_HH

#include "sort.h"
#include "term.h"
#include "variable.h"
#include "theory_module.h"

#include "feasible_set.h"
#include "representant.h"
#include "equality_clause.h"
#include "compact_vector.h"

class UFModule : public TheoryModule {
public:

    UFModule(SMTSolver &solver);
    ~UFModule() = default;

    void new_term_notify( Term t ) override;

    void propagate() override;

    void push() override;

    void pop() override;

    void make_decision(Variable) override;

    void get_conflict(std::vector<Variable>&) override;

    void get_justification(Variable, std::vector<Variable> &) override;

private:

    using EqClause = compact_vector<Variable>;
    using EqClausePtr = EqClause *;

    // store the funtion application in the header, the argument
    // in the list
    using AppClause = compact_vector_with_header<Variable,Variable>;
    using AppClausePtr = AppClause *;

    // report to the main solver that there is a conflict
    void report_conflict();

    // build new module component
    void new_fun_app(Term t);
    void new_equality(Term t);

    // propagate component
    void propagate_fun_app(Variable var);
    void propagate_eq(Variable var);

    // build a conflict in function application
    void build_fun_app_conflict(Variable l, Variable r);

    Variable get_val_of_rep(Variable var);
    void set_val_of_rep(Variable rep, Variable rep_val);

    // representant table for function application
    RepresentantTable rep_table;

    // feasible set for uninterpreted term
    FeasibleSet feasible_set;

    // store the current conclict clause
    std::vector<Variable> conflict_reason;

    // mapping from fully assigned function application to a variable
    // that represent the value
    std::unordered_map<Variable,Variable> rep_to_val;

    // equality clauses and their watch list
    std::vector<EqClausePtr> eq_clauses;
    std::unordered_map<Variable, std::vector<EqClausePtr>> eq_watchs;

    // application clauses and their watch list
    std::vector<AppClausePtr> app_clauses;
    std::unordered_map<Variable, std::vector<AppClausePtr>> app_watchs;

    // keep track of last position seen in trail by this module
    size_t propagation_starting_pos = 0;

    // backtrack stacks
    std::vector<size_t> propagation_pos_stack;
    std::vector<size_t> app_size_stack;
    std::vector<size_t> assigned_rep_stack;

    // caches and tmps
    std::vector<Variable> all_variables;
    std::vector<Variable> all_application;
    std::vector<Variable> assigned_rep;
    std::vector<EqClausePtr>  to_move_eq;
    std::vector<AppClausePtr> to_move_app;

};

#endif // EOS_UF_UF_MODULE_HH
