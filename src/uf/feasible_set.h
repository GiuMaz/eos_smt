/*
 * This file is part of the Eos SMT Solver.
 * Copyright (C) 2018 Giulio Mazzi
 *
 * Eos is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Eos is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Eos.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EOS_UF_FEASIBLE_SET_HH
#define EOS_UF_FEASIBLE_SET_HH

#include "variable.h"
#include "value.h"
#include "term.h"
#include "value.h"
#include "trail.h"

#include <unordered_map>
#include <vector>

/*
 * A feasible set for uninterpreted variables store equalities and disequalities
 * asserted or deduced on all the uninterpreted term.
 * It is possible to get an assignable values for them, all this values are
 * supposed to come from an countable infinite set.
 *
 * If some incompatible equalities or disequalities are asserted the class
 * can identify the inconsistncy, and it can also build the reason for this
 * conflict.
 */
class FeasibleSet {

public:

    // a feasible set use the trail to identify the current assignments
    FeasibleSet(const Trail &, TermTable &, SortTable &, VariableTable &);

    // return a feasible value for the variable var
    UninterpretedValue get_feasible_value( Variable var );

    // set disequality between a variable and a given value.
    // it return true iff it found a conflict, otherwise it return false.
    // A conflict can happen if a variable is set to be equal and disequal
    // to the same value.
    bool set_disequality( Variable var, Value val, Variable reason );

    // set equality between a variable and a given value.
    // it return true iff it found a conflict, otherwise it return false.
    // A conflict can happen if a variable is set to be equal and disequal
    // to the same value.
    bool set_equality( Variable var, Value val, Variable reason );

    // reason for equality
    Variable get_equality_reason(Variable var);

    // build a conflict
    void build_conflict_reason(Variable var, std::vector<Variable> &reason);

    // backtracking support
    void push();
    void pop();

private:

    // identify an equality in the trail in the form:
    // var ~ val, where ~ can be == or !=
    // this also store a reason for the relation.
    struct Equality {
        // variable
        Variable var;
        // true if is an equality, false if is a disequality
        bool is_eq;
        // equal or disequal to this value
        Value val;
        // reason for the relation
        Variable reason;
    };

    // get the offset in the equality trail, if the variable have no known
    // equality this return NO_POS
    uint32_t pos_in_trail(Variable var);

    // add a new element to the equality trail
    void set_new_element(Variable var, bool is_eq, Value val, Variable reason);

    // build a conflict from two incompatible equalies
    void build_conflict(const Equality &, const Equality &,
            std::vector<Variable> &);

    // usefull reference
    const Trail &main_trail;
    TermTable &term_table;
    SortTable &sort_table;
    VariableTable &variable_table;

    static const uint32_t NO_POS = UINT32_MAX;

    // trail of equality
    std::vector<Equality> equality_trail;

    // if the feasible set known an equality or a disequality about
    // a variable this map store the position of the last relation about it
    std::unordered_map<Variable,uint32_t> last_equality;
    // remember the position of the last known relation about var,
    // is set to NO_POS if we haven't any previous information
    std::vector<uint32_t> old_pos;

    // remember the size of the equality trail between push and pop
    std::vector<size_t> trail_stack;
};

#endif // EOS_UF_FEASIBLE_SET_HH
