(set-logic QF_UF)
(set-option :produce-models true)
(declare-sort U 0)
(declare-const x U)
(declare-const y U)

(assert (distinct x y))

(check-sat)
(get-model)


