(set-logic QF_UF)
(declare-sort U 0)
(declare-const x U)
(declare-fun f (U) Bool)

(declare-sort W 0)
(declare-const y W)
(declare-fun g (W) Bool)

(assert (f x));
(assert (g y));
(check-sat)
(get-model)
