(set-logic QF_UF)
(set-option :produce-models true)
(declare-const x Real)
(declare-const y Real)
(declare-const z Real)

(assert (<= (+ x y z) 5))
(assert (< y z))
(assert (< (* 2 y) (* (/ 18 9) z))

(check-sat)
(get-model)

