(set-logic QF_UF)

(declare-sort U 0)

(declare-const x U)
(declare-fun f (U) U)

(assert (= (f x) x))
(assert (distinct (f (f (f x))) x))

(check-sat)
(get-model)

