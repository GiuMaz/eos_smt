(set-logic QF_UF)
(set-option :produce-models true)
(declare-const x Real)
(declare-const y Real)
(declare-const z Real)

(assert (= y (+ x 7)))
(assert (distinct y (- z x 7)))

(check-sat)
(get-model)

