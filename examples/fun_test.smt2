(set-option :produce-models true)
(set-logic QF_UF)

; declare bolean literal
(declare-const x Bool)
(declare-const y Bool)
(declare-fun   f (Bool)  Bool)

(assert (f (and x y) ) )
(assert (and x y))

(check-sat)
(get-model)
(exit)


