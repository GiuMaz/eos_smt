(set-logic QF_UF)
(set-option :produce-models true)
(declare-const x Real)
(declare-const y Real)
(declare-const z Real)

(assert (= (+ (* (/ 1 4) x) (* (/ 7 3) y) (* z (- 5))) (/ 5 2) ))
(assert (= (+  (* (/ 7 5) y) (* z (/ (- 4) 39))) 7 ))
(assert (= (+  (* z (/ 5 2))) (- 1) ))

(check-sat)
(get-model)

