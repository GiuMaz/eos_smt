; check local scope of let
(set-logic QF_UF)

(declare-sort U 0)
(declare-fun f (U) Bool)

; declare x of sort U
(declare-const x U)

; shadow x with a local (boolean) value for x
(assert (let ( (x true) (y (=> true false)) ) (or x y)))

; after the let, x return to be a constant of sort U
(assert (= (f x) true))

(check-sat)
(exit)
