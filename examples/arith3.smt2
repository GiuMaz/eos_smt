(set-option :produce-models true)
(set-logic QF_UF)
(declare-const x Real)
(declare-const y Real)
(declare-const z Real)

; ((1/4)*x)+((7/3)*y)+(-5*z) = 5/2 
; (-(8/3)*x)+((7/5)*y)+(-(4/39)*z) = 7 
; (13*x)+((7/3)*y)+((5/2)*z) = -1
(assert (= (+ (* (/ 1 4) x) (* (/ 7 3) y) (* z (- 5))) (/ 5 2) ))
(assert (= (+ (* (- (/ 8 3)) x) (* (/ 7 5) y) (* z (/ (- 4) 39))) 7 ))
(assert (= (+ (* 13 x) (* (/ 7 3) y) (* z (/ 5 2))) (- 1) ))


(check-sat)
(get-model)

