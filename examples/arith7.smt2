(set-logic QF_LRA)

(declare-fun a () Real)
(declare-fun b () Real)
(declare-fun c () Real)
(declare-fun d () Real)

(assert (and (= a b) (<= a 0) (<= c 0) (<= d 0)))
(check-sat)
(exit)
