(set-logic QF_UF)

; declare bolean literal
(declare-const x Bool)
(declare-const y Bool)
(declare-fun   z ()  Bool)

; or of a list of varibles
(assert (or x y z))

; it is also possible to use true and false constants
(assert (or x y z true false))

; x and y and z
(assert (and x y z))

; not x
(assert (not x))

; implication (right associative, so is the same as x => (y => z) )
(assert (=> x y z))

; xor
(assert (xor x y z))

; equality of boolean value is the same as an iff
(assert (= x y))
; mulitple equality is concatenated
(assert (= x y z)); ~> (and (= x y) (= y z))

; distinct of two boolean value is the same as a xor
(assert (distinct x y ))
; distinct of multiple value is different from a xor of multiple values,
; is the and of all the possible pair of distinct
(assert (distinct x y z)) ; ~> (and (distinct x y) (distinct x z) (distinct y z))

; if-then-else, if x is true use the value of y, otherwise use the value of z
(assert (ite x y z))

(check-sat)
(exit)
