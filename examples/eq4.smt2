(set-logic QF_UF)
(set-option :produce-models true)
(declare-sort U 0)
(declare-const a Bool)
(declare-const b Bool)
(declare-const x U)
(declare-const y U)
(declare-const z U)

(assert (xor a b))
(assert (or a (= x y)))
(assert (= y z))
(assert (distinct z x))

(check-sat)
(get-model)

