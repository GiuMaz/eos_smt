(set-logic QF_UF)

(declare-sort U 0)

(declare-const x U)
(declare-const y U)
(declare-const z U)
(declare-const w U)

(assert (= x y z w))
(assert (distinct w x))

(check-sat)

