(set-option :produce-models true)
(set-logic QF_UF)

(declare-const a Bool)
(declare-const b Bool)
(declare-const x Real)

(assert (= (ite (ite a b false) (ite (not a) x 3) 5) 4))

(check-sat)
