(set-logic QF_UF)
(declare-const x Bool)
(declare-const y Bool)

(assert (or x y))
(assert (or (not x) y))
(assert (or x (not y)))
(assert (or (not x) (not y)))

(check-sat)

