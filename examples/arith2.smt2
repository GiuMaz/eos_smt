(set-logic QF_UF)
(set-option :produce-models true)
(declare-const x Real)

(assert (= (* (/ 1 4) x) (+ (* (/ 2 16) x) (* x (/ 1 8)) )) )

(check-sat)
(get-model)

