(set-logic QF_UF)
(set-option :produce-models true)
(declare-sort U 0)
(declare-const x U)
(declare-const y U)
(declare-const z U)

(assert (or (= x y) (= y z) (= x z)))

(check-sat)
(get-model)

