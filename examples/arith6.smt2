(set-logic QF_LRA)

(declare-fun x () Real)
(declare-fun y () Real)
(declare-fun z () Real)

(assert (> y (/ 1 2)))
(assert (>= z 0))
(assert (and (= z 0) (< (+ y z x) 0)))

(check-sat)
(get-model)
(exit)
