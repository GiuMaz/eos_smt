(set-logic QF_UF)
(set-option :produce-models true)
(declare-const x Bool)
(declare-const y Bool)
(declare-const z Bool)

(assert (or x y z))
(assert (or (not x) y z))
(assert (or x (not y) z))
(assert (or (not x) (not y) z))
(assert (or x y (not z)))
(assert (or (not x) y (not z)))
(assert (or x (not y) (not z)))
;(assert (or (not x) (not y) (not z)))

(check-sat)
(get-model)

