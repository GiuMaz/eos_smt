; testing of sort declaration
(set-option :produce-models true)
(set-logic QF_UF)

(declare-sort U 0)
(declare-sort V 0)
(declare-sort W 0)

(declare-fun f  (U) Bool)
(declare-fun g1 (V V V) W)
(declare-fun g2 (V V V) W)
(declare-fun h  (Bool Bool) W )

; declare x of sort U
(declare-const x U)

(declare-const a V)
(declare-const b V)
(declare-const c V)

(assert (= (f x) true))
(assert (= (g1 a b c) (h true false))) 
(assert (= (g2 a a a) (h true false))) 
(assert (distinct a b c))

(check-sat)
(get-model)
(exit)
