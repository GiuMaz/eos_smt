(set-option :produce-models true)
(set-logic QF_UF)
(declare-const x Real)
(declare-const y Real)

; all this equality should be the same!

; x - 2y + 1 = 0
(assert (= (+ x (- (* 2 y)) 1) 0))

; shuffle
(assert (= (+ (- (* 2 y)) x 1) 0))
(assert (= (+ (- (* 2 y)) 1 x) 0))
(assert (= (+ 1 (- (* 2 y)) x) 0))
(assert (= (+ 1 x (- (* 2 y))) 0))

; -
(assert (= (+ (- x) (* 2 y) (- 1) ) 0))

; * 2
(assert (= (+ (* 2 x) (- (* 4 y)) 2 ) 0))

; / 2
(assert (= (+ (* x (/ 1 2)) (- y) (/ 1.0 2) ) 0))
(assert (= (+ (* x 0.5) (- y) 0.5 ) 0))

; normalize equality
(assert (= (+ x  1) (* 2 y)))
(assert (= x (- (* 2 y) 1) ))

; num and denum normalization
(assert (= (+ (* (/ 2 3) x) (- (* (/ 20 15) y)) (/ 8 12)) 0))

(check-sat)
(get-model)

