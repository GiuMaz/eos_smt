(set-logic QF_UF)
(declare-const x Bool)
(declare-const y Bool)

(assert (or x y))

(check-sat)
