(set-logic QF_LRA)

(declare-fun x () Real)
(declare-fun y () Real)

(assert (distinct x 1))
(assert (>= x 1))
(assert (<= x 1))

(check-sat)
(get-model)
(exit)

