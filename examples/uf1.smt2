(set-logic QF_UF)
(declare-sort U 0)
(declare-const x U)
(declare-const y U)
(declare-fun f (U) U)

(assert (distinct x y))
(assert (= (f x) x))
(assert (= (f y) x))

(check-sat)
(get-model)

