#launch parallel instance of eos in a folder

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
EOS_DIR="$DIR/.."

mkdir -p _tmp_result

NR_CPUS=4
job_count=0
file_count=0
for f in "$@"
do
    echo "$f" > _tmp_result/tmp_$file_count.txt
    $EOS_DIR/release/eos "$f" >> _tmp_result/tmp_$file_count.txt &

    job_count=$((job_count+1))
    file_count=$((file_count+1))
    if [ "$job_count" -eq $NR_CPUS ]; then
        wait
        job_count=0
    fi
done

cat _tmp_result/*.txt > result.txt
rm -rf _tmp_result

