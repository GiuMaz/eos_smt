DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
java -jar $DIR/antlr-4.7.1-complete.jar -Dlanguage=Cpp $DIR/../grammar/SMTLIBv2.g4 -o $DIR/../src/parser -visitor -package smtlibv2
